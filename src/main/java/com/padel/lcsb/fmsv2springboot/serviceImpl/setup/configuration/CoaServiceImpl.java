package com.padel.lcsb.fmsv2springboot.serviceImpl.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ChartofacccountEntity;
import com.padel.lcsb.fmsv2springboot.repository.setup.configuration.CoaRepository;
import com.padel.lcsb.fmsv2springboot.service.setup.configuration.CoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoaServiceImpl implements CoaService {

    @Autowired
    private CoaRepository repository;

    @Override
    public ChartofacccountEntity create(ChartofacccountEntity coa) {
        return repository.save(coa);
    }

    @Override
    public ChartofacccountEntity delete(String code) {
        ChartofacccountEntity coa = findByCode(code);
        if(coa != null){
            repository.delete(coa);
        }
        return coa;
    }

    @Override
    public List<ChartofacccountEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public ChartofacccountEntity findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public Boolean checkAccountCodeExist(String code) {
        Boolean isExist = false;
        ChartofacccountEntity c = repository.findByCode(code);

        if(c!=null){
            isExist = true;
        }
        return isExist;
    }

    //@Override
    //public ChartofacccountEntity update(ChartofacccountEntity coa, int id) {
    //    return null;
    //}
}
