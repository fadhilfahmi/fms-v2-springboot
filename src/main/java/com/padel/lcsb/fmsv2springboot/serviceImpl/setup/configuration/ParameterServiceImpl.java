package com.padel.lcsb.fmsv2springboot.serviceImpl.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ParameterEntity;
import com.padel.lcsb.fmsv2springboot.repository.setup.configuration.ParameterRepository;
import com.padel.lcsb.fmsv2springboot.service.setup.configuration.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParameterServiceImpl implements ParameterService {

    @Autowired
    private ParameterRepository parameterRepository;

    @Override
    public List<ParameterEntity> findAll() {
        return parameterRepository.findAll();
    }

    @Override
    public ParameterEntity findById(String id) {
        return parameterRepository.findById(id);
    }

    @Override
    public List<ParameterEntity> findAllByParameterEquals(String param) {

        List<ParameterEntity> prm = new ArrayList<>();

        prm = parameterRepository.findAllByParameterEquals(param);
        return prm;
    }

}
