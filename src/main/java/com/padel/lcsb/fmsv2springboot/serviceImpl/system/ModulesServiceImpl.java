package com.padel.lcsb.fmsv2springboot.serviceImpl.system;

import com.padel.lcsb.fmsv2springboot.model.system.SecModuleEntity;
import com.padel.lcsb.fmsv2springboot.repository.system.ModulesRepository;
import com.padel.lcsb.fmsv2springboot.service.system.ModulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ModulesServiceImpl implements ModulesService {

    @Autowired
    private ModulesRepository repository;

    @Override
    public SecModuleEntity create(SecModuleEntity mod) {
        return repository.save(mod);
    }

    @Override
    public SecModuleEntity delete(String id) {
        SecModuleEntity mod = findById(id);
        if(mod != null){
            repository.delete(mod);
        }
        return mod;
    }

    @Override
    public List<SecModuleEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public List<SecModuleEntity> findSecModuleEntitiesByModuleidAfter(String id) {
        return repository.findSecModuleEntitiesByModuleidAfter(id);
    }

    @Override
    public List<SecModuleEntity> findSecModuleEntitiesByModuleid(int length,String id) {
        return repository.findSecModuleEntitiesByModuleid(length,id);
    }

    @Override
    public List<SecModuleEntity> findRootModule() {
        return repository.findRootModule();
    }

    @Override
    public SecModuleEntity findById(String id) {
        return repository.findById(id);
    }

    @Override
    public SecModuleEntity update(SecModuleEntity mod, String id) {
        return null;
    }
}
