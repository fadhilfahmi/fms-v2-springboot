package com.padel.lcsb.fmsv2springboot.controller.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ParameterEntity;
import com.padel.lcsb.fmsv2springboot.service.setup.configuration.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api/setup/configuration/parameter"})
public class ParameterController {

    @Autowired
    private ParameterService paramService;

    @GetMapping(path = {"/{id}"})
    public ParameterEntity findOne(@PathVariable("id") String id){
        return paramService.findById(id);
    }

    @GetMapping
    public List<ParameterEntity> findAll(){
        return paramService.findAll();
    }

    @GetMapping(path = {"/type/{id}"})
    public List<ParameterEntity> getParameter(@PathVariable("id") String param){
        return paramService.findAllByParameterEquals(param);
    }


}
