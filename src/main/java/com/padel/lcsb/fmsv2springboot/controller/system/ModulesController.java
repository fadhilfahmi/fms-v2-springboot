package com.padel.lcsb.fmsv2springboot.controller.system;

import com.padel.lcsb.fmsv2springboot.model.system.SecModuleEntity;
import com.padel.lcsb.fmsv2springboot.service.system.ModulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api/system/modules"})
public class ModulesController {

    @Autowired
    private ModulesService modService;

    @PostMapping
    public SecModuleEntity create(@RequestBody SecModuleEntity mod){
        return modService.create(mod);
    }

    @GetMapping(path = {"/{id}"})
    public SecModuleEntity findOne(@PathVariable("id") String id){
        return modService.findById(id);
    }


    @DeleteMapping(path ={"/{id}"})
    public SecModuleEntity delete(@PathVariable("id") String id) {
        return modService.delete(id);
    }

    @GetMapping
    public List<SecModuleEntity> findAll(){
        return modService.findAll();
    }

    @GetMapping(path = {"/root"})
    public List<SecModuleEntity> findRootModule(){
        return modService.findRootModule();
    }

    @GetMapping(path = {"/start"})
    public List<SecModuleEntity> findSecModuleEntitiesByModuleidAfter(String id){
        return modService.findSecModuleEntitiesByModuleidAfter(id);
    }

    @GetMapping(path = {"/startwith"})
    public List<SecModuleEntity> findSecModuleEntitiesByModuleid(int length,String id){
        return modService.findSecModuleEntitiesByModuleid(length,id);
    }
}
