package com.padel.lcsb.fmsv2springboot.controller.setup.configuration;


import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ChartofacccountEntity;
import com.padel.lcsb.fmsv2springboot.service.setup.configuration.CoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api/setup/configuration/coa"})
public class CoaController {

    @Autowired
    private CoaService coaService;

    @PersistenceContext
    private EntityManager entityManager;

    @GetMapping(path = {"/datatable"})
    public List<ChartofacccountEntity> findAll(){
        return coaService.findAll();
    }

    @GetMapping(path = {"/check/{id}"})
    public Boolean checkAccountCodeExist(@PathVariable(value = "id")String id){
        return coaService.checkAccountCodeExist(id);
    }

    @PostMapping(path = {"/add"})//@@RequestBody is important to save json format data, figured it out at 20/8/2018 4:01pm
    public ChartofacccountEntity create(@RequestBody ChartofacccountEntity coa){
        return coaService.create(coa);
    }

    //@PutMapping(path = {"/update"})
    ////public ChartofacccountEntity update(ChartofacccountEntity coa){
        //return coaService.update(coa);
    //}

    @GetMapping("/{id}")
    public ChartofacccountEntity findOne(@PathVariable(value = "id") String code){
        return coaService.findByCode(code);
    }

    @DeleteMapping("/delete/{id}")
    public ChartofacccountEntity delete(@PathVariable(value = "id") String code) {
        return coaService.delete(code);
    }

    //@GetMapping("/notes/{id}")
    //public Note getNoteById(@PathVariable(value = "id") Long noteId) {
      //  return noteRepository.findById(noteId)
        //        .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
    //}
    //@RequestMapping(value="/users", method=RequestMethod.GET)
    //public String listCoa(Model model) {
    //    return "users";
    //}



    //@PutMapping
    //public User update(@RequestBody User user){
    //    return userService.update(user);
    //}

    //@PutMapping(value="/user/{id}")
    //public void putCustomer(@RequestBody User user, @PathVariable int id){
    //    user.replace(id, user);
    //}

}
