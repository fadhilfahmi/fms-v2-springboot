package com.padel.lcsb.fmsv2springboot.repository.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ParameterEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ParameterRepository  extends Repository<ParameterEntity, String> {

    void delete(ParameterEntity coa);

    List<ParameterEntity> findAll();

    List<ParameterEntity> findAllByParameterEquals(String param);


    //@Query("SELECT p FROM SecModuleEntity p WHERE LENGTH(RTRIM(p.moduleid)) = 2")
    //List<ParameterEntity> findRootModule();

    //@Query("SELECT p FROM SecModuleEntity p WHERE LENGTH(RTRIM(p.moduleid)) = ?1 AND p.moduleid LIKE ?2%")
    //List<ParameterEntity> findSecModuleEntitiesByModuleid(int length, String moduleid);

    ParameterEntity findById(String id);

}