package com.padel.lcsb.fmsv2springboot.repository.system;

import com.padel.lcsb.fmsv2springboot.model.system.SecModuleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ModulesRepository  extends Repository<SecModuleEntity, String> {

    void delete(SecModuleEntity coa);

    //@Query("SELECT p FROM SecModuleEntity p WHERE LENGTH(RTRIM(p.moduleid)) = '2'")
    List<SecModuleEntity> findAll();

    //@Query("SELECT p FROM sec_module p WHERE p.moduleid = '0201'")
    List<SecModuleEntity> findSecModuleEntitiesByModuleidAfter(String moduleid);

    @Query("SELECT p FROM SecModuleEntity p WHERE LENGTH(RTRIM(p.moduleid)) = 2")
    List<SecModuleEntity> findRootModule();

    @Query("SELECT p FROM SecModuleEntity p WHERE LENGTH(RTRIM(p.moduleid)) = ?1 AND p.moduleid LIKE ?2%")
    List<SecModuleEntity> findSecModuleEntitiesByModuleid(int length, String moduleid);

    SecModuleEntity findById(String id);

    SecModuleEntity save(SecModuleEntity mod);
}

