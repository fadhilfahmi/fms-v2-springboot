package com.padel.lcsb.fmsv2springboot.repository.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ChartofacccountEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CoaRepository  extends Repository<ChartofacccountEntity, Integer> {

    void delete(ChartofacccountEntity coa);

    List<ChartofacccountEntity> findAll();

    ChartofacccountEntity findByCode (String code);

    ChartofacccountEntity save(ChartofacccountEntity coa);


    //ChartofacccountEntity update(ChartofacccountEntity coa);
}
