package com.padel.lcsb.fmsv2springboot.model.setup.configuration;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "chartofacccount", schema = "fmsv2", catalog = "")
public class ChartofacccountEntity {
    private Long id;
    private String code;
    private String descp;
    private String type;
    private String apptype;
    private String applevel;
    private String report;
    private String attype;
    private String finallvl;
    private String active;
    private String balancesheet;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    @Column(name = "id")
    @NotNull
    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "descp")
    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "apptype")
    public String getApptype() {
        return apptype;
    }

    public void setApptype(String apptype) {
        this.apptype = apptype;
    }

    @Basic
    @Column(name = "applevel")
    public String getApplevel() {
        return applevel;
    }

    public void setApplevel(String applevel) {
        this.applevel = applevel;
    }

    @Basic
    @Column(name = "report")
    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    @Basic
    @Column(name = "attype")
    public String getAttype() {
        return attype;
    }

    public void setAttype(String attype) {
        this.attype = attype;
    }

    @Basic
    @Column(name = "finallvl")
    public String getFinallvl() {
        return finallvl;
    }

    public void setFinallvl(String finallvl) {
        this.finallvl = finallvl;
    }

    @Basic
    @Column(name = "active")
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Basic
    @Column(name = "balancesheet")
    public String getBalancesheet() {
        return balancesheet;
    }

    public void setBalancesheet(String balancesheet) {
        this.balancesheet = balancesheet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChartofacccountEntity that = (ChartofacccountEntity) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (descp != null ? !descp.equals(that.descp) : that.descp != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (apptype != null ? !apptype.equals(that.apptype) : that.apptype != null) return false;
        if (applevel != null ? !applevel.equals(that.applevel) : that.applevel != null) return false;
        if (report != null ? !report.equals(that.report) : that.report != null) return false;
        if (attype != null ? !attype.equals(that.attype) : that.attype != null) return false;
        if (finallvl != null ? !finallvl.equals(that.finallvl) : that.finallvl != null) return false;
        if (active != null ? !active.equals(that.active) : that.active != null) return false;
        if (balancesheet != null ? !balancesheet.equals(that.balancesheet) : that.balancesheet != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (descp != null ? descp.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (apptype != null ? apptype.hashCode() : 0);
        result = 31 * result + (applevel != null ? applevel.hashCode() : 0);
        result = 31 * result + (report != null ? report.hashCode() : 0);
        result = 31 * result + (attype != null ? attype.hashCode() : 0);
        result = 31 * result + (finallvl != null ? finallvl.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        result = 31 * result + (balancesheet != null ? balancesheet.hashCode() : 0);
        return result;
    }


}
