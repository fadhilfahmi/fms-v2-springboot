package com.padel.lcsb.fmsv2springboot.model.system;

import javax.persistence.*;

@Entity
@Table(name = "sec_module", schema = "fmsv2", catalog = "")
public class SecModuleEntity {
    private String moduleid;
    private String moduledesc;
    private String hyperlink;
    private Byte view;
    private Byte disable;
    private Byte finallevel;
    private String pathcontrol;
    private Integer sortid;
    private String icon;
    private String maintable;
    private String subtable;
    private String abb;
    private String posttype;
    private String referidMaster;
    private String referidSub;
    private String approve;
    private String check;
    private String post;

    @Id
    @Column(name = "moduleid")
    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }

    @Basic
    @Column(name = "moduledesc")
    public String getModuledesc() {
        return moduledesc;
    }

    public void setModuledesc(String moduledesc) {
        this.moduledesc = moduledesc;
    }

    @Basic
    @Column(name = "hyperlink")
    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }

    @Basic
    @Column(name = "view")
    public Byte getView() {
        return view;
    }

    public void setView(Byte view) {
        this.view = view;
    }

    @Basic
    @Column(name = "disable")
    public Byte getDisable() {
        return disable;
    }

    public void setDisable(Byte disable) {
        this.disable = disable;
    }

    @Basic
    @Column(name = "finallevel")
    public Byte getFinallevel() {
        return finallevel;
    }

    public void setFinallevel(Byte finallevel) {
        this.finallevel = finallevel;
    }

    @Basic
    @Column(name = "pathcontrol")
    public String getPathcontrol() {
        return pathcontrol;
    }

    public void setPathcontrol(String pathcontrol) {
        this.pathcontrol = pathcontrol;
    }

    @Basic
    @Column(name = "sortid")
    public Integer getSortid() {
        return sortid;
    }

    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    @Basic
    @Column(name = "icon")
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Basic
    @Column(name = "maintable")
    public String getMaintable() {
        return maintable;
    }

    public void setMaintable(String maintable) {
        this.maintable = maintable;
    }

    @Basic
    @Column(name = "subtable")
    public String getSubtable() {
        return subtable;
    }

    public void setSubtable(String subtable) {
        this.subtable = subtable;
    }

    @Basic
    @Column(name = "abb")
    public String getAbb() {
        return abb;
    }

    public void setAbb(String abb) {
        this.abb = abb;
    }

    @Basic
    @Column(name = "posttype")
    public String getPosttype() {
        return posttype;
    }

    public void setPosttype(String posttype) {
        this.posttype = posttype;
    }

    @Basic
    @Column(name = "referid_master")
    public String getReferidMaster() {
        return referidMaster;
    }

    public void setReferidMaster(String referidMaster) {
        this.referidMaster = referidMaster;
    }

    @Basic
    @Column(name = "referid_sub")
    public String getReferidSub() {
        return referidSub;
    }

    public void setReferidSub(String referidSub) {
        this.referidSub = referidSub;
    }

    @Basic
    @Column(name = "approve")
    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    @Basic
    @Column(name = "check")
    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    @Basic
    @Column(name = "post")
    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SecModuleEntity that = (SecModuleEntity) o;

        if (moduleid != null ? !moduleid.equals(that.moduleid) : that.moduleid != null) return false;
        if (moduledesc != null ? !moduledesc.equals(that.moduledesc) : that.moduledesc != null) return false;
        if (hyperlink != null ? !hyperlink.equals(that.hyperlink) : that.hyperlink != null) return false;
        if (view != null ? !view.equals(that.view) : that.view != null) return false;
        if (disable != null ? !disable.equals(that.disable) : that.disable != null) return false;
        if (finallevel != null ? !finallevel.equals(that.finallevel) : that.finallevel != null) return false;
        if (pathcontrol != null ? !pathcontrol.equals(that.pathcontrol) : that.pathcontrol != null) return false;
        if (sortid != null ? !sortid.equals(that.sortid) : that.sortid != null) return false;
        if (icon != null ? !icon.equals(that.icon) : that.icon != null) return false;
        if (maintable != null ? !maintable.equals(that.maintable) : that.maintable != null) return false;
        if (subtable != null ? !subtable.equals(that.subtable) : that.subtable != null) return false;
        if (abb != null ? !abb.equals(that.abb) : that.abb != null) return false;
        if (posttype != null ? !posttype.equals(that.posttype) : that.posttype != null) return false;
        if (referidMaster != null ? !referidMaster.equals(that.referidMaster) : that.referidMaster != null)
            return false;
        if (referidSub != null ? !referidSub.equals(that.referidSub) : that.referidSub != null) return false;
        if (approve != null ? !approve.equals(that.approve) : that.approve != null) return false;
        if (check != null ? !check.equals(that.check) : that.check != null) return false;
        if (post != null ? !post.equals(that.post) : that.post != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = moduleid != null ? moduleid.hashCode() : 0;
        result = 31 * result + (moduledesc != null ? moduledesc.hashCode() : 0);
        result = 31 * result + (hyperlink != null ? hyperlink.hashCode() : 0);
        result = 31 * result + (view != null ? view.hashCode() : 0);
        result = 31 * result + (disable != null ? disable.hashCode() : 0);
        result = 31 * result + (finallevel != null ? finallevel.hashCode() : 0);
        result = 31 * result + (pathcontrol != null ? pathcontrol.hashCode() : 0);
        result = 31 * result + (sortid != null ? sortid.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (maintable != null ? maintable.hashCode() : 0);
        result = 31 * result + (subtable != null ? subtable.hashCode() : 0);
        result = 31 * result + (abb != null ? abb.hashCode() : 0);
        result = 31 * result + (posttype != null ? posttype.hashCode() : 0);
        result = 31 * result + (referidMaster != null ? referidMaster.hashCode() : 0);
        result = 31 * result + (referidSub != null ? referidSub.hashCode() : 0);
        result = 31 * result + (approve != null ? approve.hashCode() : 0);
        result = 31 * result + (check != null ? check.hashCode() : 0);
        result = 31 * result + (post != null ? post.hashCode() : 0);
        return result;
    }
}
