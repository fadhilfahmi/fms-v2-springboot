package com.padel.lcsb.fmsv2springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FmsV2SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(FmsV2SpringbootApplication.class, args);
	}
}
