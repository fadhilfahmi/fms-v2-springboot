package com.padel.lcsb.fmsv2springboot.service.system;

import com.padel.lcsb.fmsv2springboot.model.system.SecModuleEntity;

import java.util.List;

public interface ModulesService {

    SecModuleEntity create(SecModuleEntity mod);

    SecModuleEntity delete(String id);

    List<SecModuleEntity> findSecModuleEntitiesByModuleidAfter(String moduleid);

    List<SecModuleEntity> findSecModuleEntitiesByModuleid(int length,String moduleid);

    List<SecModuleEntity> findRootModule();

    List<SecModuleEntity> findAll();

    SecModuleEntity findById(String id);

    SecModuleEntity update(SecModuleEntity mod, String id);
}

