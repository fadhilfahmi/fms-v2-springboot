package com.padel.lcsb.fmsv2springboot.service.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ParameterEntity;

import java.util.List;

public interface ParameterService {

    List<ParameterEntity> findAll();

    ParameterEntity findById(String id);

    List<ParameterEntity> findAllByParameterEquals(String param);

}
