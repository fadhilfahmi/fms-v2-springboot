package com.padel.lcsb.fmsv2springboot.service.setup.configuration;

import com.padel.lcsb.fmsv2springboot.model.setup.configuration.ChartofacccountEntity;

import java.util.List;

public interface CoaService {

    ChartofacccountEntity create(ChartofacccountEntity coa);

    ChartofacccountEntity delete(String code);

    List<ChartofacccountEntity> findAll();

    ChartofacccountEntity findByCode(String code);

    Boolean checkAccountCodeExist(String code);

   //ChartofacccountEntity update(ChartofacccountEntity coa);
}
