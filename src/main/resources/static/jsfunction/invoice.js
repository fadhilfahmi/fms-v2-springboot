function invx(){
    alert('sa');
}
function inv(estcode,estname,year,period,userid,username,currentdate) {

			//line to configure***************************************************************************************************************
		    var columntoView = [ "code","descp","type","finallvl","active" ];


		    var viewBoxTitle = "View Invoice";
		    var addBoxTitle = "Add New Invoice";
		    var updateBoxTitle = "Edit Invoice";
		   	var deleteBoxTitle = "Delete Invoice";
		   	var generalUsage = "Invoice";


		   	var updateTable = "ap_inv";
		    var updateTitles = [ "Date","Invoice No","Invoice Type","Supplier Name","Supplier Code","Account Description","Account Code","Address", "Total Amount","Deliver To (Code)","Company Name","Remarks","Year","Period","Reference No","Prepared ID","Prepared Name", "Prepared Date"];

		    //var updateTitles = [ "Date","Invoice No","Invoice Type","Supplier Name","Supplier Code","Account Description","Account Code","Total Amount","Address","Deliver To","Remarks","Prepared ID","Prepared Name","Prepared Date","Checked Name","Checked ID","Checked Date","Approved Name","Approved ID","Approved Designation","Approved Date","Year","Period","Reference No"];


		    var updateFields = [ "date","invno","invtype","suppname","suppcode","acdesc","accode","suppaddress","totalamount","estatecode","estatename","remark","year","period","invrefno","preid","prename","predate" ];

		    //var updateFields = [ "date","invno","invtype","suppname","suppcode","acdesc","accode","totalamount","","estatename","remark","preid","prename","predate","checkname","checkid","checkdate","appname","appid","appdesig","appdate","year","period","invrefno" ];



		    var updateReq = [ "1", "1", "1", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" ];
		    var updateID = "invrefno";
		    var updateDesc = "vendor_name";
		    var yesno = [ "Yes", "No" ];		
			var updateFieldtype = [ "text","text","combo - listdb","text - button","text","text","text","textarea","text","text","text","textarea","text","text","text","text","text","text","text","text","combo - listdb",  "text","text","text","text","text","text" ];
			var updateFieldbtparam = [ "readonly","","value=\"Get Account\" onClick=\"popUpWindow('cf_coa_search.jsp?loclevel=none&fcoacode=accountcode&fcoadesc=accountdescp')\"","","","","","","","" ];
			var updateFieldvalue=[ currentdate,"","","","","","","","0.00",estcode,estname,"",year,period,"",userid,username,currentdate ];
			var updateFieldsql = [ "","","Invoice Type","","","","","","","","","","","","","","","","","Payment Method","","","","","","" ];
			var updateFieldparam = ["size=\"25\"","","","size=\"50\"","","size=\"50\"","","","","","size=\"50\"","","","","","","","","","","","","","","","" ];


		    
		   	var deletePage = "ap_inv_delete";
		   	var editPage = "ap_inv_editprocess";
		   	var addPage = "ap_inv_addprocess";

		   	//--------------additional functions goes here---------------------------------------------------------------------------------------------------------
		   	//----for branch b=branch-----------

		   	var bTable = "info_branch";
		    var bTitles = [ "Bank Code","Bank Name","Branch Code","Branch Name","Contact Person","Contact Title", "Contact Position", "Contact HP No", "Address", "Postcode", "City", "State", "Telephone", "Fax", "Email", "URL", "Remarks" ];
		    var bFields = [ "bankcode","bankname","branchcode","branchname","person","title","position","hp","address","postcode","city","state","phone","fax","email","url","remarks" ];
		    var bFieldstoview = [ "branchcode","branchname" ];
		    var bReq = [ "1", "1", "1", "0", "0", "0", "0", "0", "0", "0" ];
		    var bID = "branchcode";
		    var bDesc = "branchname";	
			var bFieldtype = [ "text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text" ];
			var bFieldbtparam = [ "readonly","","","","","","","","","","","","","","","","" ];
			var bFieldvalue=[ "AUTO","","","","","","","","","","","","","","","","" ];
			var bFieldsql = [ "","","","","","","","","","","","","","","","","" ];
			var bFieldparam = ["size=\"25\"","size=\"50\"","size=\"25\"","size=\"50\"","size=\"50\"","size=\"50\"","size=\"50\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"" ];

			var bBoxTitle = "Add New Branch";
			//var deletePage = "cf_bank_delete";
		   	//var editPage = "cf_bank_editprocess";
		   	var bPage = "cf_bank_branch_addprocess";

		    //line to configure***********************************************************************************************************************************

		    var colsTosend = 'cols=';
			
			for (m = 0; m < updateFields.length; ++m) {//put value from database to variable jget
					
					if(m == updateFields.length - 1){
						colsTosend += updateFields[m];
					}else{
						colsTosend += updateFields[m]+'&cols=';
					}
			}

			//alert(colsTosend);

			

		    $( "#add-new-invoice" ).button({
				icons: {
					primary: "ui-icon-circle-plus"
				}
			})
			.click(function() {
			    addNewInvoice();
			     //$(this).toggle(1000);
			});

			$( "#button_view" ).button({
				icons: {
					primary: "ui-icon-circle-plus",
					text: false
				}
			});

			$( "#button_edit" ).button({
				icons: {
					primary: "ui-icon-bookmark",
					text: false
				}
			});

			$( "#get-acct" ).button({
				icons: {
					primary: "ui-icon-circle-plus"
				}
			})
			.click(function() {
			    $( "#account-form" ).dialog( "open" );
			});

			var oTable = $('#invoice').dataTable( {
				"bJQueryUI": true,
				"bProcessing": true,
				"bServerSide": true,
				"bAutoWidth": true,
				"sPaginationType": "full_numbers",
				"sScrollY": "320px",
				//"sAjaxSource": "<%=pagex%>.jsp?table="+updateTable+"&code="+updateID+"&cols="+colsTosend,
				"sAjaxSource": "ap_inv_table.jsp",
				"aoColumns": [
					{"sTitle": "Invoice No", "mData": "invrefno", "sWidth": "10%", "bSearchable": true},
					{"sTitle": "Date", "mData": "date", "sWidth": "10%"},
					{"sTitle": "Supplier", "mData": "suppname", "sWidth": "30%"},
					{"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "20%"}
						
						],
					"aoColumnDefs": [ {
						"aTargets": [3],
							"fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
								
								var a = $('<img src="image/icon/addmaterial.png" title="View"   style="vertical-align:middle;cursor:pointer">');
								var d = $('<img src="image/icon/addrefer.png" title="Edit"   style="vertical-align:middle;cursor:pointer">');
								var b = $('<img src="image/icon/view.png" title="View" width="22" style="vertical-align:middle;cursor:pointer">');
								var c = $('<img src="image/icon/edit_2.png" title="Edit" width="22" style="vertical-align:middle;cursor:pointer">');
								var e = $('<img src="image/icon/delete.png" title="Delete" width="22" style="vertical-align:middle;cursor:pointer">');

								a.addClass('imgbtn');
								d.addClass('imgbtn');
								b.addClass('imgbtn');
								c.addClass('imgbtn');
								e.addClass('imgbtn');
								
								a.on('click', function() {
									//console.log(oData);
									addMaterial(oData.invrefno);
									return false;
								});
								 b.on('click', function() {
									//console.log(oData);
									viewInv(oData.invrefno);
									return false;
								});
								 c.on('click', function() {
									//console.log(oData);
									editRow(oData.code);
									//$("#dialog-form-update").dialog("open");
									//$( "#dialog-form-update" ).dialog( "open" );
									return false;
								});
								e.on('click', function() {
									//console.log(oData);
									deleteRow(oData.code);
									return false;
								});
	                    
								$(nTd).empty();
								$(nTd).attr("id",'btntest');
	                    		$(nTd).prepend(a,d,b,c,e);
						  }
						 
					} ]

					
					
				} );



			$( "#btntest" ).button({
				icons: {
					primary: "ui-icon-circle-plus"
				}
			});

			var tips = $( ".validateTips" );
 
			function updateTips( t ) {
				alert(t);
			    tips.text( t ).addClass( "ui-state-highlight" );
			    setTimeout(function() {
			        tips.removeClass( "ui-state-highlight", 1500 );
			    }, 500 );
			      
			}
			 
			function checkLength( o, n, min, max ) {
			    if ( o.length > max || o.length < min ) {
			        o.addClass( "ui-state-error" );
			        updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );
			        return false;
			    } else {
			        return true;
			    }
			}
			 
			function checkRegexp( o, regexp, n ) {
			    if ( !( regexp.test( o.val() ) ) ) {
			        o.addClass( "ui-state-error" );
			        updateTips( n );
			        return false;
			    } else {
			        return true;
			    }
			}
				
			function addNewInvoice() {
				var newItem = "";
 				var str = '<div id="dialog-form-update" title="'+addBoxTitle+'"><p class="validateTips">All form fields are required.</p><form><table id="table_2" cellspacing="0">';
				var i;

				for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
					str += '<tr><td width="30%" class="bd_bottom" align="left" valign="top">'+updateTitles[i]+'</td>';
					str += '<td width="63%" class="bd_bottom" align="left">';

					if(updateFieldtype[i] == "text"){

						

							//begin*************for generate automatic refer no***************
							if(updateFields[i]=='invno'){
								var no = '';//column to retrieve the value
								var table = 'ap_inv';
								var refertitle = 'invno';
								var colx = ['invno'];
								var strx = 'cols=';
								for (j = 0; j < colx.length; ++j) {
								    strx+= colx[j]+'&cols=';
								}

								var where = '';
								var exString1 = 'table='+table+'&refertitle='+refertitle;
								$.ajax({
									//url: "get_dataquery.jsp?definecolumn="+encodeURIComponent(defineColumn),
									url: "get_maxno.jsp",
									type:'POST',
									async: false,
									data: exString1,
									success: function(output_string){
										var obj = $.parseJSON(output_string);
										$.each(obj, function() {
										    no = this['newrefer'];
										});
									}
								});

								updateFieldvalue[i]=no;
								str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  />';
								//end*************for generate automatic refer no*****************
							

							}else if(updateFields[i]=='invrefno'){
								var no = '';//column to retrieve the value
								var table = 'ap_inv';
								var abb = 'PNV';
								var refertitle = 'invrefno';
								var colx = ['no'];
								var defineColumn = "ifnull(concat(lpad(max(no)+1,4,'0')), '0001') as";
								var strx = 'cols=';
								for (j = 0; j < colx.length; ++j) {
								    strx+= colx[j]+'&cols=';
								}

								var where = '';
								var exString1 = 'table='+table+'&refertitle='+refertitle+'&abb='+abb;
								$.ajax({
									url: "get_referno.jsp?definecolumn="+encodeURIComponent(defineColumn),
									type:'POST',
									async: false,
									data: exString1,
									success: function(output_string){
										var obj = $.parseJSON(output_string);
										$.each(obj, function() {
										    no = this['newrefer'];
										});
									}
								});

								updateFieldvalue[i]=no;
								str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  />';
						}else{
								str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" value="'+updateFieldvalue[i]+'" type="text" '+updateFieldparam[i]+'  />';
							}

						

					}else if(updateFieldtype[i] == "text - button"){

						str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  /><input type="button" id="getcomp" value="Get Company">';

					}else if(updateFieldtype[i] == "combo - listdb"){

						str += '<select id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
						var listDB = "parameter";
						var listFields = [ "parameter", "value" ];
						var listID = "aparameter";
						var listItem = 'cols=';
						
						for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
							listItem += listFields[m]+'&cols=';
						}
						
						var listString = 'identifier='+listID+'&'+listID+'='+updateFieldsql[i]+'&table='+listDB+'&'+listItem;

						$.ajax({
							url: 'get_data.jsp',
							type:'POST',
							async: false,
							data: listString,
							success: function(output){
								var obja = $.parseJSON(output);
				            		$.each(obja, function() {
										var paramt = this['parameter'];
										var valt = this['value'];
										str += '<option value"'+valt+'">'+valt+'</option>';
									});
							}
						});
						
						str += '</select>';
													
					}else if(updateFieldtype[i] == "combo - yesno"){

						str += '<select id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
						var selOpt = "";

						for (u = 0; u < yesno.length; ++u) {
							str += '<option value"'+yesno[u]+'">'+yesno[u]+'</option>';
						}

						str += '</select>';

						
					}else if(updateFieldtype[i] == "textarea"){

						str += '<textarea id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+' rows="4" cols="50">';
						

						str += '</textarea>';

						
					}
 												
 					str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

				}

				str += '</table></form></div>';

				var addTable = $(str);

				$(addTable).dialog({
				    autoOpen: false,
					height: 550,
					width: 700,
					modal: true,
					buttons: {
		  				"Save": function() {
							var eachI = [];
							//var bValid = true;
							//allFields.removeClass( "ui-state-error" );
							for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
								if(updateReq[j] == 1){
									newItem = $('#ad_'+updateFields[1]).val(); 
									eachI[j] = $('#ad_'+updateFields[j]).val();  
									//bValid = bValid && checkLength( eachI[j] , updateTitles[j], 1, 16 );
									//alert(eachI[j]);
								}
							}

							/*
							bValid = bValid && checkLength( code, "Material Code", 1, 16 );
							bValid = bValid && checkLength( descp, "Material Description", 1, 100 );
							//bValid = bValid && checkLength( accountcode, "Account Code", 1, 100 );

							bValid = bValid && checkRegexp( code, /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/, " Numeric characters only!" );
							bValid = bValid && checkRegexp( descp, /^[a-z]([0-9a-z_])+$/i, " Material Description may consist of a-z, 0-9, underscores, begin with a letter." );
							bValid = bValid && checkLength( email, "email", 6, 80 );
							bValid = bValid && checkLength( password, "password", 5, 16 );
												 
							bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
							// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
							bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
							bValid = bValid && checkRegexp( password/^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );*/
							//if ( bValid ) {

							var tbl = 'ap_inv';
							var pagetoredirect = 'ap_inv_list';
							var upTitle = [];
							var dataStringUp = "";
							for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
								upTitle[j] = $('#ad_'+updateFields[j]).val();

								if(j == updateFields.length-1){
									dataStringUp += updateFields[j]+'='+upTitle[j];
								}else{
									dataStringUp += updateFields[j]+'='+upTitle[j]+'&';
								}

								

											                 	
							}
							dataStringUp+='&'+colsTosend+'&table='+tbl+'&page='+pagetoredirect;
															 //alert(dataStringUp);
							$.ajax({
								type:'POST',
								async: false,
								data:dataStringUp,
								url:'save_data.jsp',
								success:function(response) {
									$(this).dialog("destroy");
								}
							});
														
							//}bValid
							$(this).dialog("destroy");
							successNoty( generalUsage , newItem, generalUsage+' Added', 'Successfull added ');
						},
						Cancel: function() {
							$(this).dialog("destroy");
							infoNoty('You did not add anything.');
						}
					}
				});

    			$(addTable).dialog("open"); 

				$('#get-acctx').on('click',function(){
					var strGetBox = '<div id="account-form-update" title="Get Account Code"><div class="ui-widget" align="left"><label for="keyword-update">Type the Keyword: </label><input id="keyword-update" align="left align="left""  size="55"></div><div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Account Code:<input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div></div>';

					var updateTableMini = $(strGetBox);

					$( updateTableMini ).dialog({
						autoOpen: false,
						height: 300,
						width: 400,
						modal: true,
						buttons: {
						
							"Get Code": function() {
								var gcodex = $("#gcode-update").val();
								var gdescpx = $("#gdescp-update").val();
								$("#ad_accountcode").val(gcodex);
								$("#ad_accountdescp").val(gdescpx);
								$(this).dialog("destroy");
							},
							Cancel: function() {
								$(this).dialog("destroy");
								$( this ).dialog( "close" );
							}
						}
					});

					$( updateTableMini ).dialog( "open" );
					$( "#keyword-update" ).autocomplete({
						source: "get_account.jsp",
						minLength: 2,
						select: function( event, ui ) {
							loga( ui.item ?
								ui.item.id :
								"Nothing selected, input was " + this.value );
							logi( ui.item ?
								ui.item.value :
								"Nothing selected, input was " + this.value );
						}
					});
				});

			

					function loga( message ) {
						$("#gcode-update").val(message);
						$( "#keyword-update" ).autocomplete( "close" );
					}

					function logi( message ) {
						$("#gdescp-update").val(message);
						$( "#keyword-update" ).autocomplete( "close" );
					}

				$('#getcomp').on('click',function(){

	 				
	 				var table = 'ph_supplier';
					var colx = ['suppcode', 'suppname', 'suppaddress', 'accode', 'acdesc' ];
					var strx = 'cols=';
					for (j = 0; j < colx.length; ++j) {
						strx+= colx[j]+'&cols=';
					}

					var where = " where estatecode='"+estcode+"' order by suppname";
					var exString1 = 'table='+table+'&'+strx;

					var strGetBox = '<div id="get_supplier_box" title="Get Supplier">';

						strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Supplier Name or Supplier Code: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
						strGetBox += '<div><table id="supplier_output">';
						//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';

						$.ajax({
							url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
							type:'POST',
							async: false,
							data: exString1,
							success: function(output){
								var jGet = [];
								var i = 0;
								var obja = $.parseJSON(output);
				            		$.each(obja, function() {

				            			
				            			for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
											jGet[i] = this[colx[i]];
										}
										strGetBox += '<tr><td class="selected_td" style="cursor:hand">'+jGet[0]+' - '+jGet[1];
										strGetBox += '<input type="hidden" value="'+jGet[0]+'" id="suppcode'+jGet[0]+'">';
										strGetBox += '<input type="hidden" value="'+jGet[1]+'" id="suppname'+jGet[0]+'">';
										strGetBox += '<input type="hidden" value="'+jGet[2]+'" id="suppaddress'+jGet[0]+'">';
										strGetBox += '<input type="hidden" value="'+jGet[3]+'" id="suppacode'+jGet[0]+'">';
										strGetBox += '<input type="hidden" value="'+jGet[4]+'" id="suppdesc'+jGet[0]+'">';
										strGetBox += '</td></tr>';
									});
								}	
						});	
						strGetBox += '';
						strGetBox += '</table></div>';
						strGetBox += '</div>';


					var updateTableMini = $(strGetBox);

					$( updateTableMini ).dialog({
						autoOpen: false,
						height: 300,
						width: 400,
						modal: true,
						buttons: {
						
							/*"Get Code": function() {
								var gcodex = $("#gcode-update").val();
								var gdescpx = $("#gdescp-update").val();
								$("#ad_companycode").val(gcodex);
								$("#ad_companyname").val(gdescpx);
								$(this).dialog("destroy");
							},
							Cancel: function() {
								$(this).dialog("destroy");
								$( this ).dialog( "close" );
							}*/
						}
					});

					$( updateTableMini ).dialog( "open" );
					
					/*$( "#keyword-update" ).autocomplete({
						source: "get_buyer.jsp",
						minLength: 2,
						select: function( event, ui ) {
							loga( ui.item ?
								ui.item.id :
								"Nothing selected, input was " + this.value );
							logi( ui.item ?
								ui.item.value :
								"Nothing selected, input was " + this.value );
						}
					});

	*/			
					$( "#keyword-insert" ).keyup(function() {

						var newsupp = $('#keyword-insert').val();
						var where2 = " where estatecode='"+estcode+"' and suppname like '%"+newsupp+"%' or suppcode like '%"+newsupp+"%' order by suppname";
						var exString2 = 'table='+table+'&'+strx;


						var returnsuppbox = '<table id="supplier_output">';
						//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';

						$.ajax({
							url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
							type:'POST',
							async: false,
							data: exString2,
							success: function(output){
								var jGet = [];
								var i = 0;
								var obja = $.parseJSON(output);
				            		$.each(obja, function() {
				            			
				            			
				            			for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
											jGet[i] = this[colx[i]];
										}
										returnsuppbox += '<tr><td class="selected_td" style="cursor:hand">'+jGet[0]+' - '+jGet[1];
										returnsuppbox += '<input type="hidden" value="'+jGet[0]+'" id="suppcode'+jGet[0]+'">';
										returnsuppbox += '<input type="hidden" value="'+jGet[1]+'" id="suppname'+jGet[0]+'">';
										returnsuppbox += '<input type="hidden" value="'+jGet[2]+'" id="suppaddress'+jGet[0]+'">';
										returnsuppbox += '<input type="hidden" value="'+jGet[3]+'" id="suppacode'+jGet[0]+'">';
										returnsuppbox += '<input type="hidden" value="'+jGet[4]+'" id="suppdesc'+jGet[0]+'">';
										returnsuppbox += '</td></tr>';
									});
								}	
						});	
						returnsuppbox += '';
						returnsuppbox += '</table>';

						$('#supplier_output').html(returnsuppbox);
					});

				});

				$(document).on('click','.selected_td', function(){
					var m = $(this).html();
					var v = m.substring(0, 4);
					var aVal = $('#suppcode'+v).val();
					var bVal = $('#suppname'+v).val();
					var cVal = $('#suppaddress'+v).val();
					var dVal = $('#suppacode'+v).val();
					var eVal = $('#suppdesc'+v).val();

					$("#ad_suppcode").val(aVal);
					$("#ad_suppname").val(bVal);
					$("#ad_suppaddress").val(cVal);
					$("#ad_accode").val(dVal);
					$("#ad_acdesc").val(eVal);
					$( "#get_supplier_box" ).remove();


				    var table_in = 'ph_supplier_detail';
					var colx_in = ['matcode', 'matdesc' ];
					var strx_in = 'cols=';
					for (j = 0; j < colx_in.length; ++j) {
						strx_in+= colx_in[j]+'&cols=';
					}

					var selectmat = '<select id="ad_typedesc">';
					selectmat += '<option value="">Please Select</option>';
					var where_in = " where suppcode='"+v+"'";
					var exString_in = 'table='+table_in+'&'+strx_in;
				    $.ajax({
						url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where_in),
						type:'POST',
						async: false,
						data: exString_in,
						success: function(output){
							var jGet_in = [];
							var i = 0;
							var obja = $.parseJSON(output);
							$.each(obja, function() {
								            			
								for (i = 0; i < colx_in.length; ++i) {//put value from database to variable jget
									jGet_in[i] = this[colx_in[i]];
															
								}
								selectmat += '<option value="'+jGet_in[0]+'">'+jGet_in[1]+'</option>';
								//alert(jGet_in[1]);
								
							});	
						}
					
					});

					selectmat += '</select>';

					$( "#ad_typedesc" ).replaceWith( selectmat );
					$( "#ad_typecode" ).val( "" );


				});

				$(document).on('focus','#ad_totalamount', function(){

					var oVal = $(this).val();

					var str_amountbox = '<div id="enteramount" title="Enter Amount">';

						str_amountbox += '<div class="ui-widget" align="left"><label for="keyword-update">';
						str_amountbox += '<input type="text"  id="amountinput" value="'+oVal+'" onChange="moneyFormat(this)"><p> And Press Enter</p>';
						str_amountbox += '</div>';
						str_amountbox += '</div>';

					var amountbox = $(str_amountbox);

					$( amountbox ).dialog({
						autoOpen: false,
						height: 140,
						width: 300,
						modal: true,
						buttons: {
						
							/*"Get Code": function() {
								var gcodex = $("#gcode-update").val();
								var gdescpx = $("#gdescp-update").val();
								$("#ad_companycode").val(gcodex);
								$("#ad_companyname").val(gdescpx);
								$(this).dialog("destroy");
							},
							Cancel: function() {
								$(this).dialog("destroy");
								$( this ).dialog( "close" );
							}*/
						}
					});

					$( amountbox ).dialog( "open" );

					$('#amountinput').keypress(function(e) {

				    if (e.keyCode == $.ui.keyCode.ENTER) {

				    	var nVal = parseFloat($(this).val());
				    	var n = nVal.toFixed(2);
						$("#ad_totalamount").val(n);
						$(amountbox).dialog("destroy");
				          //Close dialog and/or submit here...
				    }
				});
				});

				

				}
				//$("body").on("click", "#example tbody tr", function (e) {
				function editRow(tdCode) {
					var nDesc = "";
		            var strItem = 'cols=';

		            for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
		                strItem+= updateFields[j]+'&cols=';
					}

					var sCode = tdCode;
		            var dialogText="Value = "+sCode;
		            var targetUrl = $(this).attr("href");
					var exString = 'identifier='+updateID+'&'+updateID+'='+sCode+'&table='+updateTable+'&'+strItem;
		            
		            $.ajax({
				        url: 'get_data.jsp',
				        type:'POST',
				        data: exString,
				        success: function(output_string){
				            var obj = $.parseJSON(output_string);
				            	$.each(obj, function() {
				            		//var pd = $("<div/>").attr("id", "pageDialog");
 									//$(pd).text(jsType ).dialog("open");
									var str = '<div id="dialog-form-update" title="'+updateBoxTitle+'"><p class="validateTips">All form fields are required.</p><form><table id="table_2" cellspacing="0">';
									var i;
									var jGet = [];

									for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
										jGet[i] = this[updateFields[i]];
										str += '<tr><td width="30%" class="bd_bottom" align="left">'+updateTitles[i]+'</td>';
										str += '<td width="63%" class="bd_bottom" align="left">';

										if(updateFieldtype[i] == "text"){
											str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+'  />';
										}else if(updateFieldtype[i] == "text - button"){
											str += '<input id="up_'+updateFields[i]+'"  type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' /></td><td> <button id="get-acct">Get Account</button></td>';
										}else if(updateFieldtype[i] == "combo - listdb"){
											str += '<select id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
											var selParam = "";
											var listDB = "parameter";
										    var listFields = [ "parameter", "value" ];
										    var listID = "aparameter";
										    var listItem = 'cols=';
										    
										    for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
										        listItem += listFields[m]+'&cols=';
											}
										    
										    var listString = 'identifier='+listID+'&'+listID+'='+updateFieldsql[i]+'&table='+listDB+'&'+listItem;

											$.ajax({
												url: 'get_data.jsp',
											    type:'POST',
											    async: false,
											    data: listString,
											    success: function(output){
											        var obja = $.parseJSON(output);
				            						$.each(obja, function() {
														var paramt = this['parameter'];
														var valt = this['value'];

														if(valt == jGet[i]){
															selParam="selected";
														}else{
															selParam="";
														}
															str += '<option value"'+valt+'" '+selParam+'>'+valt+'</option>';
																
													});
											    }
											});
											
											str += '</select>';
													
										}else if(updateFieldtype[i] == "combo - yesno"){
											str += '<select id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
											var selOpt = "";

											for (u = 0; u < yesno.length; ++u) {
												if(yesno[u] == jGet[i]){
													selOpt="selected";
												}else{
													selOpt="";
												}
												str += '<option value"'+yesno[u]+'" '+selOpt+'>'+yesno[u]+'</option>';
											}
											str += '</select>';
										}
 												
 										str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

						        	}

						            str += '</table></form></div>';

				            		var updateTable = $(str);

				            		$(updateTable).dialog({
				            			autoOpen: false,
										height: 550,
										width: 700,
										modal: true,
										buttons: {
		  									"Update": function() {
											    var upTitle = [];
												var dataStringUp = "";

												for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
										            upTitle[j] = $('#up_'+updateFields[j]).val();

													if(updateDesc == updateFields[j]){
										                nDesc = $('#up_'+updateFields[j]).val();
										            }
										                 	
										            if(j == updateFields.length-1){
										                dataStringUp += updateFields[j]+'='+upTitle[j];
										            }else{
										                dataStringUp += updateFields[j]+'='+upTitle[j]+'&';
										            }
										                 	
												}
														 //alert(dataStringUp);
												$.ajax({
												    type:'POST',
												    data:dataStringUp,
												    url:editPage+'.jsp',
												    success:function(response) {
												        $(this).dialog("destroy");
												    }
												});
												
												$(this).dialog("destroy");
												successNoty( generalUsage , nDesc, generalUsage+' Updated', 'Successfull for ');
													 
											},
											Cancel: function() {
												$(this).dialog("destroy");
												//$( this ).dialog( "close" );
											}
										}
					            	});
    								
    								$(updateTable).dialog("open"); 
								});

									        
								$('#get-acct').on('click',function(){
									var strGetBox = '<div id="account-form-update" title="Get Account Code"><div class="ui-widget" align="left"><label for="keyword-update">Type the Keyword: </label><input id="keyword-update" align="left align="left""  size="55"></div><div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Account Code:<input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div></div>';
									var updateTableMini = $(strGetBox);
									
									$( updateTableMini ).dialog({
										autoOpen: false,
										height: 300,
										width: 400,
										modal: true,
										buttons: {
										    "Get Code": function() {
												var gcodex = $("#gcode-update").val();
										        var gdescpx = $("#gdescp-update").val();
										        $("#up_accountcode").val(gcodex);
										        $("#up_accountdescp").val(gdescpx);
										        $(this).dialog("destroy");
										          //$( this ).dialog( "close" );
										    },
										    Cancel: function() {
										        $(this).dialog("destroy");
										        $( this ).dialog( "close" );

										    }
										}
									});
									
									$( updateTableMini ).dialog( "open" );

									$( "#keyword-update" ).autocomplete({
										source: "get_account.jsp",
										minLength: 2,
										select: function( event, ui ) {
											loga( ui.item ?
											    ui.item.id :
											    "Nothing selected, input was " + this.value );

											logi( ui.item ?
											    ui.item.value :
											    "Nothing selected, input was " + this.value );


										}
									});
								});

								function loga( message ) {
									$("#gcode-update").val(message);
									$( "#keyword-update" ).autocomplete( "close" );
								}

								function logi( message ) {
									$("#gdescp-update").val(message);
									$( "#keyword-update" ).autocomplete( "close" );
								}


				            		


				            		
				         	}
				         });

		                          
		            }

		            function viewRow(tdCode) {
		                 var strItem = 'cols=';
		                for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
		                	strItem+= updateFields[j]+'&cols=';
						}

						 
		                var sCode = tdCode;
		                var dialogText="Value = "+sCode;

		                var exString = 'identifier='+updateID+'&'+updateID+'='+sCode+'&table='+updateTable+'&'+strItem;
		                $.ajax({
				            url: 'get_data.jsp',
				            type:'POST',
				            data: exString,
				            success: function(output_string){
				            		var obj = $.parseJSON(output_string);
				            		$.each(obj, function() {
				            			

					            				//var pd = $("<div/>").attr("id", "pageDialog");
	 											//$(pd).text(jsType ).dialog("open");
											var str = '<div id="dialog-form-update" title="'+viewBoxTitle+'"><table id="table_2" cellspacing="0">';
											var i;
										    var jGet = [];

											for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
												jGet[i] = this[updateFields[i]];

												str += '<tr><td width="30%" class="bd_bottom" align="left">'+updateTitles[i]+'</td>';
												str += '<td width="63%" class="bd_bottom" align="left">';
												str += '<strong>'+jGet[i]+'</strong>';
												str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

						            		}

						            		str += '</table></div>';

				            				var viewTable = $(str);

				            				 $(viewTable).dialog({
				            				 	autoOpen: false,
										      	height: 550,
										      	width: 700,
										      	modal: true,

										      	buttons: {
		  
											        "Print": function() {
											         
											         	$(this).dialog("destroy");
													 
													},
													Cancel: function() {
													          $(this).dialog("destroy");
													        }
													}
					            				});
    

				            				 
							                	$(viewTable).dialog("open"); 
									        });
									}
				         });

		                          
		            }

		            function viewInv(x){

		            	window.location = 'ph_inv_view.jsp?invrefno='+x;

		            }
		            

		            function deleteRow(tdCode){

		            	var str = '<div id="dialog-form-update" title="'+deleteBoxTitle+'">';
		            		str += 'Are you sure to delete this (';
		            		str += tdCode;
		            		str += ') ?</div>';
		            	var deleteBox = $(str);

				        $(deleteBox).dialog({
					        autoOpen: false,
							modal: true,

							buttons: {
			  
								"I'm Sure": function() {

									$.ajax({
										type:'POST',
										data:'id='+tdCode,
										url:deletePage+'.jsp',
										success:function(response) {
											$(this).dialog("destroy");
										}
									});
													         
									$(this).dialog("destroy");
									successNoty( generalUsage , tdCode, generalUsage+' Deleted', 'Successful delete '+generalUsage+' ');
															 
								},
								Cancel: function() {
									$(this).dialog("destroy");
								}
							}
						});

    					$(deleteBox).dialog("open"); 
					
		        	}

		        	function successNoty( nType, nIdentify, nTitle, nText){
		        		$.pnotify({
							title: nTitle,
							text: nText+nIdentify,
							type: 'success',
							styling: 'jqueryui',
							shadow: false,
							addClass:'customsuccess'
						});
		        	}

		        	function infoNoty( nText){
		        		$.pnotify({
		        			text:nText,
		        			styling: 'jqueryui'
		        		});
		        	}

		        	function errorNoty( nType, nIdentify ){
		        		$.pnotify({
						    title: 'No Shadow Error',
						    text: 'I don\'t have a shadow. (It\'s cause I\'m a vampire or something. Or is that reflections...)',
						    type: 'error',
						    shadow: false
						});
		        	}

		        	//--------------------------this page functions goes here--------------------------------------------------------
		        	
		        	function addMaterial(tdCode){

		        		//get invoice data
						var cekDB = "ph_inv";
						var cekFields = [ "invtype", "suppname", "date" ];
						var cekID = "invrefno";
						var cekItem = 'cols=';

						var invType = '';
						var invSuppname = '';
						var invDate = '';
										    
						for (m = 0; m < cekFields.length; ++m) {//put value from database to variable jget
							cekItem += cekFields[m]+'&cols=';
						}
										    
						var listString = 'identifier='+cekID+'&'+cekID+'='+tdCode+'&table='+cekDB+'&'+cekItem;

						$.ajax({
							url: 'get_data.jsp',
							type:'POST',
							async: false,
							data: listString,
							success: function(output){
								var obja = $.parseJSON(output);
				            	$.each(obja, function() {
									invType = this['invtype'];
									invSuppname = this['suppname'];
									invDate = this['date'];
																
								});
							}
						});

						//end-------------

		        		var mat_Title = [ "Reference No","Company Name","Material Code","Material Description","Quantity","Unit of Measure","Unit Price","Amount","Remarks","Year","Period","Reference"];

					    

					    var mat_Field = [ "no","companyname","matcode","matdesc","quantity","unitmeasure","unitprice","amount","remark","year","period","invrefno" ];

					   	var mat_Req = [ "1", "1", "1", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" ];

					    var mat_ID = "invrefno";
					    var mat_Desc = "vendor_name";
					    var yesno = [ "Yes", "No" ];
					    		
						var mat_Fieldtype = [ "text","text","text - button","text","text","text","text","textarea","text","text","text","textarea","text","text","text","text","text","text","text","text","combo - listdb",  "text","text","text","text","text","text" ];
						var mat_Fieldbtparam = [ "readonly","","value=\"Get Account\" onClick=\"popUpWindow('cf_coa_search.jsp?loclevel=none&fcoacode=accountcode&fcoadesc=accountdescp')\"","","","","","","","" ];
						var mat_Fieldvalue=[ invDate,invSuppname,"","","","","","","0.00",estcode,estname,"",year,period,"",userid,username,currentdate ];
						var mat_Fieldsql = [ "","","Invoice Type","","","","","","","","","","","","","","","","","Payment Method","","","","","","" ];
						var mat_Fieldparam = ["size=\"25\"","size=\"50\"","","size=\"50\"","","size=\"50\"","","","","","size=\"50\"","","","","","","","","","","","","","","","" ];

						var str = '<div id="dialog-form-update" title="Add Material for Invoice '+tdCode+'"><table id="table_2" cellspacing="0">';
						str+= '';

						var i;

						for (i = 0; i < mat_Field.length; ++i) {

							str += '<tr><td width="30%" class="bd_bottom" align="left" valign="top">'+mat_Title[i]+'</td>';
							str += '<td width="63%" class="bd_bottom" align="left">';

							if(mat_Fieldtype[i] == "text"){

								str += '<input style="font-size:11px" id="ad_'+mat_Field[i]+'" type="text" value="'+mat_Fieldvalue[i]+'" '+mat_Fieldparam[i]+'  />';
							
							
								
							//	str += '<input style="font-size:11px" id="ad_'+mat_Field[i]+'" value="'+updateFieldvalue[i]+'" type="text" '+updateFieldparam[i]+'  />';
							//}

							

							}else if(mat_Fieldtype[i] == "text - button"){

								if(mat_Field[i] == 'matcode'){
									str += '<input style="font-size:11px" id="ad_'+mat_Field[i]+'" type="text" value="'+mat_Fieldvalue[i]+'" '+mat_Fieldparam[i]+'  /><input type="button" id="getcomp1" value="Get Company">';
								}else{
									str += '<input style="font-size:11px" id="ad_'+mat_Field[i]+'" type="text" value="'+mat_Fieldvalue[i]+'" '+mat_Fieldparam[i]+'  /><input type="button" id="getcomp1" value="Get Company">';
								}

								

							}


						}
						

						str+= '</div>';
		        		var addMat = $(str);

						$(addMat).dialog({
						    autoOpen: false,
							height: 550,
							width: 900,
							modal: true,
							buttons: {
				  				"Save": function() {
									
								},
								Cancel: function() {
									$(this).dialog("destroy");
									infoNoty('You did not add anything.');
								}
							}
						});

		    			$(addMat).dialog("open");

		    			$('#getcomp1').on('click',function(){

	 				
			 				var table = 'ph_supplier';
							var colx = ['suppcode', 'suppname', 'suppaddress', 'accode', 'acdesc' ];
							var strx = 'cols=';
							for (j = 0; j < colx.length; ++j) {
								strx+= colx[j]+'&cols=';
							}

							var where = " where estatecode='"+estcode+"' order by suppname";
							var exString1 = 'table='+table+'&'+strx;

							var strGetBox = '<div id="get_supplier_box" title="Get Supplier">';

								strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Supplier Name or Supplier Code: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
								strGetBox += '<div><table id="supplier_output">';
								//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';

								$.ajax({
									url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
									type:'POST',
									async: false,
									data: exString1,
									success: function(output){
										var jGet = [];
										var i = 0;
										var obja = $.parseJSON(output);
						            		$.each(obja, function() {

						            			
						            			for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
													jGet[i] = this[colx[i]];
												}
												strGetBox += '<tr><td class="selected_td" style="cursor:hand">'+jGet[0]+' - '+jGet[1];
												strGetBox += '<input type="hidden" value="'+jGet[0]+'" id="suppcode'+jGet[0]+'">';
												strGetBox += '<input type="hidden" value="'+jGet[1]+'" id="suppname'+jGet[0]+'">';
												strGetBox += '<input type="hidden" value="'+jGet[2]+'" id="suppaddress'+jGet[0]+'">';
												strGetBox += '<input type="hidden" value="'+jGet[3]+'" id="suppacode'+jGet[0]+'">';
												strGetBox += '<input type="hidden" value="'+jGet[4]+'" id="suppdesc'+jGet[0]+'">';
												strGetBox += '</td></tr>';
											});
										}	
								});	
								strGetBox += '';
								strGetBox += '</table></div>';
								strGetBox += '</div>';


							var updateTableMini = $(strGetBox);

							$( updateTableMini ).dialog({
								autoOpen: false,
								height: 300,
								width: 400,
								modal: true,
								buttons: {
								
									/*"Get Code": function() {
										var gcodex = $("#gcode-update").val();
										var gdescpx = $("#gdescp-update").val();
										$("#ad_companycode").val(gcodex);
										$("#ad_companyname").val(gdescpx);
										$(this).dialog("destroy");
									},
									Cancel: function() {
										$(this).dialog("destroy");
										$( this ).dialog( "close" );
									}*/
								}
							});

							$( updateTableMini ).dialog( "open" );
							
							/*$( "#keyword-update" ).autocomplete({
								source: "get_buyer.jsp",
								minLength: 2,
								select: function( event, ui ) {
									loga( ui.item ?
										ui.item.id :
										"Nothing selected, input was " + this.value );
									logi( ui.item ?
										ui.item.value :
										"Nothing selected, input was " + this.value );
								}
							});

			*/			
							$( "#keyword-insert" ).keyup(function() {

								var newsupp = $('#keyword-insert').val();
								var where2 = " where estatecode='"+estcode+"' and suppname like '%"+newsupp+"%' or suppcode like '%"+newsupp+"%' order by suppname";
								var exString2 = 'table='+table+'&'+strx;


								var returnsuppbox = '<table id="supplier_output">';
								//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';

								$.ajax({
									url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
									type:'POST',
									async: false,
									data: exString2,
									success: function(output){
										var jGet = [];
										var i = 0;
										var obja = $.parseJSON(output);
						            		$.each(obja, function() {
						            			
						            			
						            			for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
													jGet[i] = this[colx[i]];
												}
												returnsuppbox += '<tr><td class="selected_td" style="cursor:hand">'+jGet[0]+' - '+jGet[1];
												returnsuppbox += '<input type="hidden" value="'+jGet[0]+'" id="suppcode'+jGet[0]+'">';
												returnsuppbox += '<input type="hidden" value="'+jGet[1]+'" id="suppname'+jGet[0]+'">';
												returnsuppbox += '<input type="hidden" value="'+jGet[2]+'" id="suppaddress'+jGet[0]+'">';
												returnsuppbox += '<input type="hidden" value="'+jGet[3]+'" id="suppacode'+jGet[0]+'">';
												returnsuppbox += '<input type="hidden" value="'+jGet[4]+'" id="suppdesc'+jGet[0]+'">';
												returnsuppbox += '</td></tr>';
											});
										}	
								});	
								returnsuppbox += '';
								returnsuppbox += '</table>';

								$('#supplier_output').html(returnsuppbox);
							});

						});

						$(document).on('click','.selected_td', function(){
							var m = $(this).html();
							var v = m.substring(0, 4);
							var aVal = $('#suppcode'+v).val();
							var bVal = $('#suppname'+v).val();
							var cVal = $('#suppaddress'+v).val();
							var dVal = $('#suppacode'+v).val();
							var eVal = $('#suppdesc'+v).val();

							$("#ad_suppcode").val(aVal);
							$("#ad_suppname").val(bVal);
							$("#ad_suppaddress").val(cVal);
							$("#ad_accode").val(dVal);
							$("#ad_acdesc").val(eVal);
							$( "#get_supplier_box" ).remove();


						    var table_in = 'ph_supplier_detail';
							var colx_in = ['matcode', 'matdesc' ];
							var strx_in = 'cols=';
							for (j = 0; j < colx_in.length; ++j) {
								strx_in+= colx_in[j]+'&cols=';
							}

							var selectmat = '<select id="ad_typedesc">';
							selectmat += '<option value="">Please Select</option>';
							var where_in = " where suppcode='"+v+"'";
							var exString_in = 'table='+table_in+'&'+strx_in;
						    $.ajax({
								url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where_in),
								type:'POST',
								async: false,
								data: exString_in,
								success: function(output){
									var jGet_in = [];
									var i = 0;
									var obja = $.parseJSON(output);
									$.each(obja, function() {
										            			
										for (i = 0; i < colx_in.length; ++i) {//put value from database to variable jget
											jGet_in[i] = this[colx_in[i]];
																	
										}
										selectmat += '<option value="'+jGet_in[0]+'">'+jGet_in[1]+'</option>';
										//alert(jGet_in[1]);
										
									});	
								}
							
							});

							selectmat += '</select>';

							$( "#ad_typedesc" ).replaceWith( selectmat );
							$( "#ad_typecode" ).val( "" );


						});
		        	}

		        	function assignAs(tdCode, tdDesc) {

		        		
						var listDB = "vendor_type";
						var listFields = [ "ven_table", "ven_param", "ven_desc" ];
						var listID = "ven_type";
						var listItem = 'cols=';
						var paramto = 'vendor';
						var aTitle = 'Assign ';

						for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
							listItem += listFields[m]+'&cols=';
						}


						var listString = 'identifier='+listID+'&'+listID+'='+paramto+'&table='+listDB+'&'+listItem;
						$.ajax({
							url: 'get_data.jsp',
							type:'POST',
							async: false,
							data: listString,
							success: function(output){
								var jGet = [];
								var jCek = []; 
								var x = 0;
								var j = 0;
								var str = '<div id="dialog-form-update" title="'+aTitle+' for '+tdDesc+' ('+tdCode+')"><table id="table_2" cellspacing="0">';
									str += '<tr><td colspan="3" class="bd_bottom" align="left"><strong>Click on the subject to assign vendor.</strong></td>';
								var obja = $.parseJSON(output);
				            		$.each(obja, function() {
				            			
				            			var bool = '';
				            			for (i = 0; i < listFields.length; ++i) {//put value from database to variable jget
											jGet[i] = this[listFields[i]];
										

											var listString_a = 'identifier=code&code='+tdCode+'&table='+jGet[0];
											$.ajax({
												url: 'get_bool.jsp',
												type:'POST',
												async: false,
												data: listString_a,
												success: function(output){
													var obja = $.parseJSON(output);
									            		$.each(obja, function() {
															bool = this['bool'];
															//alert(bool);
														});

													
												}
											});
										}
										if(bool != 1){
											x++;
											j++;
											str += '<tr id="temp"><td colspan="3" width="100%" class="select_td" align="left" style="cursor:hand">'+jGet[2]+'</td></tr>';
											//str += '<td width="30%" class="bd_bottom" align="right"><div id="result"></div></td></tr>';
										}
									});
								if(x == 0){
									str += '<tr id="allassign"><td colspan="3" width="100%" class="bd_bottom" align="left"><img src="image/icon/1388246235_OK.png" width="16" style="vertical-align:middle">&nbsp;&nbsp;This vendor has been assigned for all type.</td></tr>';
								}
								str += '<tr><td colspan="3" width="100%" class="bd_bottom" align="left">&nbsp;</td></tr>';
								str += '<tr id="emptyTr"><td colspan="3" width="100%" class="bd_bottom" align="left" ></td></tr>';
								str += '</table>';
								str += '<input type="hidden" id="jVal" name="jVal" value="'+j+'">';
								//if(j < 5 ){
									str += '<br><table id="table_3" width="100%" cellspacing="0">';
									str += '<tr id="tete"><td colspan="3" class="bd_bottom" align="left"><strong>Assigned To</strong><div id="sese"></div></td>';
									var cnt = 0;
										$.each(obja, function() {
											for (i = 0; i < listFields.length; ++i) {
												jGet[i] = this[listFields[i]];
											}

											//alert(getBool(tdCode,jGet[0]));
											var listFields_a = [ "coa", "coadescp" ];
											var listItem_a = 'cols=';
											var paramto = 'vendor';
											var aTitle = 'Assign ';
											var listID_a = "code";

											for (m = 0; m < listFields_a.length; ++m) {//put value from database to variable jget
												listItem_a += listFields_a[m]+'&cols=';
											}

											var listString_b = 'identifier='+listID_a+'&'+listID_a+'='+tdCode+'&table='+jGet[0]+'&'+listItem_a;
											
											$.ajax({
												url: 'get_data.jsp',
												type:'POST',
												async: false,
												data: listString_b,
												success: function(output){
													
													var jGet_a = [];
													var obja = $.parseJSON(output);
									            		$.each(obja, function() {
									            			cnt++;
															for (i = 0; i < listFields_a.length; ++i) {//put value from database to variable jget
																jGet_a[i] = this[listFields_a[i]];
															}
															
															str += '<tr id="tete"><td width="20%" id="newTd" class="bd_bottom" align="left">'+jGet_a[0]+'</td><td id="newTd" width="60%" class="bd_bottom" align="left">'+jGet_a[1]+'</td><td id="newTd" width="60%" class="bd_bottom" align="left"><img src="image/icon/1388246235_OK.png" width="16" style="vertical-align:middle">&nbsp;&nbsp;Assigned</td></tr>';

														});

													

													
												}

											});

											
											//alert(jGet[0]);
										});
										if(cnt == 0){
												str += '<tr id="notyet"><td colspan="3" width="100%" id="newTd" class="bd_bottom" align="left"><img src="image/icon/info.png" width="16" style="vertical-align:middle">&nbsp;&nbsp;Not Yet Assigned</td></tr>';
										}

									str += '</table>';
								//}
								str += '</div>';
								var viewTable = $(str);

				            		$(viewTable).dialog({
				            			autoOpen: false,
										height: 550,
										width: 700,
										modal: true,
										buttons: {
		  									"Ok": function() {
											    $(this).dialog("destroy");
											    location.reload();
											},
											Cancel: function() {
												$(this).dialog("destroy");
												location.reload();
											}
										}
					            	});

    							$(viewTable).dialog("open");
							}
						});
						
						
						$(document).on('click','.select_td', function(){
							var y = $(this);
							var m = $(this).html();
							var jVal = $('#jVal').val();
       						//alert(m);
       						var viewConfirm = '<div id="dialog-form-update" title="Confirm Assign">Are you sure to assign this vendor as <strong>'+m+'</strong>?</div>';

       						var tutu = $(viewConfirm);
       						$(tutu).dialog({
				            			autoOpen: false,
										modal: true,
										buttons: {
		  									"Ok": function() {

		  										$(y).fadeOut('slow');
					       						//***************call page for assigning process**********************************
					       						var dataToSend = 'vcode='+tdCode+'&type='+m;
					       						$.ajax({
													type:'POST',
													data:dataToSend,
													url:'cf_vendor_assign_process.jsp',
													success:function(response) {
														var assCode = '';
														var assDesc = '';
														if(m == 'Supplier'){
															assCode = "130101";
															assDesc = "TRADE CREDITORS";
														}else if(m == 'Contractor'){
															assCode = "131101";
															assDesc = "CONTRACTORS";
														}else if(m == 'Buyer'){
															assCode = "126101";
															assDesc = "SALES";
														}else if(m == 'Deposit Receivable'){
															assCode = "121101";
															assDesc = "DEPOSIT RECEIVABLE";
														}else if(m == 'Deposit Payable'){
															assCode = "133101";
															assDesc = "DEPOSIT PAYABLE";
														}
														
														var emptyTr = $('#table_3');
														var toAppend = '';

														if(jVal == 5){//belum assign lagi
															toAppend = '<tr id="tete"><td width="20%" id="newTd" class="bd_bottom" align="left">'+assCode+'</td><td id="newTd" width="60%" class="bd_bottom" align="left">'+assDesc+'</td><td id="newTd" width="60%" class="bd_bottom" align="left"><img src="image/icon/1388246235_OK.png" width="16" style="vertical-align:middle">&nbsp;&nbsp;Assigned</td></tr>';
															//$(toAppend).hide().fadeIn(1000);
															$('#notyet').remove();
															$(emptyTr).append(toAppend).hide().fadeIn(1000);
															
														}else if(jVal < 5){//dah assign
															toAppend = '<tr id="tete"><td width="20%" id="newTd" class="bd_bottom" align="left">'+assCode+'</td><td id="newTd" width="60%" class="bd_bottom" align="left">'+assDesc+'</td><td id="newTd" width="60%" class="bd_bottom" align="left"><img src="image/icon/1388246235_OK.png" width="16" style="vertical-align:middle">&nbsp;&nbsp;Assigned</td></tr>';
															//$('#tete').after(toAppend).hide().fadeIn(1000);
															$('#tete').after('<tr id="tete"><td width="20%" id="newTd" class="bd_bottom" align="left">'+assCode+'</td><td id="newTd" width="60%" class="bd_bottom" align="left">'+assDesc+'</td><td id="newTd" width="60%" class="bd_bottom" align="left"><img src="image/icon/1388246235_OK.png" width="16" style="vertical-align:middle">&nbsp;&nbsp;Assigned</td></tr>');
															$('#tete').hide().fadeIn('slow');

														//	toAppend = '<br><table id="table_3" width="100%" cellspacing="0">';
														//	toAppend += '<tr><td width="20%" id="newTd" class="bd_bottom" align="left">'+assCode+'</td><td id="newTd" width="60%" class="bd_bottom" align="left">'+assDesc+'</td><td id="newTd" width="60%" class="bd_bottom" align="left">Assigned</td></tr>';
														//	toAppend += '</table>';
														//	$('#table_2').hide().after(toAppend).fadeIn(1000);
														}
														
														//$(toAppend).appendTo(emptyTr).hide().fadeIn(1000);
													}
												});

												//***************end*************************************************************
											    $(this).dialog("destroy");
											},
											Cancel: function() {
												$(this).dialog("destroy");
											}
										}
					            	});
							$(tutu).dialog("open");

       						
       						


			        	});

						$(document).on('mouseenter','.select_td', function(){
							$(this).toggleClass('highlight');
       						
						});
						$(document).on('mouseleave','.select_td', function(){
							$(this).toggleClass('highlight');
       						
						});

						function getBool(tdCode, tdTable){
							var bool = '';
							var listString_a = 'identifier=code&code='+tdCode+'&table='+tdTable;
							$.ajax({
								url: 'get_bool.jsp',
								type:'POST',
								async: false,
								data: listString_a,
								success: function(output){
									var obja = $.parseJSON(output);
									$.each(obja, function() {
										bool = this['bool'];
															//alert(bool);
									});

													
								}
							});

							return bool;
						}
                    }
}