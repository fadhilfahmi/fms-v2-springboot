/* 
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
window.scrollTo(0, 0);
//var currenttab = sessionStorage.getItem(1);

        function dateChanged(ev) {
        $(this).datepicker('hide');
                var a = $(this).val();
                var y = a.substring(0, 4);
                var p = a.substring(5, 7);
                var pshort = a.substring(5, 6);
                console.log(pshort);
                if (pshort == 0) {
        p = a.substring(6, 7)
        }

        $('#year').val(y);
                $('#period').val(p);
                }

function callPnotify(status, text) {
var statusNotify = status;
        var textP = text;
        var titlex = '';
        var text = '';
        if (statusNotify == 'success') {

titlex = 'Successful!';
        new PNotify({
        title: titlex,
                text: textP,
                type: 'success',
                styling: 'bootstrap3'
        });
} else if (statusNotify == 'info') {

titlex = 'Info';
        new PNotify({
        title: titlex,
                text: textP,
                type: 'info',
                styling: 'bootstrap3'
        });
} else if (statusNotify == 'failed') {

titlex = 'Alert';
        new PNotify({
        title: titlex,
                text: textP,
                type: 'error',
                styling: 'bootstrap3'
        });
}
}

$('#gotoreferral').click(function () {
var a = $(this).attr('name');
        var b = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + b + "&process=editlist&referno=" + a,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(100);
                }
        });
        return false;
        });
//$('body').on('click', '#backto', function (e) {
        $("#backto").click(function () {
                var path = $(this).attr('title'); //moduleid
                $('#herey').empty().load(path+'.html').hide().fadeIn(300);
                        /*var process = $(this).attr('name');
                        var refer = $(this).attr('type');
                        var q = '';
                        if (refer != null) {
                q = '&referno=' + refer;
                }

                var urlx = "";
                        if (process == 'frommodal') {

                var token = refer.split('/');
                        //token[0] = "part1"
                        //token[1] = "part2
                        urlx = "PathController?moduleid=040101&process=" + process + "&tomodule=" + path + "&status=" + refer;
                } else {
                urlx = "PathController?moduleid=" + path + "&process=" + process + q;
                }

                $.ajax({
                url: urlx,
                        success: function (result) {
                        // $("#haha").html(result);
                        if (process == 'frommodal') {
                        $('.summary-content').empty().html(result).hide().fadeIn(100);
                        } else {
                        $('#herey').empty().html(result).hide().fadeIn(100);
                        }

                        }
                });*/
        return false;
        });

        $("#savebutton").unbind('click').bind('click', function (e) {
                var path = $(this).attr('title');
                var process = $(this).attr('name');
                var a = $("#saveform :input").serialize();
                $('#maincontainer').empty().html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(300);

            $.post("/api/setup/configuration/coa/add", a, function(data, textStatus) {
                //data contains the JSON object
                //textStatus contains the status: success, error, etc
                console.log(data);
                if(textStatus=='success'){
                    $('#herey').empty().load(path+'.html').hide().fadeIn(300);
                }
            }, "json");

                /*$.ajax({
                async: true,
                        data: a,
                        type: 'POST',
                        url: "PathController?moduleid=" + path + "&process=" + process,
                        success: function (result) {
                        $('#maincontainer').empty().html(result).hide().fadeIn(100);
                        }
                });*/
                //}

                e.stopPropagation();
                return false;
        });

        $('.getaccount').click(function (e) {
var a = $(this).attr('id');
        var b = $(this).attr('id1');
        //e.preventDefault();
        e.stopImmediatePropagation();
        e.preventDefault();
        if (e.handled !== true) { //Checking for the event whether it has occurred or not.
e.handled = true; // Basically setting value that the current event has occurred.
        BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Account',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                console.log('testa');
                        var $content = $('<body></body>').load('list_coa.jsp?code=' + a + '&descp=' + b);
                        $('body').on('click', '.thisresult', function (event) {
                dialog.close();
                });
                        $('body').on('click', '.gotocoa', function (event) {
                console.log('test');
                        $.ajax({
                        url: "PathController?moduleid=010109&process=viewlist",
                                success: function (result) {
                                // $("#haha").html(result);
                                $('#herey').empty().html(result).hide().fadeIn(300);
                                }
                        });
                        dialog.close();
                        return false;
                        //event.preventDefault();

                });
                        return $content;
                }
        });
}
return false;
        });
        $('.getmaterial').click(function (e) {
var a = $(this).attr('id');
        var b = $(this).attr('id1');
        //e.preventDefault();
        e.stopImmediatePropagation();
        e.preventDefault();
        if (e.handled !== true) { //Checking for the event whether it has occurred or not.
e.handled = true; // Basically setting value that the current event has occurred.
        BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Material',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                console.log('testa');
                        var $content = $('<body></body>').load('list_com.jsp?code=' + a + '&descp=' + b);
                        $('body').on('click', '.thisresult', function (event) {
                dialog.close();
                });
                        $('body').on('click', '.gotocom', function (event) {
                console.log('test');
                        $.ajax({
                        url: "PathController?moduleid=010110&process=viewlist",
                                success: function (result) {
                                // $("#haha").html(result);
                                $('#herey').empty().html(result).hide().fadeIn(300);
                                }
                        });
                        dialog.close();
                        return false;
                        //event.preventDefault();

                });
                        return $content;
                }
        });
}
return false;
        });
        $('body').on('click', '.gotoaddsupplier', function (e) {

$.ajax({
url: "PathController?moduleid=010118&process=addnew",
        success: function (result) {
        // $("#haha").html(result);
        $('#34d160c5-9c28-46fd-aeac-bce027b2b3f3').modal('toggle');
                $('#herey').empty().html(result).hide().fadeIn(300);
        }
});
        return false;
        });
        function numberOnly(str) {
        return /^ *[0-9]+ *$/.test(str);
                }

function printElement(data)
        {
        var mywindow = window.open('', 'Print Voucher', 'height=600,width=900');
                mywindow.document.write('<html><head><title>Print from FMS</title>');
                mywindow.document.write('<link href="bower_components/bootstrap.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('<link rel="stylesheet" href="css/pvoucher.css?1" type="text/css" />');
                mywindow.document.write('<link rel="stylesheet" href="css/table_style.css?1" type="text/css" />');
                mywindow.document.write('<link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                //mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                setTimeout(function () {
                mywindow.print();
                        mywindow.close();
                }, 500);
                return true;
                }

function printLandscape(data)
        {
        var mywindow = window.open('', 'Print Voucher', 'height=600,width=900');
                mywindow.document.write('<html><head><title>Print from FMS</title>');
                mywindow.document.write('<link href="bower_components/bootstrap.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('<link rel="stylesheet" href="css/print_landscape.css?1" type="text/css" />');
                mywindow.document.write('<link rel="stylesheet" href="css/table_style.css?1" type="text/css" />');
                mywindow.document.write('<link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                //mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                setTimeout(function () {
                mywindow.print();
                        mywindow.close();
                }, 500);
                return true;
                }

function printCheque(data)
        {
        var mywindow = window.open('', 'Print Cheque', 'height=600,width=900');
                mywindow.document.write('<html><head><title>Print from FMS</title>');
                mywindow.document.write('<link href="bower_components/bootstrap.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('<link rel="stylesheet" href="css/pcheque.css?2" type="text/css" />');
                mywindow.document.write('<link rel="stylesheet" href="css/table_style.css" type="text/css" />');
                mywindow.document.write('<link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                //mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                setTimeout(function () {
                mywindow.print();
                        mywindow.close();
                }, 500);
                return true;
                }

function printReport(data)
        {
        var mywindow = window.open('', 'Print Voucher', 'height=600,width=900');
                mywindow.document.write('<html><head><title>Print from FMS</title>');
                mywindow.document.write('<link rel="stylesheet" href="css/table_style.css" type="text/css" />');
                mywindow.document.write('<link href="bower_components/bootstrap.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('<link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                //mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                setTimeout(function () {
                mywindow.print();
                        mywindow.close();
                }, 500);
                return true;
                }

$(document).ready(function () {

window.scrollTo(0, 0);
        $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        })

        $('.activerowm').click(function (e) {
e.preventDefault();
        $(".togglerow").toggle();
        //$(this).toggleClass('info');
        //$("#togglerow_nd"+b).toggleClass('warning');
});
        $('.activerowx').click(function (e) {
e.preventDefault();
        var b = $(this).attr('id');
        $("#togglerow_nd" + b).toggle();
        $(this).toggleClass('info');
        $("#togglerow_nd" + b).toggleClass('warning');
});
        $('.activerowy').click(function (e) {
e.preventDefault();
        var b = $(this).attr('id');
        $("#togglerow_st" + b).toggle();
        $(this).toggleClass('info');
        $("#togglerow_st" + b).toggleClass('warning');
});
        $(".cancel").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        BootstrapDialog.confirm({
        title: '<span style="color:#337ab7;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                message: 'You are about to cancel this, proceed?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelClass: 'btn-primary',
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Proceed', // <-- Default value is 'OK',
                btnOKClass: 'btn-primary', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                $.ajax({
                url: "PathController?moduleid=" + moduleid + "&process=cancel&refer=" + refer,
                        success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                });
                }
                }
        });
        return false;
});
        $(".viewgl").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        $('#maincontainer').empty().html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(300);
        $.ajax({
        url: "PathController?moduleid=000000&process=viewgl&modulefrom=" + moduleid + "&refer=" + refer,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".replicate").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        BootstrapDialog.confirm({
        title: '<span style="color:#337ab7;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                //message: 'Are you sure to replicate this Vendor Invoice?',
                message: $('<div></div>').load('remote_dateperiod.jsp'),
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelClass: 'btn-primary',
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Replicate', // <-- Default value is 'OK',
                btnOKClass: 'btn-primary', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {

                var newdate = $('#date').val();
                        var newyear = $('#year').val();
                        var newperiod = $('#period').val();
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        if (result) {

                $.ajax({
                url: "PathController?moduleid=" + moduleid + "&process=replicate&refer=" + refer + "&replicatedate=" + newdate + "&replicateyear=" + newyear + "&replicateperiod=" + newperiod,
                        success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                });
                }
                }
        });
        return false;
});
        $(".createnote").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        BootstrapDialog.confirm({
        title: '<span style="color:#337ab7;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                message: 'You are about to generate Debit/Credit Note, proceed?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelClass: 'btn-primary',
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Generate Debit/Credit Note', // <-- Default value is 'OK',
                btnOKClass: 'btn-primary', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                $.ajax({
                url: "PathController?moduleid=" + moduleid + "&process=createnote&refer=" + refer,
                        success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                });
                }
                }
        });
        return false;
});
        $(".approve").click(function () {
//var a = $("form").serialize();
var a = $(this).attr('id');
        var b = $(this).attr('title');
        BootstrapDialog.confirm({
        title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                $.ajax({
                url: "PathController?moduleid=" + b + "&process=dist&referno=" + a,
                        success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                });
                }
                }
        });
        return false;
});
        $(".check").click(function (e) {
var id = $(this).attr('id');
        var moduleid = $(this).attr('title');
        e.preventDefault();
        $.ajax({
        url: "PathController?moduleid=" + moduleid + "&process=check&referno=" + id,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".deletemain").click(function () {
var a = $(this).attr('id');
        var b = $(this).attr('title');
        BootstrapDialog.confirm({
        title: 'Confirmation',
                message: 'All datas will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                $.ajax({
                url: "PathController?moduleid=" + b + "&process=delete&referno=" + a,
                        success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                });
                }
                }
        });
        return false;
});
        $('.viewvoucher').click(function (e) {
var a = $(this).attr('id');
        var b = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + b + "&process=viewprinted&refer=" + a,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".editmain").click(function () {
                var a = $(this).attr('id');
                var b = $(this).attr('title');
            $('#herey').empty().load(b+'.html').hide().fadeIn(300);

                return false;
        });
        $(".edititem").click(function () {
var a = $(this).attr('id');
        var b = $(this).attr('name');
        var c = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + c + "&process=" + b + "&referno=" + a,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".deleteitem").click(function () {
var a = $(this).attr('id');
        var b = $(this).attr('name');
        var c = $(this).attr('title');
        BootstrapDialog.confirm({
        title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                $.ajax({
                url: "PathController?moduleid=" + c + "&process=" + b + "&referno=" + a,
                        success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                });
                }
                }
        });
        return false;
});
        $(".addcredit").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + moduleid + "&process=addcredit&refer=" + refer,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".addaccount").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + moduleid + "&process=addaccount&refer=" + refer,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".addmaterial").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + moduleid + "&process=addmaterial&refer=" + refer,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".addreceipt").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + moduleid + "&process=addreceipt&refer=" + refer,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".adddebitnote").click(function (e) {
//e.preventDefault();
var referno = $(this).attr('id');
        var moduleid = $(this).attr('title');
        BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Debit Note',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {


                var $content = $('<body></body>').load('list_ardebitnote.jsp?referno=' + referno);
                        $('body').on('click', '.thisresult-debitnote', function (event) {
                event.preventDefault();
                        dialog.close();
                });
                        return $content;
                }
        });
        return false;
});
        $(".addcreditnote").click(function (e) {
//e.preventDefault();
var referno = $(this).attr('id');
        var moduleid = $(this).attr('title');
        BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Credit Note',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {


                var $content = $('<body></body>').load('list_arcreditnote.jsp?referno=' + referno);
                        $('body').on('click', '.thisresult-creditnote', function (event) {
                event.preventDefault();
                        dialog.close();
                });
                        return $content;
                }
        });
        return false;
});
        $(".addcheque").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('id');
        var moduleid = $(this).attr('title');
        $.ajax({
        url: "PathController?moduleid=" + moduleid + "&process=addcheque&refer=" + refer,
                success: function (result) {
                $('#herey').empty().html(result).hide().fadeIn(300);
                }
        });
        return false;
});
        $(".gotonote").click(function (e) {
e.preventDefault();
        var refer = $(this).attr('title');
        var referno = $(this).attr('id');
        var moduleid = $(this).attr('name');
        var typenote = refer.substring(0, 3);
        var moduleidnote = '';
        if (typenote == 'CNE') {
moduleidnote = '020109';
}

if (typenote == 'DNE') {
moduleidnote = '020108';
}


$.ajax({
url: "PathController?moduleid=" + moduleidnote + "&process=viewfromother&referno=" + refer + "&frommodule=" + moduleid + "&topath=editlist&referother=" + referno,
        success: function (result) {
        $('#herey').empty().html(result).hide().fadeIn(300);
        }
});
        return false;
});
        $('#gettype').click(function (e) {
//e.preventDefault();

var a = 'paidcode';
        var b = 'paidname';
        var c = 'paidaddress';
        var d = 'paidpostcode';
        var e = 'paidcity';
        var f = 'paidstate';
        var g = 'gstid';
        BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Paid Type',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                var $content = $('<body></body>').load('list_type.jsp?code=' + a + '&name=' + b + '&address=' + c + '&postcode=' + d + '&city=' + e + '&state=' + f + '&gstid=' + g);
                        $('body').on('click', '.thisresult_nd', function (event) {
                dialog.close();
                        event.preventDefault();
                });
                        return $content;
                }
        });
        return false;
});
        $('#getbank').click(function (e) {

var a = 'bankcode';
        var b = 'bankname';
        //e.preventDefault();
        BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Bank',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                        $('body').on('click', '.thisresult', function (event) {
                dialog.close();
                        event.preventDefault();
                });
                        return $content;
                }
        });
        return false;
});




                        });


              