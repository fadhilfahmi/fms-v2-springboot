module.exports = {
    entry: {
        jquery: './jquery-1.12.4.js',
        bootstrap: './bootstrap.min.js'
    },
    output: {
        path: __dirname,
        filename: "bundle.js"
    }//,
//    module: {
//        loaders: [
//            { test: /\.css$/, loader: "style!css" }
//        ]
//    }
};