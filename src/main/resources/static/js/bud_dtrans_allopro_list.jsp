<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%Connection con = (Connection)session.getAttribute("con");%>
<%@ page import="java.text.*,java.util.*" %>
<%@ page import="java.util.Date"%>
<%response.setHeader("Cache-Control", "no-cache");
SimpleDateFormat sdf=new SimpleDateFormat("dd.MM.yyyy");
%>
<html>
<head>	
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<script language="JavaScript" >
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// Example: obj = findObj("image1");
function findObj(theObj, theDoc)
{
  var p, i, foundObj;
  
  if(!theDoc) theDoc = document;
  if( (p = theObj.indexOf("?")) > 0 && parent.frames.length)
  {
    theDoc = parent.frames[theObj.substring(p+1)].document;
    theObj = theObj.substring(0,p);
  }
  if(!(foundObj = theDoc[theObj]) && theDoc.all) foundObj = theDoc.all[theObj];
  for (i=0; !foundObj && i < theDoc.forms.length; i++) 
    foundObj = theDoc.forms[i][theObj];
  for(i=0; !foundObj && theDoc.layers && i < theDoc.layers.length; i++) 
    foundObj = findObj(theObj,theDoc.layers[i].document);
  if(!foundObj && document.getElementById) foundObj = document.getElementById(theObj);
  
  return foundObj;
}
// * Dependencies * 
// this function requires the following snippets:
// JavaScript/readable_MM_functions/findObj
//
// Accepts a variable number of arguments, in triplets as follows:
// arg 1: simple name of a layer object, such as "Layer1"
// arg 2: ignored (for backward compatibility)
// arg 3: 'hide' or 'show'
// repeat...
//
// Example: showHideLayers(Layer1,'','show',Layer2,'','hide');
function showHideLayers()
{ 
  var i, visStr, obj, args = showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  {
    if ((obj = findObj(args[i])) != null)
    {
      visStr = args[i+2];
      if (obj.style)
      {
        obj = obj.style;
        if(visStr == 'show') visStr = 'visible';
        else if(visStr == 'hide') visStr = 'hidden';
      }
      obj.visibility = visStr;
    }
  }
}
/* 
Example:
function test()
{
  if (document.layers) getMouseLoc;     //NS
  else if (document.all) getMouseLoc(); //IE
  alert(mouseLocation.x+","+mouseLocation.y);
}
in the BODY:
<a href="#" onmouseover="test()">test</a>
*/

//-->
// * Dependencies * 
// this function requires the following snippets:
// JavaScript/readable_MM_functions/findObj
// JavaScript/readable_MM_functions/showHideLayers
// JavaScript/events/getMouseLoc
function moveLayerToMouseLoc(theLayer, offsetH, offsetV)
{
  var obj;
  if ((findObj(theLayer))!=null)
  {
    if (document.layers)  //NS
    {
      document.onMouseMove = getMouseLoc;
      obj = document.layers[theLayer];
      obj.left = mLoc.x +offsetH;
      obj.top  = mLoc.y +offsetV;
    }
    else if (document.all)//IE
    {
      getMouseLoc();
      obj = document.all[theLayer].style;
      obj.pixelLeft = mLoc.x +offsetH;
      obj.pixelTop  = mLoc.y +offsetV;
	  obj.left=offsetV;
    }
    showHideLayers(theLayer,'','show');
  }
}
// get mouse location
function Point(x,y) {  this.x = x; this.y = y; }
mLoc = new Point(-500,-500);
function getMouseLoc(e)
{
  if(!document.all)  //NS
  {
    mLoc.x = e.pageX;
    mLoc.y = e.pageY;
  }
  else               //IE
  {
    mLoc.x = event.x + document.body.scrollLeft;
    mLoc.y = event.y + document.body.scrollTop;
  }
  return true;
}
//NS init:
if(document.layers){ document.captureEvents(Event.MOUSEMOVE); document.onMouseMove = getMouseLoc; }

function begin_confirm(code,y)
{
	var result = confirm("Are You Sure To Delete ?");
	
	if (result == true){
		location.href = "bud_dtrans_allopro_process.jsp?flag=delete&year="+y+"&code="+code;
	}else{
		return false;
	}

}
</script>
<body bgcolor="#FFFFFF" onScroll="moveLayerToMouseLoc('Layer1', document.scrollTop+10, 10)">
<form action="bud_dtrans_allopro_add.jsp?flag=add" method="post" name="form1">
  <div align="center">
    <br>
    <h2>ALLOCATION OF PROPORTIONATE CHARGES</h2>
  </div>
  <table width="80%" align="center">
  <tr>
  	  <td><font size="2" face="Arial, Helvetica, sans-serif"> 
        <input type="submit" name="addgce" value="Add" style="font-size:11px;width:60px">
        </font></td>
  </tr>
  </table>
  <table width="80%" border="1" align="center" cellpadding="1" cellspacing="0" class="tborderblack">
    <tr > 
      <td width="15%" height="25" class="upheader"> <div align="center"><strong>Estate 
          Code </strong></div></td>
      <td class="upheader"> <div align="center"><strong>Estate Description</strong></div></td>
      <td class="upheader" width="15%"><div align="center"><strong>Year</strong></div></td>
      <td class="upheader" width="25%"><div align="center"><strong>Allocation of <br>
          Proportionate Charges (%)</strong></div></td>
      <td width="8%" class="upheader"> <div align="center"><strong>Edit</strong></div></td>
      <td width="8%" class="upheader"> <div align="center"><strong>Delete</strong></div></td>
    </tr>
    <%String estate = (String)session.getAttribute("estateid");
	DecimalFormat df=new DecimalFormat("###,###,###,##0.00");
int b=0;
Statement stmt=con.createStatement();
ResultSet set=stmt.executeQuery("select * from bud_dtrans_allopro order by year desc,estcode");
if(set!=null){
while(set.next()){b++;
String code=set.getString("estcode");
String name=set.getString("estname");
String year=set.getString("year");
double per=set.getDouble("per");
String dis="";String h="ko";
	//out.println("select * from bud_app where estcode like '"+estate+"' and year='"+year+"'");
	Statement bud = con.createStatement();
	ResultSet bud2 = bud.executeQuery("select * from bud_app where '"+estate+"' like concat(estcode,'%') and year='"+year+"'");
	if(bud2.next())
	{
	  h="ok";
	 
	}
	
	if(h.equals("ok")) {dis="disabled";}else if(h.equals("ko")){dis="";}

%>
    <tr> 
      <td><div align="center"><%=code%></div></td>
      <td><%=name%>
        <div align="right"></div></td>
      <td><div align="center"><%=year%></div></td>
      <td><div align="center"><%=df.format(per)%></div></td>
      <td><div align="center"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp; 
          <input type="button"  <%=dis%> name="edit22" value=". . ." onClick="location.href='bud_dtrans_allopro_add.jsp?flag=edit&year=<%=year%>'">
          </font></div></td>
      <td height="25"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif"> 
          <input type="button" <%=dis%>  name="delete" value=". . ." onClick="begin_confirm('<%=code%>','<%=year%>')">
          &nbsp; </font></div></td>
    </tr>
    <%}}%>
  </table>

  <p align="center"><font size="2" face="Arial, Helvetica, sans-serif"> 
    <input type="button" name="buttonBack" value="Back" style="font-size:11px;width:70px" onClick="location.href='bud_dtrans_list.jsp'">
    </font></p>
</form>
</body>
</html>
