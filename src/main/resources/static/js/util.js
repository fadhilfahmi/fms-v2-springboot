// JavaScript Document

var popUpWin=0;
function popUpWindow(URLStr)
{
  if(popUpWin)
  {
    if(!popUpWin.closed) popUpWin.close();
  }
  popUpWin = open(URLStr, 'popUpWin', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars,resizable=yes,copyhistory=yes,width=800,height=250');
}

var popUpWinV=0;
function popUpWindowV(URLStr)
{
  if(popUpWinV)
  {
    if(!popUpWinV.closed) popUpWinV.close();
  }
  popUpWinV = open(URLStr, 'popUpWin', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars,resizable=yes,copyhistory=yes,width=800,height=700');
}
function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value, decimal_places) {

    // Convert the number to a string
    var value_string = rounded_value.toString()
    
    // Locate the decimal point
    var decimal_location = value_string.indexOf(".")

    // Is there a decimal point?
    if (decimal_location == -1) {
        
        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0
        
        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : ""
    }
    else {

        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1
    }
    
    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length
    
    if (pad_total > 0) {
        
        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++) 
            value_string += "0"
        }
    return value_string
}

function printSetup()
    {
    var a = document.all.item("noprint");


        if (a!=null) {


            if (a.length!=null) {
            //multiple tags found


                for (i=0; i< a.length; i++) {
                a(i).style.display = window.event.type == "beforeprint" ? "none" :"inline";
            }
        } else 
        //only one tag
        a.style.display = window.event.type == "beforeprint" ? "none" :"inline";
    }
}

function round_this(rval,rpl)
{
	rpl=parseFloat(rpl);
	rval=parseFloat(rval);
	var p=Math.pow(10,rpl);
	rval=rval*p;
	var tmp=Math.round(rval);
	return tmp/p;
}

// Convert DIGIT to SENTENCE - START
function case1(number) {
	if (number == 1) { return "ONE"; }
	else if (number == 2) { return "TWO"; }
	else if (number == 3) { return "THREE"; }
	else if (number == 4) { return "FOUR"; }
	else if (number == 5) { return "FIVE"; }
	else if (number == 6) { return "SIX"; }
	else if (number == 7) { return "SEVEN"; }
	else if (number == 8) { return "EIGHT"; }
	else if (number == 9) { return "NINE"; }
}

function case2(number) {
	if (number == 11) { return "ELEVEN"; }
	else if (number == 12) { return "TWELVE"; }
	else if (number == 13) { return "THIRTEEN"; }
	else if (number == 14) { return "FOURTEEN"; }
	else if (number == 15) { return "FIFTEEN"; }
	else if (number == 16) { return "SIXTEEN"; }
	else if (number == 17) { return "SEVENTEEN"; }
	else if (number == 18) { return "EIGHTEEN"; }
	else if (number == 19) { return "NINETEEN"; }
}

function case3(number) {
	if (number == 10) { return "TEN"; }
	else if (number == 20) { return "TWENTY"; }
	else if (number == 30) { return "THIRTY"; }
	else if (number == 40) { return "FORTY"; }
	else if (number == 50) { return "FIFTY"; }
	else if (number == 60) { return "SIXTY"; }
	else if (number == 70) { return "SEVENTY"; }
	else if (number == 80) { return "EIGHTY"; }
	else if (number == 90) { return "NINETY"; }
}

function identifyCase(number) {
	if (number < 10) { return 1; }
	else if (number < 100) { return 2; }
	else if (number < 1000) { return 3; }
	else if (number < 10000) { return 4; }
	else if (number < 100000) { return 5; } 
	else if (number < 1000000) { return 6; }
	else if (number < 10000000) { return 7; }
	else if (number < 100000000) { return 8; }
	else if (number < 1000000000) { return 9; }	
}

function scnddigit(digit) {
	//var number = number.substring(1);
	var number = digit.toString().substr(1);
	if (number == 1) { return "ONE"; }
	else if (number == 2) { return "TWO"; }
	else if (number == 3) { return "THREE"; }
	else if (number == 4) { return "FOUR"; }
	else if (number == 5) { return "FIVE"; }
	else if (number == 6) { return "SIX"; }
	else if (number == 7) { return "SEVEN"; }
	else if (number == 8) { return "EIGHT"; }
	else if (number == 9) { return "NINE"; }
}

function case4(digit) {
	var number = digit.toString().substr(0, 1);
	if (number == 1) { return "TEN"; }
	else if (number == 2) { return "TWENTY"; }
	else if (number == 3) { return "THIRTY"; }
	else if (number == 4) { return "FORTY"; }
	else if (number == 5) { return "FIFTY"; }
	else if (number == 6) { return "SIXTY"; }
	else if (number == 7) { return "SEVENTY"; }
	else if (number == 8) { return "EIGHTY"; }
	else if (number == 9) { return "NINETY"; }
}
function convertRM(input,output) {
	var amount = document.getElementById(input).value;
	 //amount1 = parseFloat(document.getElementById(input).value);
	 amount=round_decimals(round_this(parseFloat(amount),2),2);
	 document.getElementById(input).value = amount
	try
	{
		var digit = round_decimals(round_this(parseFloat(amount),2),2);
	}
	catch(ex) { alert("Conversion Exception!") }
	
	if (digit < 0) { alert("Negatif Exception") }
	else if (digit == 0) { document.getElementById(output).value = "NONE"; }
	else {
		var cent = Math.round(round_decimals(digit % 1, 2) * 100);
		var rm = round_decimals(digit - round_decimals(digit % 1, 2), 0);
		
		//identify RM
		var caseRM = identifyCase(rm);
		var strRM = "";
		if (rm != 0) {		
		while (caseRM != 0) {
			if (caseRM == 8) {
				var rmTemp = Math.floor(rm / 1000000);
				if (rmTemp > 10 && rmTemp < 20) { strRM = strRM + case2(rmTemp) + " MILLION"; }
				//else{ strRM = case4(rmTemp) + " " + scnddigit(rmTemp) + " MILLION";}
				//else{ strRM = case4(rmTemp) + "  MILLION";}


				else { 
					var rmTemp1 = rmTemp;
					
					var rmTemp2 = Math.floor(rmTemp1 / 10) * 10;
					//alert(rmTemp1 +" "+ rmTemp2)
					strRM = strRM + case3(rmTemp2);
					rmTemp1 -= rmTemp2;
					if (rmTemp1 == 0) { strRM = strRM + " MILLION"; }
					else { strRM = strRM + " " + case1(rmTemp1) + " MILLION"; }
				}				
				
				
				rm -= (rmTemp * 1000000);
				if (rm == 0) { caseRM = 1; }
				else { 
					strRM = strRM + " ";
					if (identifyCase(rm) == 5) { caseRM = 6; }
					else if (identifyCase(rm) == 4) { caseRM = 5; }
					else if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			//////////////////////////////////////////////////////////
			}
			else			
			if (caseRM == 7) {
				var rmTemp = Math.floor(rm / 1000000);
				strRM = strRM + case1(rmTemp) + " MILLION";
				rm -= (rmTemp * 1000000);
				if (rm == 0) { caseRM = 1; }
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 5) { caseRM = 6; }
					else if (identifyCase(rm) == 4) { caseRM = 5; }
					else if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 6) {
				var rmTemp = Math.floor(rm / 100000);
				strRM = strRM + case1(rmTemp) + " HUNDRED";
				rm -= (rmTemp * 100000);
				if (rm == 0) { caseRM = 1;strRM += " THOUSAND"; }
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 5) { caseRM = 6; }
					else if (identifyCase(rm) == 4) { caseRM = 5; }
					else if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 5) {
				var rmTemp = Math.floor(rm / 1000);
				if (rmTemp > 10 && rmTemp < 20) { strRM = strRM + case2(rmTemp) + " THOUSAND"; }
				else { 
					var rmTemp1 = rmTemp;
					var rmTemp2 = Math.floor(rmTemp1 / 10) * 10;
					strRM = strRM + case3(rmTemp2);
					rmTemp1 -= rmTemp2;
					if (rmTemp1 == 0) { strRM = strRM + " THOUSAND"; }
					else { strRM = strRM + " " + case1(rmTemp1) + " THOUSAND"; }
				}
				rm -= (rmTemp * 1000);
				if (rm == 0) { caseRM = 1; }
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}			
			else if (caseRM == 4) {
				var rmTemp = Math.floor(rm / 1000);
				strRM = strRM + case1(rmTemp) + " THOUSAND";
				rm -= (rmTemp * 1000);
				if (rm == 0) { 
					 document.getElementById(output).value = strRM;
					caseRM = 1; 
				}
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 3) {
				var rmTemp = Math.floor(rm / 100);
				strRM = strRM + case1(rmTemp) + " HUNDRED";
				rm -= (rmTemp * 100);
				if (rm == 0) { 
					 document.getElementById(output).value = strRM;
					caseRM = 1; 
				}
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 2) {
				if (rm > 10 && rm < 20) {
					strRM = strRM + case2(rm);
					caseRM --;
				}
				else {
					var rmTemp = Math.floor(rm / 10) * 10;
					strRM = strRM + case3(rmTemp);
					rm -= rmTemp;
					if (rm == 0) { caseRM --; }
					else { strRM = strRM + " "; }
				}
			}
			else if (caseRM == 1) { strRM = strRM + case1(rm); }
			caseRM --;
		}
		}
		
		//identify CENT
		if (cent == 0) {  document.getElementById(output).value = strRM + " ONLY"; } 
		else {
			if (strRM != "")
				strRM = strRM + " AND "
			var caseCENT = identifyCase(cent);
			while (caseCENT != 0) {
				if (caseCENT == 2) {
					if (cent > 10 && cent < 20) {
						 document.getElementById(output).value = strRM + case2(cent) + " CENT ONLY";
						caseCENT --;
					}
					else {
						var centTemp = Math.floor(cent / 10) * 10;
						strRM = strRM + case3(centTemp);
						cent -= centTemp;
						if (cent == 0) { 
							 document.getElementById(output).value = strRM + " CENT ONLY";
							caseCENT --; 
						}
						else { strRM = strRM + " "; }
					}
				}
				else if (caseCENT == 1) {  document.getElementById(output).value = strRM + case1(cent) + " CENT ONLY"; }
				caseCENT --;			
			}
		}	
	}
}// Convert DIGIT to SENTENCE - END