/*
   Deluxe Menu Data File
   Created by Deluxe Tuner v2.4
   http://deluxe-menu.com
*/


// -- Deluxe Tuner Style Names
var itemStylesNames=[];
var menuStylesNames=["Style1",];
// -- End of Deluxe Tuner Style Names

//--- Common
var isHorizontal=1;
var smColumns=1;
var smOrientation=0;
var smViewType=0;
var dmRTL=0;
var pressedItem=0;
var itemCursor="default";
var itemTarget="_blank";
var statusString="link";
var blankImage="images/blank.gif";

//--- Dimensions
var menuWidth="";
var menuHeight="";
var smWidth="";
var smHeight="";

//--- Positioning
var absolutePos=0;
var posX="0";
var posY="0";
var topDX=0;
var topDY=1;
var DX=1;
var DY=0;

//--- Font
var fontStyle="normal 11px Tahoma";
var fontColor=["#2D1E33","#FFFFFF"];
var fontDecoration=["none","none"];
var fontColorDisabled="#AAAAAA";

//--- Appearance
var menuBackColor="#EFBEEE";
var menuBackImage="";
var menuBackRepeat="repeat";
var menuBorderColor="";
var menuBorderWidth=0;
var menuBorderStyle="solid";

//--- Item Appearance
var itemBackColor=["#EFBEEE","#D248CF"];
var itemBackImage=["",""];
var itemBorderWidth=1;
var itemBorderColor=["#EFBEEE","#FFFFFF"];
var itemBorderStyle=["solid","solid"];
var itemSpacing=0;
var itemPadding="6px 10px 6px 10px";
var itemAlignTop="left";
var itemAlign="left";
var subMenuAlign="";

//--- Icons
var iconTopWidth=24;
var iconTopHeight=24;
var iconWidth=30;
var iconHeight=15;
var arrowWidth=11;
var arrowHeight=11;
var arrowImageMain=["arrow_main3.gif","arrow_main4.gif"];
var arrowImageSub=["arrow_sub5.gif","arrow_sub5.gif"];

//--- Separators
var separatorImage="";
var separatorWidth="100%";
var separatorHeight="3";
var separatorAlignment="left";
var separatorVImage="";
var separatorVWidth="3";
var separatorVHeight="100%";
var separatorPadding="0px";

//--- Floatable Menu
var floatable=0;
var floatIterations=6;
var floatableX=1;
var floatableY=1;

//--- Movable Menu
var movable=0;
var moveWidth=12;
var moveHeight=20;
var moveColor="#AA0000";
var moveImage="";
var moveCursor="default";
var smMovable=0;
var closeBtnW=15;
var closeBtnH=15;
var closeBtn="";

//--- Transitional Effects & Filters
var transparency="75";
var transition=35;
var transOptions="gridSizeX=5, gridSizeY=5";
var transDuration=300;
var transDuration2=200;
var shadowLen=5;
var shadowColor="#FC77FF";
var shadowTop=0;

//--- CSS Support (CSS-based Menu)
var cssStyle=0;
var cssSubmenu="";
var cssItem=["",""];
var cssItemText=["",""];

//--- Advanced
var dmObjectsCheck=0;
var saveNavigationPath=1;
var showByClick=0;
var noWrap=1;
var pathPrefix_img="images/";
var pathPrefix_link="";
var smShowPause=200;
var smHidePause=1000;
var smSmartScroll=1;
var topSmartScroll=0;
var smHideOnClick=1;
var dm_writeAll=0;

//--- AJAX-like Technology
var dmAJAX=0;
var dmAJAXCount=0;

//--- Dynamic Menu
var dynamic=0;

//--- Keystrokes Support
var keystrokes=1;
var dm_focus=1;
var dm_actKey=113;


var menuItems = [

    ["Home","testlink.htm"],
    ["Product Info",""],
        ["|Features","testlink.htm"],
        ["|Installation",""],
            ["||Description of Files","testlink.htm"],
            ["||How To Setup","testlink.htm"],
        ["|Dynamic Functions","testlink.htm"],
        ["|Params Info","testlink.htm"],
        ["|Supported Browsers",""],
            ["||Windows OS",""],
                ["|||Internet Explorer",""],
                ["|||Firefox",""],
                ["|||Mozilla",""],
                ["|||Opera",""],
                ["|||Netscape Navigator",""],
            ["||MAC OS",""],
                ["|||Firefox",""],
                ["|||Safari",""],
                ["|||Internet Explorer",""],
            ["||Unix/Linux OS",""],
                ["|||Firefox",""],
                ["|||Konqueror",""],
    ["Samples",""],
        ["|Sample 1","testlink.htm"],
        ["|Sample 2","testlink.htm"],
        ["|Sample 3","testlink.htm"],
        ["|Sample 4","testlink.htm"],
        ["|Sample 5","testlink.htm"],
        ["|Sample 6","testlink.htm"],
    ["Purchase","testlink.htm"],
    ["Contact Us","testlink.htm"],
];

dm_initFrame("frmSet", 0, 1, 0);
