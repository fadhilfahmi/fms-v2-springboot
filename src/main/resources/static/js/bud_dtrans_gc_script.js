function calculateTop1(x)
{

	if(x=='actual')
	{
		calculateActual();
	}	
	else if(x=='average')
	{
		calculateAverage();	
	}
}

function calculateActual()
{
	var bilBln=parseInt(document.getElementById("pjg").value)	//bil bulan	
	var amt=parseFloat(document.getElementById("amt").value)	//amount
	var locbil=parseInt(document.getElementById("locbil").value);	//bil estate
	document.getElementById("amt_month").value=amt; //amount montly
	document.getElementById("amt_year").value=round_this(amt*bilBln,0); //amount year
	document.getElementById("amt_varian").value=0; // varians - if applicable
	
	var valmth=0;
	var total=0;
	var totalp=0;
	var maxValue=0;
	var maxPos=0;
	
//get max position n adjustment	
	for(var loc=1;loc<=locbil;loc++)
	{		
		var perc=parseFloat(document.getElementById("pc_"+loc).value);
		totalp+=perc;
		var valmth=round_this(perc/100*amt,0);
		
		document.getElementById("lval_"+loc).value=valmth;
		if(valmth>maxValue)
		{
			maxValue=valmth;
			maxPos=loc;	
		}
		total+=valmth;
		
		if(loc==locbil)
		{
			var beza=round_this(amt-total,0);
			if(beza!=0)
			{
				maxValue+=beza;	
				if(maxPos==0)maxPos=1;
				document.getElementById("lval_"+maxPos).value=maxValue;
				total+beza;
			}
		}		
	}
	
	//set total amount n percent
	document.getElementById("tlval").value=total;
	document.getElementById("tpc").value=totalp

//set total ha for every estate	
	var tha_all=0;
	for(var loc=1;loc<=locbil;loc++)
	{
		var tha=0;
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			var haf=parseFloat(document.getElementById("haf_"+loc+"_"+fbil).value);
			tha+=haf;	
		}
		tha_all+=tha;
		document.getElementById("tha_"+loc).value=round_this(tha,2);		
	}	
	document.getElementById("tha").value=round_this(tha_all,2);
//iterate every estate

	var tamt_all=0;
	for(var loc=1;loc<=locbil;loc++)
	{
		var tha=parseFloat(document.getElementById("tha_"+loc).value);
		var duitest=parseInt(document.getElementById("lval_"+loc).value);
		maxValue=0;
		maxPos=0;
		var tamt=0;
		//total=0;
		//set amount utk setiap field
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			var haf=parseFloat(document.getElementById("haf_"+loc+"_"+fbil).value);
			var amt=round_this(haf/tha*duitest,0);
			tamt+=amt;
			document.getElementById("amtf_"+loc+"_"+fbil).value=amt;
			
			if(amt>maxValue)
			{
				maxValue=amt;
				maxPos=fbil;	
			}	
			
			if(fbil==fcnt)				
			{
				var beza=round_this(duitest-tamt,0);
				if(beza!=0)
				{
					//alert(maxPos)
					if((beza<0)&&((maxValue+beza)<0))
					{
						for(var fbilx=1;fbilx<=fcnt;fbilx++)
						{
							var val=parseFloat(document.getElementById("amtf_"+loc+"_"+fbilx).value);	
							if(val>0)
							{
								var kk=val+beza;
								if(kk<0)
								{
									document.getElementById("amtf_"+loc+"_"+fbilx).value=(val-1);
									beza+=1;
								}				
								else 
								{
									document.getElementById("amtf_"+loc+"_"+fbilx).value=kk;
									break;
								}
							}
						}
					}
					else
					{
						maxValue+=beza;	
						if(maxPos==0)maxPos=1;
						document.getElementById("amtf_"+loc+"_"+maxPos).value=maxValue;
						tamt+=beza;									
					}
				}
			}
		}
		tamt_all+=tamt;
		document.getElementById("tamt_"+loc).value=round_this(tamt,0);			
	}
	document.getElementById("tamt").value=round_this(tamt_all,0);
	
	for(var bln=1;bln<=bilBln;bln++)
	{
		//document.getElementById("toha_"+bln).value=document.getElementById("tha").value;
		document.getElementById("mthamt_"+bln).value=amt;
		//document.getElementById("tosamt_"+bln).value=document.getElementById("tamt").value	
		for(var loc=1;loc<=locbil;loc++)
		{
			document.getElementById("gua_"+bln+"_"+loc).value=document.getElementById("lval_"+loc).value;
			//document.getElementById("endha_"+bln+"_"+loc).value=document.getElementById("tha_"+loc).value;
			//document.getElementById("subamt_"+bln+"_"+loc).value=document.getElementById("tamt_"+loc).value;
			var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
			for(var fbil=1;fbil<=fcnt;fbil++)
			{
				document.getElementById("amt_"+bln+"_"+loc+"_"+fbil).value=document.getElementById("amtf_"+loc+"_"+fbil).value;									
			}
		}
	}
	//document.getElementById("grandTotal").value=document.getElementById("amt_year").value
	
		sumAll();
}

function sumAll()
{
	var bilBln=parseInt(document.getElementById("pjg").value)	//bil bulan	
	var locbil=parseInt(document.getElementById("locbil").value);	//bil estate
	
	var ttha=0;
	var ttamt=0;
	var ttpc=0;
	var ttval=0;
	for(var loc=1;loc<=locbil;loc++)
	{	
		var thaEst=0;
		var tamtEst=0;
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			tamtEst+=parseInt(document.getElementById("amtf_"+loc+"_"+fbil).value);	
			thaEst+=parseFloat(document.getElementById("haf_"+loc+"_"+fbil).value);	
		}
		thaEst=round_this(thaEst,2);
		ttha+=thaEst;
		ttamt+=tamtEst;	
		document.getElementById("tha_"+loc).value=thaEst;
		document.getElementById("tamt_"+loc).value=tamtEst;	
		ttpc+=parseFloat(document.getElementById("pc_"+loc).value);
		ttval+=parseInt(document.getElementById("lval_"+loc).value);	
	}
	
	ttha=round_this(ttha,2);
	ttpc=round_this(ttpc,2);
	document.getElementById("tha").value=ttha;
	document.getElementById("tamt").value=ttamt;
	document.getElementById("tpc").value=ttpc;
	document.getElementById("tlval").value=ttval;	
	
	
	var totalAll=0;	
	for(var bln=1;bln<=bilBln;bln++)
	{
		var totalAmtPeriod=0;
		var totalHaPeriod=0;
		for(var loc=1;loc<=locbil;loc++)
		{
			var totalAmtPeriodEst=0;
			var totalHaPeriodEst=0;
			var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
			for(var fbil=1;fbil<=fcnt;fbil++)
			{
				totalAmtPeriodEst+=parseInt(document.getElementById("amt_"+bln+"_"+loc+"_"+fbil).value)
				totalHaPeriodEst+=parseFloat(document.getElementById("ha_"+bln+"_"+loc+"_"+fbil).value)
			}
			
			totalHaPeriod+=totalHaPeriodEst;
			totalHaPeriod=round_this(totalHaPeriod,2);
			document.getElementById("endha_"+bln+"_"+loc).value=totalHaPeriod;					
			//totalAmtPeriodEst=totalAmtPeriodEst;
			totalAmtPeriod+=totalAmtPeriodEst;
			document.getElementById("subamt_"+bln+"_"+loc).value=totalAmtPeriodEst;					
		}
		
		totalHaPeriod=round_this(totalHaPeriod,2);
		document.getElementById("toha_"+bln).value=totalHaPeriod;
		//totalAmtPeriod=round_this(totalAmtPeriod,2);
		totalAll+=totalAmtPeriod;
		document.getElementById("tosamt_"+bln).value=totalAmtPeriod;
	}	
	//totalAll=round_this(totalAll,2);
	document.getElementById("grandTotal").value=totalAll;
}


function calculateAverage()
{
	var bilBln=parseInt(document.getElementById("pjg").value)	//bil bulan	
	var amt=parseFloat(document.getElementById("amt").value)	//amount
	var locbil=parseInt(document.getElementById("locbil").value);	//bil estate
	document.getElementById("amt_month").value=round_this(Math.floor(amt/bilBln),0);   
	// amount montly
	document.getElementById("amt_year").value=amt; //amount year
	document.getElementById("amt_varian").value=round_this(amt-(parseInt(document.getElementById("amt_month").value)*bilBln),0); // varians - if applicable	
	
	//amt=parseInt(document.getElementById("amt_month").value);
	var valmth=0;
	var total=0;
	var totalp=0;
	var maxValue=0;
	var maxPos=0;
	
//get max position n adjustment	
	for(var loc=1;loc<=locbil;loc++)
	{		
		var perc=parseFloat(document.getElementById("pc_"+loc).value);
		totalp+=perc;
		var valmth=round_this(perc/100*amt,0);
		
		document.getElementById("lval_"+loc).value=valmth;
		if(valmth>maxValue)
		{
			maxValue=valmth;
			maxPos=loc;	
		}
		total+=valmth;
		
		if(loc==locbil)
		{
			var beza=round_this(amt-total,0);
			if(beza!=0)
			{
				maxValue+=beza;	
				if(maxPos==0)maxPos=1;
				document.getElementById("lval_"+maxPos).value=maxValue;
				total+beza;
			}
		}		
	}
	
	//set total amount n percent
	document.getElementById("tlval").value=total;
	document.getElementById("tpc").value=totalp

//set total ha for every estate	
	var tha_all=0;
	for(var loc=1;loc<=locbil;loc++)
	{
		var tha=0;
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			var haf=parseFloat(document.getElementById("haf_"+loc+"_"+fbil).value);
			tha+=haf;	
		}
		tha_all+=tha;
		document.getElementById("tha_"+loc).value=round_this(tha,2);		
	}	
	document.getElementById("tha").value=round_this(tha_all,2);
	
//iterate every estate

	var tamt_all=0;
	for(var loc=1;loc<=locbil;loc++)
	{
		var tha=parseFloat(document.getElementById("tha_"+loc).value);
		var duitest=parseInt(document.getElementById("lval_"+loc).value);
		maxValue=0;
		maxPos=0;
		var tamt=0;
		//total=0;
		//set amount utk setiap field
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			var haf=parseFloat(document.getElementById("haf_"+loc+"_"+fbil).value);
			var amt=round_this(haf/tha*duitest,0);
			tamt+=amt;
			document.getElementById("amtf_"+loc+"_"+fbil).value=amt;
			
			if(amt>maxValue)
			{
				maxValue=amt;
				maxPos=fbil;	
			}	
			
			if(fbil==fcnt)				
			{
				var beza=round_this(duitest-tamt,0);
				if(beza!=0)
				{
					//alert(maxPos)
					if((beza<0)&&((maxValue+beza)<0))
					{
						for(var fbilx=1;fbilx<=fcnt;fbilx++)
						{
							var val=parseFloat(document.getElementById("amtf_"+loc+"_"+fbilx).value);	
							if(val>0)
							{
								var kk=val+beza;
								if(kk<0)
								{
									document.getElementById("amtf_"+loc+"_"+fbilx).value=(val-1);
									beza+=1;
								}				
								else 
								{
									document.getElementById("amtf_"+loc+"_"+fbilx).value=kk;
									break;
								}
							}
						}
					}
					else
					{
						maxValue+=beza;	
						if(maxPos==0)maxPos=1;
						document.getElementById("amtf_"+loc+"_"+maxPos).value=maxValue;
						tamt+=beza;									
					}
				}
			}
		}
		tamt_all+=tamt;
		document.getElementById("tamt_"+loc).value=round_this(tamt,0);			
	}
	document.getElementById("tamt").value=round_this(tamt_all,0);

	for(var loc=1;loc<=locbil;loc++)
	{
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			var vx=parseInt(document.getElementById("amtf_"+loc+"_"+fbil).value);
			var vmod=vx%bilBln;
			var vdist=(vx-vmod)/bilBln;
			document.getElementById("dist_"+loc+"_"+fbil).value=vdist;
			document.getElementById("varian_"+loc+"_"+fbil).value=vmod;
		}		
	}
	
	for(var bln=1;bln<=bilBln;bln++)
	{
		//document.getElementById("toha_"+bln).value=document.getElementById("tha").value;
		//document.getElementById("mthamt_"+bln).value=amt;
		//document.getElementById("tosamt_"+bln).value=document.getElementById("tamt").value	
		for(var loc=1;loc<=locbil;loc++)
		{
			//document.getElementById("gua_"+bln+"_"+loc).value=document.getElementById("lval_"+loc).value;
			//document.getElementById("endha_"+bln+"_"+loc).value=document.getElementById("tha_"+loc).value;
			//document.getElementById("subamt_"+bln+"_"+loc).value=document.getElementById("tamt_"+loc).value;
			var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
			for(var fbil=1;fbil<=fcnt;fbil++)
			{
				document.getElementById("amt_"+bln+"_"+loc+"_"+fbil).value=document.getElementById("dist_"+loc+"_"+fbil).value;									
			}
		}
	}
//	document.getElementById("grandTotal").value=document.getElementById("amt_year").value

	for(var loc=1;loc<=locbil;loc++)
	{
		var fcnt=parseInt(document.getElementById("fcnt_"+loc).value);		
		for(var fbil=1;fbil<=fcnt;fbil++)
		{
			var fvar=parseInt(document.getElementById("varian_"+loc+"_"+fbil).value);
			if(fvar>0)
			for(var bln=1;bln<=fvar;bln++)
			{
				var vamt=parseInt(document.getElementById("amt_"+bln+"_"+loc+"_"+fbil).value);
				vamt+=1;
				document.getElementById("amt_"+bln+"_"+loc+"_"+fbil).value=vamt;
			}
			//document.getElementById("amt_"+bln+"_"+loc+"_"+fbil).value=document.getElementById("dist_"+loc+"_"+fbil).value;									
		}			
	}
	
	sumAll();
}
