/* 
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */


$( "#backto" ).click(function() {
    var path = $(this).attr('title');
    var process = $(this).attr('name');
    var refer = $(this).attr('type');
    
    var q = '';
    if(refer!=null){
        q = '&referno='+refer;
    }
    $.ajax({
        url: "PathController?moduleid="+path+"&process="+process+q,
        success: function (result) {
            // $("#haha").html(result);
            $('#herey').empty().html(result).hide().fadeIn(300);
        }
    });
                    return false;
});

$( "#savebutton" ).click(function() {
    //var a = $("form").serialize();
    var path = $(this).attr('title');
    var process = $(this).attr('name');
    var a = $("#saveform :input").serialize();
    //checkField();
    //var b = checkField();
    //if(b==false){
        $.ajax({
            data: a,
            type: 'POST',
            url: "PathController?moduleid="+path+"&process="+process,
            success: function (result) {
                $('#maincontainer').empty().html(result).hide().fadeIn(300);
            }
        });
    //}    
    return false;
});

$( "#refresh" ).click(function() {
   oTable.fnClearTable(); 
    return false;
});


function numberOnly(str){
    return /^ *[0-9]+ *$/.test(str);
}


              