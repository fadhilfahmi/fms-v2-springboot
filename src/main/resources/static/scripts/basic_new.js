/* 
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */


var updateTitles = [ ];//7
var updateFields = [ ];//15
var yesno = [ "Yes", "No" ];  
var activeNo = [ "Active", "Not Active" ];  
var updateFieldtype = [ ];
var updateFieldbtparam = [ ];
var updateFieldvalue=[ ];
var columnName = [ ];
var columntoView = [ ];
var updateReq = [ ];//compulsory field
var regexValid = [ ];//check for numeric only
var updateFieldparam = [ ];
var dateCol = [ ];
var updateFieldsql = [ ];
var updateID = "";
var autorefer = [ ];
var autovoucher = [ ];
var getinfo = [ ];
var basecolumn = [ ];
var getcodecolumn = [ ];
var getdesccolumn = [ ];
var getothercolumn =[];
var gettable = [ ];
var gettablename1 = [ ];
var gettablename2 = [ ];
var gettablename3 = [ ];

var sub_updateTitles=[];
var sub_updateFields=[];
var sub_updateFieldtype=[];
var sub_updateFieldbtparam=[];
var sub_updateFieldvalue=[];
var sub_updateFieldparam=[];
var sub_updateFieldsql=[];
var sub_updateReq=[];
var sub_regexValid=[];
var sub_dateCol=[];
var sub_autorefer=[];
var sub_autovoucher=[];
var sub_getinfo=[];
var sub_basecolumn=[];
var sub_getcodecolumn=[];
var sub_getdesccolumn=[];
var sub_getothercolumn=[];
var sub_gettable=[];
var sub_gettablename1=[];
var sub_gettablename2=[];
var sub_gettablename3=[];

function invx(k){
    alert(k);
}
function updateTips( t ) {
	$("#updatetips").text( t );
	$("#updatetips").addClass( "ui-state-highlight" );
	//tips.text( t ).addClass( "ui-state-highlight" );
	//setTimeout(function() {
	//  $("#updatetips").removeClass( "ui-state-highlight", 1500 );
	//}, 500 );
			      
}
			 
function checkLength( o, x ) {
	var min = 1;
	if ( o.length <  min ) {
		$('#'+x).addClass("ui-state-error");
		//o.addClass( "ui-state-error" );
		//updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );
		//$('#errstat').val("1");
		return false;
	} else {
		$('#'+x).removeClass("ui-state-error");
		//$('#errstat').val("0");
		return true;
	}
}

function errorMsg(m,n){
	var t = '';
	if(m == 'regex'){
		t = ' <span class="ui-state-highlight" id="spanerr'+n+'">Numeric only!</span>';	
	}else if(m == 'length'){
		t = '';
	}
	
	
	return t;
}

function checkRegexp( o, x ) {
	//alert(x);
	var regexp = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
	$('#'+x).removeClass("ui-state-error");
	$('#spanerr'+x).remove();
	var err = errorMsg('regex',x);
	if ( !( regexp.test( o ) ) ) {
		$('#'+x).addClass( "ui-state-error" );
		if(o != ''){
			//$('#'+x).after( err );
		}
		
		$('#errstat').val("1");
		return false;
		//alert('asa');
	} else {
		$('#'+x).removeClass("ui-state-error");
		$('#spanerr').remove();
		$('#errstat').val("0");
		return true;
	}
}
				
 function addNew(updateTable,module_abb,year,period) {
		var newItem = "";
		var strItem = 'cols=';
		 for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
						strItem+= updateFields[j]+'&cols=';
		}

		
		var str = '<div id="dialog-form-update" title="Add New Information"><p class="validateTips">All form fields are required.</p><form><table id="table_2" cellspacing="0">';
		var i;

		for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
		  if(updateFieldtype[i] == 'hidden'){

			str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="hidden" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+'  />';

		  }else{

			
		  
		  str += '<tr><td width="30%" class="bd_bottom" align="left" valign="top">'+updateTitles[i]+'</td>';
		  str += '<td width="63%" class="bd_bottom" align="left">';

		  if(updateFieldtype[i] == "text"){
			var getbutton = '';
		
			if(getinfo[i]!='None'){
				getbutton = '<button class="getinfo" id="'+getinfo[i]+'" title="'+updateFields[i]+'">Get '+getinfo[i]+'</button>';
				getbutton += '<input type="hidden" id="basecolumn'+updateFields[i]+'" value="'+getcodecolumn[i]+'">';
				getbutton += '<input type="hidden" id="getcodecolumn'+updateFields[i]+'" value="'+getcodecolumn[i]+'">';
				getbutton += '<input type="hidden" id="getdesccolumn'+updateFields[i]+'" value="'+getdesccolumn[i]+'">';
				getbutton += '<input type="hidden" id="gettable'+updateFields[i]+'" value="'+gettable[i]+'">';
				getbutton += '<input type="hidden" id="gettablename1'+updateFields[i]+'" value="'+gettablename1[i]+'">';
				getbutton += '<input type="hidden" id="gettablename2'+updateFields[i]+'" value="'+gettablename2[i]+'">';
				
				//getbutton = '<input type="button" value="Get '+getinfo[i]+'" onClick="getinfox(\'buyer_info\',\''+code_col+'\',\'Buyer\','+toe+',\'\');" >';
				//getinfo('buyer_info',code_col,'Buyer',toe,'');//a = table, b= column in table, c=title, d=field name to effect	
			}
			
			
			if(updateFields[i]=='companyname'){

			  
			  /*var no = '';//column to retrieve the value
			  var table = 'ph_supplier';
			  var colx = ['suppcode', 'suppname', 'suppaddress', 'accode', 'acdesc' ];
			  var strx = 'cols=';
			  for (j = 0; j < colx.length; ++j) {
				  strx+= colx[j]+'&cols=';
			  }

			  var where = " where estatecode='<%=estate%>' order by suppname";
			  var exString1 = 'table='+table+'&'+strx;
			  $.ajax({
				url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
				type:'POST',
				async: false,
				data: exString1,
				success: function(output_string){
				  var obj = $.parseJSON(output_string);
				  $.each(obj, function() {
					  var mGet = [];
					  for (m = 0; m < colx.length; ++m) {
						mGet[m] = this[colx[m]];
					}
						str += '<option value="'+mGet[0]+'">'+mGet[0]+' - '+mGet[1]+'</option>';
					
				  });
				}
			  });*/

			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  /><input type="button" id="getcomp" value="Get Company">';
			}else if(updateFields[i]=='estatename'){
			  
			  updateFieldvalue[i]='<%=estatename%>';
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' />';
			}else if(updateFields[i]=='year'){
			  
			  updateFieldvalue[i]=year;
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  '+updateFieldbtparam[i]+'/>';
			}else if(updateFields[i]=='period'){
			  
			  updateFieldvalue[i]=period;
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' />';
			}else{
				
				
				
				//begin*************for generate automatic refer no***************
				if(autorefer[i]=='1'){
				 //---------------------------------
			
				  updateFieldvalue[i]=getReferenceNo(updateTable,module_abb,updateID);//getReferenceNo(table,abb,refertitle,colx)
				  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  '+updateFieldbtparam[i]+' />';
				  //end*************for generate automatic refer no*****************
				}else if(autovoucher[i]=='1'){
					updateFieldvalue[i]=getVoucherNo(updateTable,updateFields[1]);//getReferenceNo(table,abb,refertitle,colx)
				  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  '+updateFieldbtparam[i]+' />';
					
				}else{
				
			  		str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+'  />&nbsp;'+getbutton;
				}
			}
			

			



		  }else if(updateFieldtype[i] == "text - button"){

			str += '<input id="ad_'+updateFields[i]+'"  type="text" '+updateFieldparam[i]+' /></td><td> <button id="get-acctx">Get Account</button></td>';

		  }else if(updateFieldtype[i] == "combo - listdb"){

			str += '<select id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';

			
			var listDB = "parameter";
			var listFields = [ "parameter", "value" ];
			var listID = "aparameter";
			var listItem = 'cols=';
			
			for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
			  listItem += listFields[m]+'&cols=';
			}
			
			var listString = 'identifier='+listID+'&'+listID+'='+updateFieldsql[i]+'&table='+listDB+'&'+listItem;

			$.ajax({
			  url: 'get_data.jsp',
			  type:'POST',
			  async: false,
			  data: listString,
			  success: function(output){
				var obja = $.parseJSON(output);
						$.each(obja, function() {
					var paramt = this['parameter'];
					var valt = this['value'];
					str += '<option value"'+valt+'">'+valt+'</option>';
				  });
			  }
			});



			
			str += '</select>';
						  
		  }else if(updateFieldtype[i] == "combo - yesno"){

			str += '<select id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
			var selOpt = "";

			for (u = 0; u < yesno.length; ++u) {
			  str += '<option value"'+yesno[u]+'">'+yesno[u]+'</option>';
			}

			str += '</select>';

			
		  }else if(updateFieldtype[i] == "textarea"){

			str += '<textarea id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' rows="4" cols="50">';
			

			str += '</textarea>';

			
		  }
						
		  str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

		  }
		  

		}

		str += '</table></form></div>';

		var addTable = $(str);

		$(addTable).dialog({
			autoOpen: false,
		  height: 550,
		  width: 700,
		  modal: true,
		  buttons: {
			  "Save": function() {

			  var eachI = [];
			  var bValid = true;
			  var c = 0;
			  newItem = $('#ad_'+updateFields[1]).val(); 
			  var eachV = [];
				var idV = '';
				for (j = 0; j < updateFields.length; ++j) {
				if(updateReq[j] == 1){
					  //alert(updateFields[j]);

					  eachV[j]= $('#ad_'+updateFields[j]).val();

					  bValid = checkLength( eachV[j], 'ad_'+updateFields[j]);

					  if(bValid==false){
						c++;
					  }
				  

					if(regexValid[j] == 1){
					  
					  eachV[j]= $('#ad_'+updateFields[j]).val();
					  bValid = checkRegexp( eachV[j], 'ad_'+updateFields[j]);
					  if(bValid==false){
						c++;
					  }
					  
					

					}
					
				  }

				}
  
			   

			  if(c>0){
				$(this).addClass('ui-state-error');
			  }

			  if (c==0) {

				var s = 0;
				var upTitle = [];
				var dataStringUp = "";
				for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
				  upTitle[j] = $('#ad_'+updateFields[j]).val();

				  if(j == updateFields.length-1){
					dataStringUp += updateFields[j]+'='+upTitle[j];
				  }else{
					dataStringUp += updateFields[j]+'='+upTitle[j]+'&';
				  }
										  
				}
								 //alert(dataStringUp);
				dataStringUp=dataStringUp+'&table='+updateTable+'&'+strItem+'&flag=add';   
				console.log(dataStringUp);
				$.ajax({
					type:'POST',
					async:false,
					data:dataStringUp,
					url:'save_dataquery.jsp',
					success:function(data) {
					   
					   
						$(addTable).dialog("destroy");

				  successNoty( generalUsage , newItem, generalUsage+' Added', 'Successfull added ');
				  setTimeout(function(){
					  location.reload();}, 1000);
					},
						error: function(){

						   //alert('ggg');
						  //$(updateTable).dialog("destroy");
						 errorNoty('', 'Failed to save '+generalUsage+'!');
						}
				});
				 

				  
			   
				
			  }
			},
			Cancel: function() {
			  $(this).dialog("destroy");
			}
		  }
		});

		  $(addTable).dialog("open");

		  ///---------important to destroy box get
				  $('.ui-dialog-titlebar-close').on('click',function(){
					$(addTable).dialog("destroy");
					$('#get_supplier_box').remove();
				  }); 
				  ///---------important to destroy box get
		for (i = 0; i < updateFields.length; ++i) {
			if(dateCol[i]=='1'){
				$('#ad_'+updateFields[i]).datepicker({ dateFormat: 'yy-mm-dd' });
			}
			
		}
		

		$('#ad_total').on('focusout',function(){
			var x = $(this).val();
			
			var regexp = /^[0-9.,]+$/;
				if( x!='' ){
					if(!( regexp.test( x ) ) ){
						alert('Enter amount in numeric only!');
						$('#ad_amount').val('');
						$('#ad_total').val('');
					}else{
						convertRM(x);
					}  
				}else{
					$('#ad_amount').val('NONE');
					$('#ad_total').val('0.00');
				}


						
			
			
		}); 

		$('#ad_receiveid').on('focus',function(){
			var x = $('#ad_receivetype').val();
			var tbl = '';
			var col = [];
			var toe = [];
			var whe = '';
			if(x=='Executive'){
				tbl = 'executive';
				col = ['id','name'];
				tits = x;
				toe = ['ad_receiveid','ad_receivebyname'];
				whe = "where flag = 'Executive'";

			}else if(x=='Non - Executive'){
				tbl = 'executive';
				col = ['id','name'];
				tits = x;
				toe = ['ad_receiveid','ad_receivebyname'];
				whe = "where flag = 'Non-Executive'";

			}else if(x=='Contractor'){
				tbl = 'contractor_info';
				col = ['code','companyname'];
				tits = x;
				toe = ['ad_receiveid','ad_receivebyname'];
				whe = "";

			}else if(x=='Worker'){
				tbl = 'executive';
				col = ['id','name'];
				tits = x;
				toe = ['ad_receiveid','ad_receivebyname'];
				whe = "where flag = 'Worker' and status = 'Active'";

			}else if(x=='SNM Contractor'){
				tbl = 'snmcontractor';
				col = ['suppliercode','companyname'];
				tits = x;
				toe = ['ad_receiveid','ad_receivebyname'];
				whe = "";

			}

			getinfo(tbl,col,tits,toe,whe)//a = table, b= column in table, c=title, d=field name to effect
		}); 
		$('.getinfo').on('click',function(){
			var m = $(this).attr('title');
			var n = $(this).attr('id');
			var o = $("#getcodecolumn"+m).val();
			var p = $("#getdesccolumn"+m).val();
			var q = $("#gettable"+m).val();
			var r = $("#gettablename1"+m).val();
			var s = $("#gettablename2"+m).val();
			console.log(q);
			var code_col = [r,s];
			var toe = ['ad_'+o,'ad_'+p];
			if(m=='loccode'){
				getlocation(q,code_col,n,toe,'');
			}else{
				getinfox(q,code_col,n,toe,'');
			}
			
			return false;
		});
			 /* if(n.length == t.length){
				$(this).addClass('ui-state-error');
			  }*/
				//alert(n);
				//eachV= $(this).val();
				//idV= $(this).attr('id');
				
				//checkRegexp( eachV, idV);
				

		  /*$( ".getsomething" ).button({
				  icons: {
					primary: "ui-icon-circle-plus"
				  }
				});
  */

		  //---------validation----------
		  /*var eachV = '';
		  var idV = '';
		  for (j = 0; j < updateFields.length; ++j) {
		  if(updateReq[j] == 1){
			  $('#ad_'+updateFields[j]).on('focusout',function(){
				//alert(updateFields[j]);
				eachV= $(this).val();
				idV= $(this).attr('id');

				checkLength( eachV, idV);
				
			  });

			  
			  
			}

			if(regexValid[j] == 1){

				$('#ad_'+updateFields[j]).on('focus',function(){
				//alert(updateFields[j]);
				$(this).removeClass('ui-state-error');
				$("#spanerr").remove();
				
			  });

			  $('#ad_'+updateFields[j]).on('focusout',function(){
				//alert(updateFields[j]);
				eachV= $(this).val();
				idV= $(this).attr('id');
				
				checkRegexp( eachV, idV);
				
			  });

			  }
		  }
		  */
		  //special valid
			$('#ad_branchname').on('focusout',function(){
			  var n = $(this).val();
			  var t = $('#defaultabb').val();

			  if(n.length == t.length){
				$(this).addClass('ui-state-error');
			  }
				//alert(n);
				//eachV= $(this).val();
				//idV= $(this).attr('id');
				
				//checkRegexp( eachV, idV);
				
			  });
		  //------end of validation \

		//-----------get bank-----------------------

		
		$('#getlot').on('click',function(){
		  var table = 'estateinfo';
		  var colx = [ "estatecode","estatedescp" ];

		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = "";
		  var exString1 = 'table='+table+'&'+strx;

		  var strGetBox = '<div id="get_supplier_box" title="Get Location">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Location Code or Location Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';
		  
		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {

			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  var fieldform = [ "loccode","lot_Descp" ];

		  

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#ad_"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where estatecode like '%"+newacc+"%' or estatedescp like '%"+newacc+"%' order by estatecode";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';

			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#ad_"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
		});
		//---------get bank---------------

		//--------------------------get vendor---------------------------
		$('#getvendor').on('click',function(){
			var table = 'vendor_info';
		  var colx = [ "code","vendor_name","register_no","vendor_address","postcode","city","state","country","active","bumiputra","person","title","hp","phone","fax","email","url","position" ];

		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = "";
		  var exString1 = 'table='+table+'&'+strx;

		  var strGetBox = '<div id="get_supplier_box" title="Get Vendor">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Vendor Code or Vendor Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';


		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {
			
			  /*"Get Code": function() {
				var gcodex = $("#gcode-update").val();
				var gdescpx = $("#gdescp-update").val();
				$("#ad_companycode").val(gcodex);
				$("#ad_companyname").val(gdescpx);
				$(this).dialog("destroy");
			  },
			  Cancel: function() {
				$(this).dialog("destroy");
				$( this ).dialog( "close" );
			  }*/
			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  var fieldform = [ "code","companyname","coregister","bumiputra","person","title","position","hp","address","city","state","postcode","country","phone","fax","email","url" ];

		  var fromvendor = [ "code","vendor_name","register_no","bumiputra","person","title","position","hp","vendor_address","city","state","postcode","country","phone","fax","email","url" ];

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < fromvendor.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+fromvendor[i]+v).val();
				  
				  $("#ad_"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();


				});
			  }
		  
		  /*$( "#keyword-update" ).autocomplete({
			source: "get_buyer.jsp",
			minLength: 2,
			select: function( event, ui ) {
			  loga( ui.item ?
				ui.item.id :
				"Nothing selected, input was " + this.value );
			  logi( ui.item ?
				ui.item.value :
				"Nothing selected, input was " + this.value );
			}
		  });

  */      
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where code like '%"+newacc+"%' or vendor_name like '%"+newacc+"%' order by code";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < fromvendor.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+fromvendor[i]+v).val();
				  
				  $("#ad_"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
		});
		

		//-----------------------get account code-----------------------------------  
		$('.getacc').on('click',function(){

		  var where = '';
		  var where2 = '';
		  var tocd = '';
		  var tods = '';
		  var u = $(this).attr('id');

		  
		  if(u == 'yes'){
			where = " where active = 'Yes' and "+dicode+" order by code";
			tocd = 'depositcode';
			tods = 'depositdesc';
		  }else if(u == 'no'){
			where = " where active = 'Yes' order by code";
			tocd = 'coa';
			tods = 'coadescp';

		  }else if(u == 'suspense'){
			where = " where active = 'Yes' order by code";
			tocd = 'suspensecode';
			tods = 'suspensedesc';


		  }
		  var table = 'chartofacccount';
		  var colx = ['code', 'descp' ];
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  
		  var exString1 = 'table='+table+'&'+strx;

		  var strGetBox = '<div id="get_supplier_box" title="Get Account">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Account Code or Account Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					strGetBox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
					strGetBox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';


		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {
			
			  /*"Get Code": function() {
				var gcodex = $("#gcode-update").val();
				var gdescpx = $("#gdescp-update").val();
				$("#ad_companycode").val(gcodex);
				$("#ad_companyname").val(gdescpx);
				$(this).dialog("destroy");
			  },
			  Cancel: function() {
				$(this).dialog("destroy");
				$( this ).dialog( "close" );
			  }*/
			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				var aVal = $('#acccode'+v).val();
				var bVal = $('#accdesc'+v).val();
				$("#ad_"+tocd).val(aVal);
				$("#ad_"+tods).val(bVal);
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  /*$( "#keyword-update" ).autocomplete({
			source: "get_buyer.jsp",
			minLength: 2,
			select: function( event, ui ) {
			  loga( ui.item ?
				ui.item.id :
				"Nothing selected, input was " + this.value );
			  logi( ui.item ?
				ui.item.value :
				"Nothing selected, input was " + this.value );
			}
		  });

  */      
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();

			
			if(u == 'yes'){
			  where2 = " where "+dicode+" and (code like '%"+newacc+"%' or descp like '%"+newacc+"%')   order by code";
			}else if(u == 'no'){
			  where2 = " where code like '%"+newacc+"%' or descp like '%"+newacc+"%' order by code";

			}
			else if(u == 'suspense'){
			  where2 = " where code like '%"+newacc+"%' or descp like '%"+newacc+"%' order by code";

			}
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++;
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					returnsuppbox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
					returnsuppbox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
					returnsuppbox += '</td></tr>';
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				var aVal = $('#acccode'+v).val();
				var bVal = $('#accdesc'+v).val();

				$("#ad_"+tocd).val(aVal);
				$("#ad_"+tods).val(bVal);
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });

		});
  
		

		}
				
	function getBank2(a,b){
var table = 'info_bank';
          var colx = [ "code","name" ];

          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          var where = "";
          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Bank">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Vendor Code or Vendor Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';
          
          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {

            }
          });

          $( updateTableMini ).dialog( "open" );

          var fieldform = [ a,b ];

          

          for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();
            var where2 = " where code like '%"+newacc+"%' or name like '%"+newacc+"%' order by code";
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';

            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                         y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          });	
}			
function getBank(a,b){
var table = 'info_bank';
          var colx = [ "code","name" ];

          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          var where = "";
          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Bank">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Vendor Code or Vendor Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';
          
          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {

            }
          });

          $( updateTableMini ).dialog( "open" );

          var fieldform = [ a,b ];

          

          for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#ad_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();
            var where2 = " where code like '%"+newacc+"%' or name like '%"+newacc+"%' order by code";
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';

            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                         y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#ad_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          });	
}

function getinfox(a,b,c,d,e){//a = table, b= column in table, c=title, d=field name to effect

		var table = a;
		  var colx = b;
		  var titles = c;
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = " "+e;
		  var exString1 = 'table='+table+'&'+strx;
		console.log(d);
		  var strGetBox = '<div id="get_supplier_box" title="Get '+c+'">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the '+c+' Code or '+c+' Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" id="'+jGet[0]+'"><div class="rowget">'+jGet[0]+' - '+jGet[1]+'</div>';
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';
		  
		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {

			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  ///---------important to destroy box get
				$('.ui-dialog-titlebar-close').on('click',function(){
					$(updateTableMini).dialog("destroy");
				}); 
				///---------important to destroy box get

		  var fieldform = d;

		  

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where "+colx[0]+" like '%"+newacc+"%' or "+colx[1]+" like '%"+newacc+"%' order by "+colx[0]+"";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" id="'+jGet[0]+'"><div class="rowget">'+jGet[0]+' - '+jGet[1]+'</div>';
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
	}
	
 function getAccount(a,b){
			var where = '';
		  var where2 = '';
		  var tocd = a;
		  var tods = b;
		  var u = $(this).attr('id');

		  
		  if(u == 'yes'){
			where = " where active = 'Yes' and "+dicode+" order by code";
			tocd = 'depositcode';
			tods = 'depositdesc';
		  }else if(u == 'no'){
			where = " where active = 'Yes' order by code";
			tocd = 'coa';
			tods = 'coadescp';

		  }else{
			where = " where active = 'Yes' order by code";
		  }
		  var table = 'chartofacccount';
		  var colx = ['code', 'descp' ];
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  
		  var exString1 = 'table='+table+'&'+strx;

		  var strGetBox = '<div id="get_supplier_box" title="Get Account">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Account Code or Account Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					strGetBox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
					strGetBox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';


		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {
			
			  /*"Get Code": function() {
				var gcodex = $("#gcode-update").val();
				var gdescpx = $("#gdescp-update").val();
				$("#ad_companycode").val(gcodex);
				$("#ad_companyname").val(gdescpx);
				$(this).dialog("destroy");
			  },
			  Cancel: function() {
				$(this).dialog("destroy");
				$( this ).dialog( "close" );
			  }*/
			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				var aVal = $('#acccode'+v).val();
				var bVal = $('#accdesc'+v).val();
				$("#"+tocd).val(aVal);
				$("#"+tods).val(bVal);
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  /*$( "#keyword-update" ).autocomplete({
			source: "get_buyer.jsp",
			minLength: 2,
			select: function( event, ui ) {
			  loga( ui.item ?
				ui.item.id :
				"Nothing selected, input was " + this.value );
			  logi( ui.item ?
				ui.item.value :
				"Nothing selected, input was " + this.value );
			}
		  });

  */      
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();

			
			if(u == 'yes'){
			  where2 = " where "+dicode+" and (code like '%"+newacc+"%' or descp like '%"+newacc+"%')   order by code";
			}else if(u == 'no'){
			  where2 = " where code like '%"+newacc+"%' or descp like '%"+newacc+"%' order by code";

			}else{
			  where2 = " where code like '%"+newacc+"%' or descp like '%"+newacc+"%' order by code";

			}
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			//strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++;
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					returnsuppbox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
					returnsuppbox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
					returnsuppbox += '</td></tr>';
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				var aVal = $('#acccode'+v).val();
				var bVal = $('#accdesc'+v).val();

				$("#"+tocd).val(aVal);
				$("#"+tods).val(bVal);
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
		  }
		  
function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value, decimal_places) {

    // Convert the number to a string
    var value_string = rounded_value.toString()
    
    // Locate the decimal point
    var decimal_location = value_string.indexOf(".")

    // Is there a decimal point?
    if (decimal_location == -1) {
        
        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0
        
        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : ""
    }
    else {

        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1
    }
    
    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length
    
    if (pad_total > 0) {
        
        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++) 
            value_string += "0"
        }
    return value_string
}

function icon(typ,chk,app,post){
	  	var img = '';

	  	if(typ=='edit'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-default btn-xs disabled"><i class="fa fa-pencil"></i></button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>';
		  	}
	  	}

	  	if(typ=='delete'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>';
		  	}
	  	}

	  	if(typ=='addacc'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-default btn-xs disabled"><i class="fa fa-plus-circle"></i>&nbsp;Account&nbsp;</button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i>&nbsp;Account&nbsp;</button>';
		  	}
	  	}
                
                if(typ=='adddetail'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-default btn-xs disabled"><i class="fa fa-plus-circle"></i>&nbsp;Detail&nbsp;</button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i>&nbsp;Detail&nbsp;</button>';
		  	}
	  	}

	  	if(typ=='post'){
	  		if(post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-default btn-xs disabled"><i class="fa fa-list-ul"></i>&nbsp;Post&nbsp;</button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-list-ul"></i>&nbsp;Post&nbsp;</button>';
		  	}
	  	}

	  	if(typ=='cancel'){
	  		if(post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-default btn-xs disabled"><i class="fa fa-minus-circle"></i></button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-minus-circle"></i></button>';
		  	}
	  	}
		if(typ=='addcheque'){
	  		if(post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-primary btn-xs" disabled><i class="fa fa-plus-circle"></i>&nbsp;Cheque&nbsp;</button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i>&nbsp;Cheque&nbsp;</button>';
		  	}
	  	}
		if(typ=='addcredit'){
	  		if(post=='Cancel' || post=='Posted' || post=='posted'){
	  			img = '<button type="button" class="btn btn-default btn-xs disabled"><i class="fa fa-plus-circle"></i>&nbsp;Credit&nbsp;</button>';
		  	}else{
		  		img = '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i>&nbsp;Credit&nbsp;</button>';
		  	}
	  	}

	  	
	  	
	  	

	  	return img;
	  }

	  function iconClick(typ,chk,app,post){

	  	var x = 0;

	  	if(typ=='edit'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			x = 1;
		  	}
	  	}

	  	if(typ=='delete'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			x = 1;
		  	}
	  	}

	  	if(typ=='addacc'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			x = 1;
		  	}
	  	}
                
                if(typ=='adddetail'){
	  		if(app!='' || post=='Cancel' || post=='Posted' || post=='posted'){
	  			x = 1;
		  	}
	  	}

	  	if(typ=='post'){
	  		if(post=='Cancel' || post=='Posted' || post=='posted'){
	  			x = 1;
		  	}
	  	}

	  	if(typ=='cancel'){
	  		if(post=='Cancel' || post=='Posted' || post=='posted'){
	  			x = 1;
		  	}
	  	}
	  	
	  	

	  	return x;
	  }
	  
function case1(number) {
	if (number == 1) { return "ONE"; }
	else if (number == 2) { return "TWO"; }
	else if (number == 3) { return "THREE"; }
	else if (number == 4) { return "FOUR"; }
	else if (number == 5) { return "FIVE"; }
	else if (number == 6) { return "SIX"; }
	else if (number == 7) { return "SEVEN"; }
	else if (number == 8) { return "EIGHT"; }
	else if (number == 9) { return "NINE"; }
}

function case2(number) {
	if (number == 11) { return "ELEVEN"; }
	else if (number == 12) { return "TWELVE"; }
	else if (number == 13) { return "THIRTEEN"; }
	else if (number == 14) { return "FOURTEEN"; }
	else if (number == 15) { return "FIFTEEN"; }
	else if (number == 16) { return "SIXTEEN"; }
	else if (number == 17) { return "SEVENTEEN"; }
	else if (number == 18) { return "EIGHTEEN"; }
	else if (number == 19) { return "NINETEEN"; }
}

function case3(number) {
	if (number == 10) { return "TEN"; }
	else if (number == 20) { return "TWENTY"; }
	else if (number == 30) { return "THIRTY"; }
	else if (number == 40) { return "FORTY"; }
	else if (number == 50) { return "FIFTY"; }
	else if (number == 60) { return "SIXTY"; }
	else if (number == 70) { return "SEVENTY"; }
	else if (number == 80) { return "EIGHTY"; }
	else if (number == 90) { return "NINETY"; }
}

function identifyCase(number) {
	if (number < 10) { return 1; }
	else if (number < 100) { return 2; }
	else if (number < 1000) { return 3; }
	else if (number < 10000) { return 4; }
	else if (number < 100000) { return 5; } 
	else if (number < 1000000) { return 6; }
	else if (number < 10000000) { return 7; }
}

function convertRM(x,y) {
	var input = x;
	y = y;
	var amount1 = parseFloat(x);
	$('#'+y).val(round_decimals(amount1,2));
	 //document.form1.amounttotalcredit.value = round_decimals(amount1,2);
	try
	{
		var digit = round_decimals(parseFloat(input), 2);
	}
	catch(ex) { alert("Conversion Exception!") }
	
	if (digit < 0) { alert("Negatif Exception") }
	else if (digit == 0) { $('#'+y).val("NONE"); }
	else {
		var cent = Math.round(round_decimals(digit % 1, 2) * 100);
		var rm = round_decimals(digit - round_decimals(digit % 1, 2), 0);
		
		//identify RM
		var caseRM = identifyCase(rm);
		var strRM = "";
		if (rm != 0) {		
		while (caseRM != 0) {
			if (caseRM == 7) {
				var rmTemp = Math.floor(rm / 1000000);
				strRM = strRM + case1(rmTemp) + " MILLION";
				rm -= (rmTemp * 1000000);
				if (rm == 0) { caseRM = 1; }
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 5) { caseRM = 6; }
					else if (identifyCase(rm) == 4) { caseRM = 5; }
					else if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 6) {
				var rmTemp = Math.floor(rm / 100000);
				strRM = strRM + case1(rmTemp) + " HUNDRED";
				rm -= (rmTemp * 100000);
				if (rm == 0) { caseRM = 1;strRM += " THOUSAND"; }
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 5) { caseRM = 6; }
					else if (identifyCase(rm) == 4) { caseRM = 5; }
					else if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 5) {
				var rmTemp = Math.floor(rm / 1000);
				if (rmTemp > 10 && rmTemp < 20) { strRM = strRM + case2(rmTemp) + " THOUSAND"; }
				else { 
					var rmTemp1 = rmTemp;
					var rmTemp2 = Math.floor(rmTemp1 / 10) * 10;
					strRM = strRM + case3(rmTemp2);
					rmTemp1 -= rmTemp2;
					if (rmTemp1 == 0) { strRM = strRM + " THOUSAND"; }
					else { strRM = strRM + " " + case1(rmTemp1) + " THOUSAND"; }
				}
				rm -= (rmTemp * 1000);
				if (rm == 0) { caseRM = 1; }
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 3) { caseRM = 4; }
					else if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}			
			else if (caseRM == 4) {
				var rmTemp = Math.floor(rm / 1000);
				strRM = strRM + case1(rmTemp) + " THOUSAND";
				rm -= (rmTemp * 1000);
				if (rm == 0) { 
					$('#'+y).val(strRM);
					caseRM = 1; 
				}
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 2) { caseRM = 3; }
					else if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 3) {
				var rmTemp = Math.floor(rm / 100);
				strRM = strRM + case1(rmTemp) + " HUNDRED";
				rm -= (rmTemp * 100);
				if (rm == 0) { 
					document.form1.amounttotalword.value = strRM;
					caseRM = 1; 
				}
				else { 
					strRM = strRM + " "; 
					if (identifyCase(rm) == 1) { caseRM = 2; }
				}
			}
			else if (caseRM == 2) {
				if (rm > 10 && rm < 20) {
					strRM = strRM + case2(rm);
					caseRM --;
				}
				else {
					var rmTemp = Math.floor(rm / 10) * 10;
					strRM = strRM + case3(rmTemp);
					rm -= rmTemp;
					if (rm == 0) { caseRM --; }
					else { strRM = strRM + " "; }
				}
			}
			else if (caseRM == 1) { strRM = strRM + case1(rm); }
			caseRM --;
		}
		}
		
		//identify CENT
		if (cent == 0) { $('#'+y).val(strRM + " ONLY"); } 
		else {
			if (strRM != "")
			//alert(strRM)
				strRM = strRM + " AND "
			var caseCENT = identifyCase(cent);
			while (caseCENT != 0) {
				if (caseCENT == 2) {
					if (cent > 10 && cent < 20) {
						$('#'+y).val(strRM + case2(cent) + " CENT ONLY");
						caseCENT --;
					}
					else {
						var centTemp = Math.floor(cent / 10) * 10;
						strRM = strRM + case3(centTemp);
						cent -= centTemp;
						if (cent == 0) { 
							$('#'+y).val(strRM + " CENT ONLY");
							caseCENT --; 
						}
						else { strRM = strRM + " "; }
					}
				}
				else if (caseCENT == 1) { $('#'+y).val(strRM + case1(cent) + " CENT ONLY"); }
				caseCENT --;			
			}
		}	
	}
}
// Convert DIGIT to SENTENCE - END

function voucherStatus(x,y,z){
	var stat = 'Preparing';
	if(x.length > 0){
		stat = 'Checked';
	}
	if(y.length > 0){
		stat = 'Approved';
	}
	if(z == 'posted'){
		stat = 'Posted';
	}
	if(z == 'Cancel'){
		stat = 'Cancelled';
	}
	return stat;
}

function importColumn(tdCode,tbltype){
	 var fieldname = '';
	if(tbltype=='main'){
		fieldname = 'maintable';
	}else if(tbltype=='sub'){
		fieldname = 'subtable';
	}
	var txt;
    var r = confirm("Import from Information Schema?");
    if (r == true) {
        var table = 'sec_module';
		var colx = [  "moduleid",fieldname,"moduledesc" ];
		var strx = 'cols=';
		for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		}
			
		var where = " where moduleid = '"+tdCode+"' ";
		var exString1 = 'table='+table+'&'+strx;
		var storeID = [];
		var y = 0;
		$.ajax({
			url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			type:'POST',
			async: false,
			data: exString1,
			success: function(output){
			var maintable = '';
			var moduledesc = '';
			var i = 0;
			var obja = $.parseJSON(output);
				$.each(obja, function() {
					maintable = this[colx[1]];
					moduledesc = this[colx[2]];
					console.log(maintable);
					getAllColumn(tdCode,maintable,moduledesc,tbltype);
				});
			} 
		}); 
    }
	
			
}

function getAllColumn(x,y,z,o){
	var table = 'INFORMATION_SCHEMA.COLUMNS';
	var colx = [  "COLUMN_NAME" ];
	var strx = 'cols=';
	for (j = 0; j < colx.length; ++j) {
		strx+= colx[j]+'&cols=';
	}
		
	var where = " where TABLE_NAME = '"+y+"' ";
	var exString1 = 'table='+table+'&'+strx;
	var storeColumns= [];
	var y = 0;
	$.ajax({
		url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
		type:'POST',
		async: false,
		data: exString1,
		success: function(output){
		var maintable = [];
		var i = 0;
		var obja = $.parseJSON(output);
			$.each(obja, function() {
				storeColumns.push(this['COLUMN_NAME']);
				
			});
		} 
	}); 
	
	console.log(storeColumns);
	
	 var effTable = '';
	if(o=='main'){
		effTable = 'cf_module_param';
	}else if(o=='sub'){
		effTable = 'cf_module_param_sub';
	}
	
	var iDtoChk = 'id_u';
	
	var colx = ["column_name"];
	var strItem = 'cols='+colx[0]+'&cols=';	
	
							
	for (j = 0; j < storeColumns.length; ++j) {//put value from database to variable jget
	var dataStringUp = '';
		//strItem+= colx[j]+'&cols=';
		if(j == colx.length-1){
			dataStringUp += colx[0]+'='+storeColumns[j];
		}else{
			dataStringUp += colx[0]+'='+storeColumns[j]+'&';
		}
		
		dataStringUp=dataStringUp+'&theid='+iDtoChk+'&table='+effTable+'&'+strItem+'module_id&cols=module_name&cols=&flag=add&module_id='+x+'&module_name='+z;
		console.log(dataStringUp);
	$.ajax({
		type:'POST',
		data:dataStringUp,
		url:'save_dataquery.jsp',
		success:function(response) {
			$(this).dialog("destroy");
		}
	});		
	}
			
}

function getVoucherNo(table,voucherno){
	var no = '';//column to retrieve the value
	var colx = ['no'];
	var defineColumn = "ifnull(concat(lpad(max("+voucherno+")+1,5,'0')), '00001') as ";
	var strx = 'cols=';
	for (j = 0; j < colx.length; ++j) {
		strx+= colx[j]+'&cols=';
	}

	var where = '';
	var exString1 = 'table='+table+'&'+strx+'&sWhere=';
	$.ajax({
		url: "get_dataquery.jsp?definecolumn="+encodeURIComponent(defineColumn),
		type:'POST',
		async: false,
		data: exString1,
		success: function(output_string){
			var obj = $.parseJSON(output_string);
			$.each(obj, function() {
				no = this['no'];
			});
		}
	});
	
	return no;
}

function getReferenceNo(table,abb,refertitle){
	
	var no = '';//column to retrieve the value
	//var table = 'gl_jv';
	//var abb = 'JVN';
	//var refertitle = 'JVrefno';
	var colx = [refertitle];
	var defineColumn = "ifnull(concat(lpad(max("+refertitle+")+1,4,'0')), '0001') as";
	var strx = 'cols=';
	
	for (j = 0; j < colx.length; ++j) {
		strx+= colx[j]+'&cols=';
	}

	var where = '';
	var exString1 = 'table='+table+'&refertitle='+refertitle+'&abb='+abb;
	$.ajax({
		url: "get_referno.jsp?definecolumn="+encodeURIComponent(defineColumn),
		type:'POST',
		async: false,
		data: exString1,
		success: function(output_string){
			var obj = $.parseJSON(output_string);
			$.each(obj, function() {
				no = this['newrefer'];
			});
		}
	});

	return no;	
}

function deletexRow(oTable,deleteBoxTitle,updateTable,tdCode,updateID){
			var str = '<div id="dialog-form-update" title="Delete '+deleteBoxTitle+'">';
					str += 'Are you sure to delete this (';
					str += tdCode;
					str += ') ?</div>';
				  var deleteBox = $(str);
				  var k = 0;
				$(deleteBox).dialog({
				  autoOpen: false,
			  modal: true,

			  buttons: {
		
				"I'm Sure": function() {

				  $.ajax({
					type:'POST',
					async: false,
					data:'id='+tdCode+'&theid='+updateID+'&table='+updateTable,
					url:'delete_query.jsp',
					success:function(response) {
					  k=1;
					}
				  });

				  if(k==1){
					$(this).dialog("destroy");
					  successNoty( deleteBoxTitle , tdCode, deleteBoxTitle+' Deleted', 'Successful delete '+deleteBoxTitle+' ');
					  setTimeout(function(){
                                         oTable.fnClearTable();}, 500);
				  }
				  
								   
				  //$(this).dialog("destroy");
				  //successNoty( generalUsage , tdCode, generalUsage+' Deleted', 'Successful delete '+generalUsage+' ');

							   
				},
				Cancel: function() {
				  $(this).dialog("destroy");
				}
			  }
			});

			  $(deleteBox).dialog("open"); 
		  
			  }


			  function successNoty( nType, nIdentify, nTitle, nText){
				$.pnotify({
			  title: nTitle,
			  text: nText+nIdentify,
			  type: 'success',
			  styling: 'jqueryui',
			  shadow: false,
			  addClass:'customsuccess'
			});
			  }

			  function infoNoty( nText){
				$.pnotify({
				  text:nText,
				  styling: 'jqueryui'
				});
			  }

			  function errorNoty( nIdentify, nText ){
				$.pnotify({
				title: 'Error occured!',
				text: nIdentify + nText,
				type: 'error',
				styling: 'jqueryui',
				shadow: false
			});
			  }


function addSubform(refno,updateTable,module_type,module_abb,oTable){//addSubform(oData.JVrefno,subTable,module_type,module_abb);
	
		var newItem = "";
		var strItem = 'cols=';
		 for (j = 0; j < sub_updateFields.length; ++j) {//put value from database to variable jget
						strItem+= sub_updateFields[j]+'&cols=';
		}

		
		var str = '<div id="dialog-form-update" title="Add New Information"><p class="validateTips">All form fields are required.</p><form><table id="table_2" cellspacing="0">';
		var i;

		for (i = 0; i < sub_updateFields.length; ++i) {//put value from database to variable jget
		  if(sub_updateFieldtype[i] == 'hidden'){

			str += '<input style="font-size:11px" id="ad_'+sub_updateFields[i]+'" type="hidden" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+' '+sub_updateFieldbtparam[i]+'  />';

		  }else{

			
		  
		  str += '<tr><td width="30%" class="bd_bottom" align="left" valign="top">'+sub_updateTitles[i]+'</td>';
		  str += '<td width="63%" class="bd_bottom" align="left">';

		  if(sub_updateFieldtype[i] == "text"){
			  
			 if(sub_updateFields[i]==updateID){
				sub_updateFieldvalue[i]=refno; 
			 }
			var getbutton = '';
			
			if(sub_getinfo[i]!='None'){
				
				getbutton = '<button class="getinfo" id="'+sub_getinfo[i]+'" title="'+sub_updateFields[i]+'">Get '+sub_getinfo[i]+'</button>';
				getbutton += '<input type="hidden" id="basecolumn'+sub_updateFields[i]+'" value="'+sub_basecolumn[i]+'">';
				getbutton += '<input type="hidden" id="getcodecolumn'+sub_updateFields[i]+'" value="'+sub_getcodecolumn[i]+'">';
				getbutton += '<input type="hidden" id="getdesccolumn'+sub_updateFields[i]+'" value="'+sub_getdesccolumn[i]+'">';
				getbutton += '<input type="hidden" id="getothercolumn'+sub_updateFields[i]+'" value="'+sub_getothercolumn[i]+'">';
				getbutton += '<input type="hidden" id="gettable'+sub_updateFields[i]+'" value="'+sub_gettable[i]+'">';
				getbutton += '<input type="hidden" id="gettablename1'+sub_updateFields[i]+'" value="'+sub_gettablename1[i]+'">';
				getbutton += '<input type="hidden" id="gettablename2'+sub_updateFields[i]+'" value="'+sub_gettablename2[i]+'">';
				getbutton += '<input type="hidden" id="gettablename3'+sub_updateFields[i]+'" value="'+sub_gettablename3[i]+'">';
				
				//getbutton = '<input type="button" value="Get '+getinfo[i]+'" onClick="getinfox(\'buyer_info\',\''+code_col+'\',\'Buyer\','+toe+',\'\');" >';
				//getinfo('buyer_info',code_col,'Buyer',toe,'');//a = table, b= column in table, c=title, d=field name to effect	
			}
			
			 if(sub_updateFields[i]=='estatename'){
			  
			  sub_updateFieldvalue[i]='<%=estatename%>';
			  str += '<input style="font-size:11px" id="ad_'+sub_updateFields[i]+'" type="text" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+' '+sub_updateFieldbtparam[i]+' />';
			}else if(sub_updateFields[i]=='year'){
			  
			  sub_updateFieldvalue[i]=year;
			  str += '<input style="font-size:11px" id="ad_'+sub_updateFields[i]+'" type="text" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+'  '+sub_updateFieldbtparam[i]+'/>';
			}else if(sub_updateFields[i]=='period'){
			  
			  sub_updateFieldvalue[i]=period;
			  str += '<input style="font-size:11px" id="ad_'+sub_updateFields[i]+'" type="text" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+' '+sub_updateFieldbtparam[i]+' />';
			}else{
				
				//begin*************for generate automatic refer no***************
				if(sub_autorefer[i]=='1'){
				 //---------------------------------
			
				  sub_updateFieldvalue[i]=getReferenceNo(updateTable,module_abb,updateID);//getReferenceNo(table,abb,refertitle,colx)
				  str += '<input style="font-size:11px" id="ad_'+sub_updateFields[i]+'" type="text" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+'  '+sub_updateFieldbtparam[i]+' />';
				  //end*************for generate automatic refer no*****************
				}else if(sub_autovoucher[i]=='1'){
					sub_updateFieldvalue[i]=getVoucherNo(updateTable,updateFields[1]);//getReferenceNo(table,abb,refertitle,colx)
				  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+'  '+sub_updateFieldbtparam[i]+' />';
					
				}else{
			  		str += '<input style="font-size:11px" id="ad_'+sub_updateFields[i]+'" type="text" value="'+sub_updateFieldvalue[i]+'" '+sub_updateFieldparam[i]+' '+sub_updateFieldbtparam[i]+'  />&nbsp;'+getbutton;
				}
			}
			

			



		  }else if(sub_updateFieldtype[i] == "text - button"){

			str += '<input id="ad_'+sub_updateFields[i]+'"  type="text" '+sub_updateFieldparam[i]+' /></td><td> <button id="get-acctx">Get Account</button></td>';

		  }else if(sub_updateFieldtype[i] == "combo - listdb"){

			str += '<select id="ad_'+sub_updateFields[i]+'"  '+sub_updateFieldparam[i]+'>';

			
			var listDB = "parameter";
			var listFields = [ "parameter", "value" ];
			var listID = "aparameter";
			var listItem = 'cols=';
			
			for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
			  listItem += listFields[m]+'&cols=';
			}
			
			var listString = 'identifier='+listID+'&'+listID+'='+sub_updateFieldsql[i]+'&table='+listDB+'&'+listItem;

			$.ajax({
			  url: 'get_data.jsp',
			  type:'POST',
			  async: false,
			  data: listString,
			  success: function(output){
				var obja = $.parseJSON(output);
						$.each(obja, function() {
					var paramt = this['parameter'];
					var valt = this['value'];
					str += '<option value"'+valt+'">'+valt+'</option>';
				  });
			  }
			});



			
			str += '</select>';
						  
		  }else if(sub_updateFieldtype[i] == "combo - yesno"){

			str += '<select id="ad_'+sub_updateFields[i]+'"  '+sub_updateFieldparam[i]+'>';
			var selOpt = "";

			for (u = 0; u < yesno.length; ++u) {
			  str += '<option value"'+yesno[u]+'">'+yesno[u]+'</option>';
			}

			str += '</select>';

			
		  }else if(sub_updateFieldtype[i] == "textarea"){

			str += '<textarea id="ad_'+sub_updateFields[i]+'"  '+sub_updateFieldparam[i]+' '+sub_updateFieldbtparam[i]+' rows="4" cols="50">';
			str += '</textarea>';
		}
						
		  str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

		  }
		  

		}

		str += '</table></form></div>';

		var addTable = $(str);

		$(addTable).dialog({
			autoOpen: false,
		  height: 550,
		  width: 700,
		  modal: true,
		  buttons: {
			  "Save": function() {

			  var eachI = [];
			  var bValid = true;
			  var c = 0;
			  newItem = $('#ad_'+sub_updateFields[1]).val(); 
			  var eachV = [];
				var idV = '';
				for (j = 0; j < sub_updateFields.length; ++j) {
				if(sub_updateReq[j] == 1){
					  //alert(sub_updateFields[j]);

					  eachV[j]= $('#ad_'+sub_updateFields[j]).val();

					  bValid = checkLength( eachV[j], 'ad_'+sub_updateFields[j]);

					  if(bValid==false){
						c++;
					  }
				  

					if(sub_regexValid[j] == 1){
					  
					  eachV[j]= $('#ad_'+sub_updateFields[j]).val();
					  bValid = checkRegexp( eachV[j], 'ad_'+sub_updateFields[j]);
					  if(bValid==false){
						c++;
					  }
					  
					

					}
					
				  }

				}
  
			   

			  if(c>0){
				$(this).addClass('ui-state-error');
			  }

			  if (c==0) {

				var s = 0;
				var upTitle = [];
				var dataStringUp = "";
				for (j = 0; j < sub_updateFields.length; ++j) {//put value from database to variable jget
					if(sub_updateFields[j]=='Jvid'){
						 upTitle[j] = getJVid(refno);;
					}else{
						 upTitle[j] = $('#ad_'+sub_updateFields[j]).val();
					}
				 

				  if(j == sub_updateFields.length-1){
					dataStringUp += sub_updateFields[j]+'='+upTitle[j];
				  }else{
					dataStringUp += sub_updateFields[j]+'='+upTitle[j]+'&';
				  }
										  
				}
								 //alert(dataStringUp);
				dataStringUp=dataStringUp+'&table='+updateTable+'&'+strItem+'&flag=add';   
				console.log(dataStringUp);
				$.ajax({
					type:'POST',
					async:false,
					data:dataStringUp,
					url:'save_dataquery.jsp',
					success:function(data) {
					   
					   
						$(addTable).dialog("destroy");
setTimeout(function(){
                                         oTable.fnClearTable();}, 500);
				  successNoty( generalUsage , newItem, generalUsage+' Added', 'Successfull added ');
				  
					},
						error: function(){

						   //alert('ggg');
						  //$(updateTable).dialog("destroy");
						 errorNoty('', 'Failed to save '+generalUsage+'!');
						}
				});
				 

				  
			   
				
			  }
			},
			Cancel: function() {
			  $(this).dialog("destroy");
			}
		  }
		});

		  $(addTable).dialog("open");

		  ///---------important to destroy box get
		$('.ui-dialog-titlebar-close').on('click',function(){
			$(addTable).dialog("destroy");
			$('#get_supplier_box').remove();
		}); 
				  ///---------important to destroy box get
		for (i = 0; i < sub_updateFields.length; ++i) {
			if(dateCol[i]=='1'){
				$('#ad_'+sub_updateFields[i]).datepicker({ dateFormat: 'yy-mm-dd' });
			}
			
		}
		
		$('.getinfo').on('click',function(){
			var m = $(this).attr('title');
			var n = $(this).attr('id');
			var o = $("#getcodecolumn"+m).val();
			var p = $("#getdesccolumn"+m).val();
			var u = $("#getothercolumn"+m).val();
			var q = $("#gettable"+m).val();
			var r = $("#gettablename1"+m).val();
			var s = $("#gettablename2"+m).val();
			var v = $("#gettablename3"+m).val();
			var t = $("#basecolumn"+m).val();
			//console.log(t);
			var code_col = [];
			if(u==''){
				code_col = [r,s];
			}else{
				code_col = [r,s,u,v];
			}
			var toe = ['ad_'+o,'ad_'+p,'ad_'+u];
			if(m=='loccode'){
				getlocation(q,code_col,n,toe,'',t);
			}else if(m=='ctcode'){
				getchargeto(q,code_col,n,toe,'',t);
			}else if(m=='sacode'){
				getSub(q,code_col,n,toe,'',t);
			}else{
				getinfox(q,code_col,n,toe,'');
			}
			
			return false;
		});
			
}

function addForm(updateTable,module_type,module_abb,year,period,oTable,estatecode,estatename) {//refno,updateTable,module_type,module_abb)
	
	
        
	var table = 'cf_module_param';
		
		var newItem = "";
		var strItem = 'cols=';
		 for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
						strItem+= updateFields[j]+'&cols=';
		}

		
		var str = '<div id="dialog-form-update" title="Add New Information"><p class="validateTips">All form fields are required.</p><form><table id="table_2" cellspacing="0">';
		var i;

		for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
		  if(updateFieldtype[i] == 'hidden'){

			str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="hidden" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+'  />';

		  }else{

			
		  
		  str += '<tr><td width="30%" class="bd_bottom" align="left" valign="top">'+updateTitles[i]+'</td>';
		  str += '<td width="63%" class="bd_bottom" align="left">';

		  if(updateFieldtype[i] == "text"){
			var getbutton = '';
		
			if(getinfo[i]!='None'){
				getbutton = '<button class="getinfo" id="'+getinfo[i]+'" title="'+updateFields[i]+'">Get '+getinfo[i]+'</button>';
				getbutton += '<input type="hidden" id="basecolumn'+updateFields[i]+'" value="'+getcodecolumn[i]+'">';
				getbutton += '<input type="hidden" id="getcodecolumn'+updateFields[i]+'" value="'+getcodecolumn[i]+'">';
				getbutton += '<input type="hidden" id="getdesccolumn'+updateFields[i]+'" value="'+getdesccolumn[i]+'">';
				getbutton += '<input type="hidden" id="gettable'+updateFields[i]+'" value="'+gettable[i]+'">';
				getbutton += '<input type="hidden" id="gettablename1'+updateFields[i]+'" value="'+gettablename1[i]+'">';
				getbutton += '<input type="hidden" id="gettablename2'+updateFields[i]+'" value="'+gettablename2[i]+'">';
				
				//getbutton = '<input type="button" value="Get '+getinfo[i]+'" onClick="getinfox(\'buyer_info\',\''+code_col+'\',\'Buyer\','+toe+',\'\');" >';
				//getinfo('buyer_info',code_col,'Buyer',toe,'');//a = table, b= column in table, c=title, d=field name to effect	
			}
			
			 if(updateFields[i]=='estatename'){
			  
			  updateFieldvalue[i]=estatename;
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' />';
			}else if(updateFields[i]=='estatecode'){
			  
			  updateFieldvalue[i]=estatecode;
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' />';
			}else if(updateFields[i]=='year'){
			  
			  updateFieldvalue[i]=year;
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  '+updateFieldbtparam[i]+'/>';
			}else if(updateFields[i]=='period' || updateFields[i]=='curperiod'){
			  
			  updateFieldvalue[i]=period;
			  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' />';
			}else{
				
				if(updateFieldvalue[i]=='Date'){
					updateFieldvalue[i] = getCurrentDate();
				}
				
				//begin*************for generate automatic refer no***************
				if(autorefer[i]=='1'){
				 //---------------------------------
			
				  updateFieldvalue[i]=getReferenceNo(updateTable,module_abb,updateID);//getReferenceNo(table,abb,refertitle,colx)
				  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  '+updateFieldbtparam[i]+' />';
				  //end*************for generate automatic refer no*****************
				}else if(autovoucher[i]=='1'){
					updateFieldvalue[i]=getVoucherNo(updateTable,updateFields[1]);//getReferenceNo(table,abb,refertitle,colx)
				  str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+'  '+updateFieldbtparam[i]+' />';
					
				}else{
				
			  		str += '<input style="font-size:11px" id="ad_'+updateFields[i]+'" type="text" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+'  />&nbsp;'+getbutton;
				}
			}
			

			



		  }else if(updateFieldtype[i] == "text - button"){

			str += '<input id="ad_'+updateFields[i]+'"  type="text" '+updateFieldparam[i]+' /></td><td> <button id="get-acctx">Get Account</button></td>';

		  }else if(updateFieldtype[i] == "combo - listdb"){

			str += '<select id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';

			
			var listDB = "parameter";
			var listFields = [ "parameter", "value" ];
			var listID = "aparameter";
			var listItem = 'cols=';
			
			for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
			  listItem += listFields[m]+'&cols=';
			}
			
			var listString = 'identifier='+listID+'&'+listID+'='+updateFieldsql[i]+'&table='+listDB+'&'+listItem;

			$.ajax({
			  url: 'get_data.jsp',
			  type:'POST',
			  async: false,
			  data: listString,
			  success: function(output){
				var obja = $.parseJSON(output);
						$.each(obja, function() {
					var paramt = this['parameter'];
					var valt = this['value'];
					str += '<option value"'+valt+'">'+valt+'</option>';
				  });
			  }
			});



			
			str += '</select>';
						  
		  }else if(updateFieldtype[i] == "combo - yesno"){
			  

			str += '<select id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
			var selOpt = "";

			for (u = 0; u < yesno.length; ++u) {
				var selected = '';
			  	if(updateFieldvalue[i]==yesno[u]){
					selected = 'selected';
				}
			  	str += '<option value="'+yesno[u]+'" '+selected+'>'+yesno[u]+'</option>';
			}

			str += '</select>';

			
		  }else if(updateFieldtype[i] == "textarea"){

			str += '<textarea id="ad_'+updateFields[i]+'"  '+updateFieldparam[i]+' '+updateFieldbtparam[i]+' rows="4" cols="50">';
			str += '</textarea>';
		}
						
		  str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

		  }
		  

		}

		str += '</table></form></div>';

		var addTable = $(str);

		$(addTable).dialog({
			autoOpen: false,
		  height: 550,
		  width: 700,
		  modal: true,
		  buttons: {
			  "Save": function() {

			  var eachI = [];
			  var bValid = true;
			  var c = 0;
			  newItem = $('#ad_'+updateFields[1]).val(); 
			  var eachV = [];
				var idV = '';
				for (j = 0; j < updateFields.length; ++j) {
				if(updateReq[j] == 1){
					  //alert(updateFields[j]);

					  eachV[j]= $('#ad_'+updateFields[j]).val();

					  bValid = checkLength( eachV[j], 'ad_'+updateFields[j]);

					  if(bValid==false){
						c++;
					  }
				  

					if(regexValid[j] == 1){
					  
					  eachV[j]= $('#ad_'+updateFields[j]).val();
					  bValid = checkRegexp( eachV[j], 'ad_'+updateFields[j]);
					  if(bValid==false){
						c++;
					  }
					  
					

					}
					
				  }

				}
  
			   

			  if(c>0){
				$(this).addClass('ui-state-error');
			  }

			  if (c==0) {

				var s = 0;
				var upTitle = [];
				var dataStringUp = "";
				for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
				  upTitle[j] = $('#ad_'+updateFields[j]).val();

				  if(j == updateFields.length-1){
					dataStringUp += updateFields[j]+'='+upTitle[j];
				  }else{
					dataStringUp += updateFields[j]+'='+upTitle[j]+'&';
				  }
										  
				}
								 //alert(dataStringUp);
				dataStringUp=dataStringUp+'&table='+updateTable+'&'+strItem+'&flag=add';   
				console.log(dataStringUp);
				$.ajax({
					type:'POST',
					async:false,
					data:dataStringUp,
					url:'save_dataquery.jsp',
					success:function(data) {
					   
					   
						$(addTable).dialog("destroy");
					setTimeout(function(){
                                         oTable.fnClearTable();}, 500);
				  successNoty( '' , newItem, ''+' Added', 'Successfull added ');
				 
					},
						error: function(){

						   //alert('ggg');
						  //$(updateTable).dialog("destroy");
						 errorNoty('', 'Failed to save '+generalUsage+'!');
						}
				});
				 

				  
			   
				
			  }
			},
			Cancel: function() {
			  $(this).dialog("destroy");
			}
		  }
		});

		  $(addTable).dialog("open");

		  ///---------important to destroy box get
		$('.ui-dialog-titlebar-close').on('click',function(){
			$(addTable).dialog("destroy");
			$('#get_supplier_box').remove();
		}); 
				  ///---------important to destroy box get
		for (i = 0; i < updateFields.length; ++i) {
			if(dateCol[i]=='1'){
				$('#ad_'+updateFields[i]).datepicker({ dateFormat: 'yy-mm-dd' });
			}
			
		}
		
		$('.getinfo').on('click',function(){
			var m = $(this).attr('title');
			var n = $(this).attr('id');
			var o = $("#getcodecolumn"+m).val();
			var p = $("#getdesccolumn"+m).val();
			var q = $("#gettable"+m).val();
			var r = $("#gettablename1"+m).val();
			var s = $("#gettablename2"+m).val();
			console.log(q);
			var code_col = [r,s];
			var toe = ['ad_'+o,'ad_'+p];
			if(m=='loccode'){
				getlocation(q,code_col,n,toe,'');
			}else{
				getinfox(q,code_col,n,toe,'');
			}
			
			return false;
		});
		
}
function getlocationEstate(a,b,c,d,e,f){//a = table, b= column in table, c=title, d=field name to effect,e=,f=base on location
	alert('sa');
	var loclvl = $('#ad_'+f).val();
	b=[];
	var sqlstr='';
	if(loclvl=='Division')
	{
		a = 'ce_division';
		b = ['code','name'];
		//sqlstr="select code,name from ce_division order by code";
	}
	else if(loclvl=='Block')
	{
		a = 'ce_block';
		b = ['code','name'];
		//sqlstr="select code,name from ce_block order by code";
	
	}
	else if(loclvl=='Field')
	{
		a = 'ce_field';
		b = ['code','name'];
		//sqlstr="select code,name from ce_field order by code";
	
	}
	else if(loclvl=='Field Enterprise')
	{
		a = 'ce_fieldenter';
		b = ['code','name'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Estate')
	{
		a = 'ce_estate';
		b = ['code','name'];
		//sqlstr="(select code,name from ce_estate order by code) union (select estatecode as code,estatedescp as name from estateinfo order by estatecode)";
			
	}
	
	else if(loclvl=='Management')
	{
		a = 'ce_manage';
		b = ['code','name'];
		//sqlstr="select code,name from ce_manage order by code";
	
	}

	var table = a;
		  var colx = b;
		  var titles = c;
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = " "+e;
		  var exString1 = 'table='+table+'&'+strx;
		console.log(d);
		  var strGetBox = '<div id="get_supplier_box" title="Get '+c+'">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the '+c+' Code or '+c+' Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';
		  
		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {

			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  ///---------important to destroy box get
				$('.ui-dialog-titlebar-close').on('click',function(){
					$(updateTableMini).dialog("destroy");
				}); 
				///---------important to destroy box get

		  var fieldform = d;

		  

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where "+colx[0]+" like '%"+newacc+"%' or "+colx[1]+" like '%"+newacc+"%' order by "+colx[0]+"";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
	
}

function getlocation(a,b,c,d,e,f){//a = table, b= column in table, c=title, d=field name to effect,e=,f=base on location
	var loclvl = $('#ad_'+f).val();
	b=[];
	var sqlstr='';
	if(loclvl=='Management')
	{
		a = 'ce_manage';
		b = ['code','name'];
		//sqlstr="select code,name from ce_division order by code";
	}
	else if(loclvl=='Company')
	{
		a = 'ce_estate';
		b = ['code','name'];
		//sqlstr="select code,name from ce_block order by code";
	
	}
	else if(loclvl=='Department')
	{
		a = 'department';
		b = ['code','name'];
		//sqlstr="select code,name from ce_field order by code";
	
	}

	var table = a;
		  var colx = b;
		  var titles = c;
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = " "+e;
		  var exString1 = 'table='+table+'&'+strx;
		console.log(d);
		  var strGetBox = '<div id="get_supplier_box" title="Get '+c+'">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the '+c+' Code or '+c+' Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output" width="100%">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td width="100%" class="selected_td'+y+'" id="'+jGet[0]+'"><div width="100%" class="rowget">'+jGet[0]+' - '+jGet[1]+'</div>';
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';
		  
		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {

			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  ///---------important to destroy box get
				$('.ui-dialog-titlebar-close').on('click',function(){
					$(updateTableMini).dialog("destroy");
				}); 
				///---------important to destroy box get

		  var fieldform = d;

		  

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where "+colx[0]+" like '%"+newacc+"%' or "+colx[1]+" like '%"+newacc+"%' order by "+colx[0]+"";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'"><div class="rowget">'+jGet[0]+' - '+jGet[1]+'</div>';
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
	
}

function getchargeto(a,b,c,d,e,f){//a = table, b= column in table, c=title, d=field name to effect,e=,f=base on location
	var loclvl = $('#ad_'+f).val();
	b=[];
	var sqlstr='';
	


	if(loclvl=='Machinery')
	{
		a = 'machinery';
		b = ['code','descp'];
		//sqlstr="select code,name from ce_division order by code";
	}
	else if(loclvl=='Workshop')
	{
		a = 'workshop';
		b = ['code','descp'];
		//sqlstr="select code,name from ce_block order by code";
	
	}
	else if(loclvl=='Enterprise')
	{
		a = 'enterprise_info';
		b = ['code','descp'];
		//sqlstr="select code,name from ce_field order by code";
	
	}
	else if(loclvl=='Worker')
	{
		a = 'executive';
		b = ['id','name'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}

	var table = a;
		  var colx = b;
		  var titles = c;
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = " "+e;
		  var exString1 = 'table='+table+'&'+strx;
		console.log(d);
		  var strGetBox = '<div id="get_supplier_box" title="Get '+c+'">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the '+c+' Code or '+c+' Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';
		  
		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {

			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  ///---------important to destroy box get
				$('.ui-dialog-titlebar-close').on('click',function(){
					$(updateTableMini).dialog("destroy");
				}); 
				///---------important to destroy box get

		  var fieldform = d;

		  

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where "+colx[0]+" like '%"+newacc+"%' or "+colx[1]+" like '%"+newacc+"%' order by "+colx[0]+"";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++

						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
	
}

function getSub(a,b,c,d,e,f){//a = table, b= column in table, c=title, d=field name to effect,e=,f=base on location
	var loclvl = $('#ad_'+f).val();
	b=[];
	var sqlstr='';




	if(loclvl=='Worker')
	{
		a = 'executive';
		b = ['id','name'];
		//sqlstr="select code,name from ce_division order by code";
	}
	else if(loclvl=='Non-Executive')
	{
		a = 'executive';
		b = ['id','name'];
		//sqlstr="select code,name from ce_block order by code";
	
	}
	else if(loclvl=='Executive')
	{
		a = 'executive';
		b = ['id','name'];
		//sqlstr="select code,name from ce_field order by code";
	
	}
	else if(loclvl=='Sundry Contract')
	{
		a = 'snmcontractor_info';
		b = ['suppliercode','companyname'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Stage')
	{
		a = 'entrestage';
		b = ['code','descp'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Member')
	{
		a = 'coop_member';
		b = ['noanggota','nama'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Supplier')
	{
		a = 'supplier_info';
		b = ['code','companyname'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Buyer')
	{
		a = 'buyer_info';
		b = ['code','companyname'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Fund')
	{
		a = 'ce_manage_bank';
		b = ['code','name'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Investment')
	{
		a = 'est_investment';
		b = ['code','name'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Accrual')
	{
		a = 'conf_accrual_info';
		b = ['code','companyname'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Contractor')
	{
		a = 'contractor_info';
		b = ['code','companyname'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Estate')
	{
		a = 'estateinfo';
		b = ['estatecode','estatedescp'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Machinery')
	{
		a = 'machinery';
		b = ['code','descp'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}
	else if(loclvl=='Workshop')
	{
		a = 'workshop';
		b = ['code','descp'];
		//sqlstr="select code,name from ce_fieldenter order by code";
	
	}

	var table = a;
		  var colx = b;
		  var titles = c;
		  var strx = 'cols=';
		  for (j = 0; j < colx.length; ++j) {
			strx+= colx[j]+'&cols=';
		  }

		  var where = " "+e;
		  var exString1 = 'table='+table+'&'+strx;
		console.log(d);
		  var strGetBox = '<div id="get_supplier_box" title="Get '+c+'">';

			strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the '+c+' Code or '+c+' Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
			strGetBox += '<div><table id="supplier_output" width="100%">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
			  type:'POST',
			  async: false,
			  data: exString1,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						  y++
						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					strGetBox += '<tr><td class="selected_td'+y+'" id="'+jGet[0]+'"><div class="rowget">'+jGet[0]+' - '+jGet[1]+'</div>';
					 for (i = 0; i < colx.length; ++i) {
						strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
					strGetBox += '</td></tr>';
				  });
				} 
			}); 
			strGetBox += '';
			strGetBox += '</table></div>';
			strGetBox += '</div>';
		  
		  var updateTableMini = $(strGetBox);

		  $( updateTableMini ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {

			}
		  });

		  $( updateTableMini ).dialog( "open" );

		  ///---------important to destroy box get
				$('.ui-dialog-titlebar-close').on('click',function(){
					$(updateTableMini).dialog("destroy");
				}); 
				///---------important to destroy box get

		  var fieldform = d;

		  

		  for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  
		  $( "#keyword-insert" ).keyup(function() {

			var newacc = $('#keyword-insert').val();
			var where2 = " where "+colx[0]+" like '%"+newacc+"%' or "+colx[1]+" like '%"+newacc+"%' order by "+colx[0]+"";
			var exString2 = 'table='+table+'&'+strx;


			var returnsuppbox = '<table id="supplier_output">';
			
			var y = 0;
			$.ajax({
			  url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
			  type:'POST',
			  async: false,
			  data: exString2,
			  success: function(output){
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
						$.each(obja, function() {
						 y++

						  
						  for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
					  jGet[i] = this[colx[i]];
					}
					returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'"><div class="rowget">'+jGet[0]+' - '+jGet[1]+'</div>';
					 for (i = 0; i < colx.length; ++i) {
						returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
					}
				  });
				} 
			}); 
			returnsuppbox += '';
			returnsuppbox += '</table>';

			$('#supplier_output').html(returnsuppbox);

			for (j = 1; j < y+1; ++j) {  
				$('.selected_td'+j).on('click',function(){
				  var m = $(this).html();
				var v = $(this).attr('id');
				 var nV = "";
				for (i = 0; i < colx.length; ++i) {
				  //alert(colx[i]+'--'+updateFields[i]);
				  nv = $('#'+colx[i]+v).val();
				  
				  $("#"+fieldform[i]).val(nv);
				}
				
				$( "#get_supplier_box" ).remove();

				});
			  }
		  });
	
}

function getJVid(refno){
	
	var no = '';//column to retrieve the value
	//var table = 'gl_jv';
	//var abb = 'JVN';
	//var refertitle = 'JVrefno';
	//var colx = [refertitle];
	//var defineColumn = "ifnull(concat(lpad(max("+refertitle+")+1,4,'0')), '0001') as";
	var defineColumn = "ifnull(concat(jvrefno,lpad((max(right(jvid,4))+1),4,'0')),concat('"+ refno +"','0001'))";
	var strx = 'cols=';
	
	//for (j = 0; j < colx.length; ++j) {
		//strx+= colx[j]+'&cols=';
	//}

	var where = '';
	//var exString1 = 'refno='+refno+'&table='+table+'&refertitle='+refertitle+'&abb='+abb;
	var exString1 = 'refno='+refno;
	$.ajax({
		url: "get_referno_sub.jsp?definecolumn="+encodeURIComponent(defineColumn),
		type:'POST',
		async: false,
		data: exString1,
		success: function(output_string){
			var obj = $.parseJSON(output_string);
			$.each(obj, function() {
				no = this['newrefer'];
			});
		}
	});

	return no;	
}

function editRowx(tdCode,updateTable,subTable,module_type,module_abb,oTable) {
	updateTitles=[];
	updateFields=[]
	updateFieldtype=[]
	updateFieldbtparam=[]
	updateFieldvalue=[]
	updateFieldparam=[]
	updateFieldsql=[]
	updateReq=[]
	regexValid=[]
	dateCol=[]
	autorefer=[]
	autovoucher=[]
	getinfo=[]
	basecolumn=[]
	getcodecolumn=[]
	getdesccolumn=[]
	gettable=[]
	gettablename1=[]
	gettablename2=[]
					
	var table = 'cf_module_param';
		var colxv = [ "module_id","module_name","position","title","refer_id","column_name","type_field","attribute_1","attribute_2","default_value","param_name","field_size","listview","id_u","field_check","regex_check","date", "listform","autorefer","autovoucher","get_info","basecolumn","get_info_1stcolumn","get_info_2ndcolumn","get_info_table","get_info_table_column1", "get_info_table_column2" ];

		var strxv = 'cols=';
		for (var j = 0; j < colxv.length; ++j) {
			strxv+= colxv[j]+'&cols=';
		}
		
		//var updateTable = "gl_jv";
	  	//var module_type = '020104';
		//var module_abb = 'JVN';
	  	var where2v = " where module_id = '"+module_type+"' order by position asc";
		var exString2v = 'table='+table+'&'+strxv;
			
		console.log(exString2v)
		//var updateDesc = "taxcode";
		
	  	$.ajax({
		url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2v),
			  type:'POST',
			  async: false,
			  data: exString2v,
			  success: function(output){
				  
				var jGet = [];
				var i = 0;
				var obja = $.parseJSON(output);
				$.each(obja, function() {
					
					var referID = this[colxv[4]];
					if(referID=='1'){
						updateID=this[colxv[5]];
					}
					var viewInForm = this[colxv[17]];
					if(viewInForm=='1'){
						
						updateTitles.push(this[colxv[3]]);
						updateFields.push(this[colxv[5]]);
						updateFieldtype.push(this[colxv[6]]);
						updateFieldbtparam.push(this[colxv[7]]);
						updateFieldvalue.push(this[colxv[9]]);
						updateFieldparam.push(this[colxv[11]]);
						updateFieldsql.push(this[colxv[10]]);
						updateReq.push(this[colxv[14]]);
						regexValid.push(this[colxv[15]]);
						dateCol.push(this[colxv[16]]);
						autorefer.push(this[colxv[18]]);
						autovoucher.push(this[colxv[19]]);
						getinfo.push(this[colxv[20]]);
						basecolumn.push(this[colxv[21]]);
						getcodecolumn.push(this[colxv[22]]);
						getdesccolumn.push(this[colxv[23]]);
						gettable.push(this[colxv[24]]);
						gettablename1.push(this[colxv[25]]);
						gettablename2.push(this[colxv[26]]);
						
						console.log( updateFields );
					}
					
				});
				} 
			}); 
		  var nDesc = "";
				var strItem = 'cols=';

				for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
					strItem+= updateFields[j]+'&cols=';
		  }
		console.log(updateFields);
		  var sCode = tdCode;
				var dialogText="Value = "+sCode;
				var targetUrl = $(this).attr("href");
		  var exString = 'identifier='+updateID+'&'+updateID+'='+sCode+'&table='+updateTable+'&'+strItem;
				
				$.ajax({
				url: 'get_data.jsp',
				type:'POST',
				data: exString,
				success: function(output_string){
					var obj = $.parseJSON(output_string);
					  $.each(obj, function() {
						//var pd = $("<div/>").attr("id", "pageDialog");
				  //$(pd).text(jsType ).dialog("open");
				  var str = '<div id="dialog-form-update" title="Edit '+tdCode+'">';
				  str += '<span class="table_title">General Information</span>';
				  str += '<table id="table_left" width="100%" cellspacing="0">';
				  var i;
				  var jGet = [];
				  

				  for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
					jGet[i] = this[updateFields[i]];

				  if(updateFieldtype[i] == 'hidden'){

					str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="hidden" value="'+updateFieldvalue[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+'  />';

				  }else{
					str += '<tr><td width="20%" class="bd_bottom" align="left" valign="top">'+updateTitles[i]+'</td>';
					str += '<td width="73%" class="bd_bottom" align="left">';

					if(updateFieldtype[i] == "text"){
					  if(updateFields[i]==updateID){

						str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' readonly  />';
					  }else if(updateFields[i] == 'loccode'){
			 
						str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' readonly  />&nbsp;<input type="button" id="getlot" value="Get Location Code">' ;
					  }else if(updateFields[i] == 'depositcode'){
						dicode = "code like '1331%'";
						str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' readonly  />&nbsp;<input type="button" class="getacc" id="yes" value="Get Code">' ;
					  }else{
						str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' '+updateFieldbtparam[i]+'  />';
					  }
					  
					}else if(updateFieldtype[i] == "text - button"){
					  str += '<input id="up_'+updateFields[i]+'"  type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' />&nbsp;<input type="button" class="getacc" id="no"  value="Get Account Code"></td>';
					}else if(updateFieldtype[i] == "combo - listdb"){
					  str += '<select id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
					  var selParam = "";
					  var listDB = "parameter";
						var listFields = [ "parameter", "value" ];
						var listID = "aparameter";
						var listItem = 'cols=';
						
						for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
							listItem += listFields[m]+'&cols=';
					  }
						
						var listString = 'identifier='+listID+'&'+listID+'='+updateFieldsql[i]+'&table='+listDB+'&'+listItem;

					  $.ajax({
						url: 'get_data.jsp',
						  type:'POST',
						  async: false,
						  data: listString,
						  success: function(output){
							  var obja = $.parseJSON(output);
								$.each(obja, function() {
							var paramt = this['parameter'];
							var valt = this['value'];

							if(valt == jGet[i]){
							  selParam="selected";
							}else{
							  selParam="";
							}
							  str += '<option value"'+valt+'" '+selParam+'>'+valt+'</option>';
								
						  });
						  }
					  });
					  
					  str += '</select>';
						  
					}else if(updateFieldtype[i] == "combo - yesno"){
					  str += '<select id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';

					  var selOpt = "";

					  for (u = 0; u < yesno.length; ++u) {
						if(yesno[u] == jGet[i]){
						  selOpt="selected";
						}else{
						  selOpt="";
						}
						str += '<option value"'+yesno[u]+'" '+selOpt+'>'+yesno[u]+'</option>';
					  }
					  str += '</select>';
					}else if(updateFieldtype[i] == "textarea"){

						str += '<textarea id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+' rows="4" cols="50">'+jGet[i];
						

						str += '</textarea>';

						
					  }

				}
						
					

					  }

					  str += '<tr><td class="bd_bottom">&nbsp;<button id="updatethis">Update</button></td></td><td class="bd_bottom" align="right"></td></tr>';

						str += '</table>';
						str += '<br>';
						str += '<div id="listofitem">';
						str += '<table id="table_left" width="100%">';
						str += '<span class="table_title">List of Journal Transaction</span>';
						str += '<tr>';
						str += '<td class="bd_gene_header">Account Code</td><td class="bd_gene_header">Loc Code</td><td class="bd_gene_header">CT Code</td><td class="bd_gene_header">Remarks</td><td class="bd_gene_header">Debit</td><td class="bd_gene_header">Credit</td><td class="bd_gene_header" width="10%">Action</td>';
						str == '</tr>';


						var table_in = 'gl_jv_item';
						var colx_in = ['Jvid', 'actcode','loccode','ctcode','remark','debit','credit' ];
						  var strx_in = 'cols=';
						  for (j = 0; j < colx_in.length; ++j) {
							strx_in+= colx_in[j]+'&cols=';
						  }

						  var where_in = " where JVrefno='"+tdCode+"'";
						  var exString_in = 'table='+table_in+'&'+strx_in;
							$.ajax({
							url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where_in),
							type:'POST',
							async: false,
							data: exString_in,
							success: function(output){
							  var jGet_in = [];
							  var i = 0;
							  var obja = $.parseJSON(output);
							  $.each(obja, function() {
												  
								for (i = 0; i < colx_in.length; ++i) {//put value from database to variable jget
								  jGet_in[i] = this[colx_in[i]];
											  
								}
								
								str += '<tr>';
								str += '<input type="hidden" id="ls_Jvid" value="'+jGet_in[0]+'">';
							  str += '<td class="bd_bottom_list" id="td'+jGet_in[0]+'1"><span id="res'+jGet_in[0]+colx_in[1]+'">'+jGet_in[1]+'</span></td>';
							   str += '<td class="bd_bottom_list" id="td'+jGet_in[0]+'2"><span id="res'+jGet_in[0]+colx_in[2]+'">'+jGet_in[2]+'</span></td>';
								str += '<td class="bd_bottom_list" id="td'+jGet_in[0]+'3"><span id="res'+jGet_in[0]+colx_in[3]+'">'+jGet_in[3]+'</span></td>';
								 str += '<td class="bd_bottom_list"  id="td'+jGet_in[0]+'4"><span id="res'+jGet_in[0]+colx_in[4]+'">'+jGet_in[4]+'</span></td>';
								  str += '<td class="bd_bottom_list"  id="td'+jGet_in[0]+'5"><span id="res'+jGet_in[0]+colx_in[5]+'">'+jGet_in[5]+'</span></td>';
								  str += '<td class="bd_bottom_list"  id="td'+jGet_in[0]+'6"><span id="res'+jGet_in[0]+colx_in[6]+'">'+jGet_in[6]+'</span></td>';
								  str += '<td class="bd_bottom_list" align="center"><img src="image/icon/edit_2.png" title="Edit" width="22" style="vertical-align:middle;cursor:pointer" class="editlist" id="'+jGet_in[0]+'">&nbsp;&nbsp;';
								  str += '<img src="image/icon/delete.png" title="Delete" width="22" style="vertical-align:middle;cursor:pointer"></td>';
							str += '</tr>';
								
							  }); 
							}
						  
						  });

						

						str += '</table>';
						str += '</div>';//end of listofoitem div
					str += '</div>';//end of maindialog


						var updateTable = $(str);

						$(updateTable).dialog({
						 autoOpen: false,
						  height: 600,
						  width: 1100,
						  async:false,
						  modal: true,
					buttons: {
						
					  Cancel: function() {
						$(this).dialog("destroy");
						//$( this ).dialog( "close" );
					  }
					}
						});
					
					$(updateTable).dialog("open");

					///---------important to destroy box get
				  $('.ui-dialog-titlebar-close').on('click',function(){
					$(updateTable).dialog("destroy");
					$('#get_supplier_box').remove();
				  }); 
				  ///---------important to destroy box get

				  $('#up_JVdate').datepicker({ dateFormat: 'yy-mm-dd' });


				$( "#updatethis" ).button({
				icons: {
				  primary: "ui-icon-disk"
				}
			  })
			  .click(function(e) {
					var tblup = 'gl_jv';
					var idupdate= 'JVrefno';
					var upTitle = [];
						var dataStringUp = "";

						for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
								upTitle[j] = $('#up_'+updateFields[j]).val();

						 /* if(updateDesc == updateFields[j]){
									nDesc = $('#up_'+updateFields[j]).val();
								}*/
									  
								if(j == updateFields.length-1){
									dataStringUp += updateFields[j]+'='+upTitle[j];
								}else{
									dataStringUp += updateFields[j]+'='+upTitle[j]+'&';
								}
									  
						}
							 //alert(dataStringUp);
						dataStringUp=dataStringUp+'&theid='+idupdate+'&'+idupdate+'='+tdCode+'&table='+tblup+'&'+strItem+'&flag=edit';  
												
												$.ajax({
													type:'POST',
													data:dataStringUp,
													url:'save_dataquery.jsp',
													success:function(response) {
														$(this).dialog("destroy");
													}
												});
						
						$(updateTable).dialog("destroy");
						successNoty( generalUsage , nDesc, generalUsage+' Updated', 'Successfull for ');
	  });

				   $('.editlist').on('click',function(){
						  //var w = $(this).attr('name');
						  var k = $(this).attr('id');
						  //var e = k.substring(5, 4);
						  //addtab(w,tdCode,e);
						 editAccount(k,tdCode);
						});

				});

						
				  }
				 });

							  
				}
				
function getCurrentDate(){
	var d = new Date();

	var month = d.getMonth()+1;
	var day = d.getDate();
	
	var output = d.getFullYear() + '-' +
		((''+month).length<2 ? '0' : '') + month + '-' +
		((''+day).length<2 ? '0' : '') + day;
	
	return output;
}

function chequeNumbering(x)
{
	
try {
	 amount1 = parseFloat(document.form1.nocek.value);
	 //alert(amount1)
	 document.form1.nocek.value = round_decimals(amount1,0);
			
	//nilai1=round_decimals(nocek,0);
	
	}
	catch(ex)
	{
	alert("error conversion")
	}
  	a= document.form1.startcek.value
  	b = document.form1.nocek.value
	 c = parseFloat(a)
	 d = parseFloat(b)
	
//  c= document.form1.endcek.value
	//for (int i; i
  document.form1.endcek.value = parseFloat((c+d)-1)
  e = document.form1.endcek.value
  //alert(e.length)
  //alert(a.length)
  f = e;
  //alert(f)
  for (i = 0; i < (a.length - e.length); i++)
  	f = "0" + f;
  document.form1.endcek.value = f;	

  
}

function deleteModal(title,referenceno){
	var modal = '<div class="modal fade" id="deleteModal" role="dialog"><div class="modal-dialog"><!-- Modal content--><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Delete '+title+'</h4></div><div class="modal-body"><p>Are you sure to delete <strong>'+referenceno+'</strong> ?</p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">No</button><button type="button" class="btn btn-danger" id="godelete">Yes</button></div></div></div></div>';
	return modal;
	
	
}