/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

allWells.hide();

navListItems.click(function (e) {
    e.preventDefault();
    var $target = $($(this).attr('href')),
            $item = $(this);

    if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-default');
        $item.addClass('btn-primary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
    }
});

allNextBtn.click(function () {
    var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

    if (curStepBtn == 'step-1') {
        //alert(2);

        var procCode = $('#prodcode').val();
        var procName = $('#prodname').val();
        var bCode = $('#bcode').val();
        var bName = $('#bname').val();

        var path = '';

        if (procCode == '03') {
            path = 'ar_inv_add_ffb_param.jsp';
            $('#step-2-title').html('FFB Parameter');
            $('#step-3-title').html('FFB Calculation');
            $('#step-2-string').html('Enter the FFB Parameter for calculation.');
            $('#step-3-string').html('Check the calculation.');
        } else {
            path = 'ar_inv_add_step.jsp';
            $('#step-2-title').html('Choose Contract');
            $('#step-2-string').html('Choose <strong>' + procName + '</strong> Contract for <strong>' + bName + '</strong>');
            $('#step-3-title').html('Complete Refinery');
            $('#step-3-string').html('Complete Refinery');
        }

        $.ajax({
            url: path + "?buyercode=" + bCode + "&prodcode=" + procCode,
            success: function (result) {
                $('#table-result').empty().html(result).hide().fadeIn(300);

            }
        });


    }

    if (curStepBtn == 'step-2') {
        var procCode = $('#prodcode').val();
        var millCode = $('#millcode').val();
        var sessionid = $('#sessionid').val();
        var contractNo = '';
        var path = '';
        var next = false;

        if (procCode == '03') {
            var a = $("#saveffb :input").serialize();
            $.ajax({
                async: false,
                data: a,
                type: 'POST',
                url: "ProcessController?moduleid=020903&process=calculateffb&sessionid=" + sessionid,
                success: function (result) {
                    //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                }
            });
            path = 'ar_inv_add_ffb_calculate.jsp?';
            next = true;
        } else {
            contractNo = $('#selected-contract').val();

            var origin = contractNo.substring(0, 3);

            if (origin == 'SAG') {
                $.ajax({
                    async: false,
                    url: "ProcessController?moduleid=020903&process=generatedisno&prodcode=06",
                    success: function (result) {
                        b = result;
                        path = 'ar_inv_add_shell.jsp?dispatchno=' + b + '&';
                        next = true;

                    }
                });

            } else {

                var inputPrice = $('#inputprice').val();
                if (inputPrice == 0.0) {
                    BootstrapDialog.show({
                        title: 'Move to pending',
                        message: 'State the reason of pending: <input type="text" class="form-control">',
                        closeByBackdrop: false,
                        closeByKeyboard: false,
                        buttons: [{
                                label: 'Cancel',
                                action: function (dialogRef) {
                                    //$('#' + cardid).prependTo('#' + getListDIV(list_origin));
                                    dialogRef.close();
                                }
                            },
                            {
                                label: 'Send to Pending',
                                hotkey: 13, // Enter.
                                cssClass: 'btn-primary',
                                action: function (dialogRef) {
                                    var newprice = dialogRef.getModalBody().find('input').val();
                                    if (newprice === '') {
                                        //$('#'+cardid).prependTo('#progressdiv');
                                        dialogRef.setTitle('Enter the price!');
                                        //dialogRef.close();
                                        //return false;
                                    } else {
                                        //$('#comment' + cardid).empty().html(' ' + newcnt).hide().fadeIn(300);
                                        $.ajax({
                                            url: "ProcessController?moduleid=020903&process=updateprice&price="+ newprice +"&contractno="+ contractNo +"&sessionid=" + sessionid,
                                            success: function (result) {
                                                //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                                                path = 'ar_inv_check_contract.jsp?';
                                                next = true;
                                                dialogRef.close();
                                            }
                                        });
                                        
                                    }
                                }
                            }]
                    });
                } else {
                    path = 'ar_inv_check_contract.jsp?';
                    next = true;
                    //alert('xde price');
                }

            }
        }


        //var bCode = $('#bcode').val();
        if(next==true){
            $.ajax({
            async: true,
            url: path + "contract=" + contractNo + "&millcode=" + millCode + "&prodcode=" + procCode + '&sessionid=' + sessionid,
            success: function (result) {
                console.log('table-load');
                $('#table-result-contract').empty().html(result).hide().fadeIn(300);

            },
            error: function () {
                var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                $('#table-result-contract').empty().html(conts).hide().fadeIn(300);
            }


        });
        }
        



    }

    if (curStepBtn == 'step-3') {

        var sessionid = $('#sessionid').val();
        var bCode = $('#bcode').val();
        var prodCode = $('#prodcode').val();
        var contractNo = 'NONE';
        var sum = '';

        if (prodCode == '03') {

            sum = $('#nettamount').val();

        } else {
            contractNo = $('#selected-contract').val();
            sum = $('#sum_mt_refinery').val();

            var str = 'dispatchno=';
            $("input[type=checkbox]:checked").each(function () {

                str = str + $(this).val() + '&dispatchno=';

            });
            console.log(str);

            //var bCode = $('#bcode').val();

            var a = $("#saverefinery :input").serialize();
            $.ajax({
                data: a,
                type: 'POST',
                url: "ProcessController?moduleid=020903&process=temprefinery&sessionid=" + sessionid + "&" + str,
                success: function (result) {
                    //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                }
            });
        }




        $.ajax({
            async: true,
            url: "ar_inv_add_invoice.jsp?contract=" + contractNo + "&amount=" + sum + "&prodcode=" + prodCode + "&bcode=" + bCode + "&sessionid=" + sessionid,
            success: function (result) {
                console.log('table-load');
                $('#prepare-invoice').empty().html(result).hide().fadeIn(300);

            }


        });

        $('#total').trigger('change');


    }

    if (curStepBtn == 'step-4') {
        var prodCode = $('#prodcode').val();
        var sessionid = $('#sessionid').val();
        var contractNo = $('#selected-contract').val();
        var prodName = $('#prodname').val();
        var totalItem = $('#totalrow').val();
        var path = '';

        var a = $("#savemaster :input").serialize();
        $.ajax({
            data: a,
            async: false,
            type: 'POST',
            url: "ProcessController?moduleid=020903&process=tempmaster&sessionid=" + sessionid,
            success: function (result) {
                //$('#maincontainer').empty().html(result).hide().fadeIn(300);
            }
        });


        if (prodCode == '03') {
            path = "ar_inv_finalize_ffb.jsp?sessionid=" + sessionid + "&prodcode=" + prodCode + "&prodname=" + prodName;
        } else {
            path = "ar_inv_finalize.jsp?sessionid=" + sessionid + "&prodcode=" + prodCode + "&prodname=" + prodName;
        }

        $.ajax({
            async: true,
            url: path,
            success: function (result) {
                console.log('table-load');
                $('#finalize-invoice').empty().html(result).hide().fadeIn(300);

            }


        });


    }

    if (curStepBtn == 'step-5') {
        alert('Done');
    }


    $(".form-group").removeClass("has-error");
    for (var i = 0; i < curInputs.length; i++) {
        if (!curInputs[i].validity.valid) {
            isValid = false;
            $(curInputs[i]).closest(".form-group").addClass("has-error");
        }
    }

    if (isValid)
        nextStepWizard.removeAttr('disabled').trigger('click');
});

$('div.setup-panel div a.btn-primary').trigger('click');

$('body').on('click', '.activerowy', function (e) {
    $('.refineryBtn').trigger('click');
    return false;
});


