/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


  

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;
        
        if(curStepBtn=='step-1'){
            //alert(2);
            
           
//            var bCode = $('#bcode').val();
//            
//              $.ajax({
//                        url: "ar_inv_add_step.jsp?buyercode="+bCode,
//                        success: function (result) {
//                        $('#table-result').empty().html(result).hide().fadeIn(300);
//                
//            }});
        }
        
        if(curStepBtn=='step-2'){
            
            var str = 'invno=';
            $("input[type=checkbox]:checked").each(function() {
                
                str = str + $(this).val() + '&invno=';

            });
            console.log(str);
//            var contractNo = $('#selected-contract').val();
//            var millCode = $('#millcode').val();
//            //var bCode = $('#bcode').val();
            $.ajax({
                async:true,
                url: "ap_pv_topay.jsp?"+str,
                success: function (result) {
                       $('#amount-to-pay').empty().html(result).hide().fadeIn(300);
                
                },
                error: function(){
                    var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                    $('#table-result-contract').empty().html(conts).hide().fadeIn(300);
                }
                
                
            });
            
            
            
        }
        
        if(curStepBtn=='step-3'){
            
            var sessionID = $('#sessionid').val();
            var suppCode = $('#suppcode').val();
            console.log(sessionID);
            
            var a = $("#saveamount :input").serialize();
            $.ajax({
                data: a,
                type: 'POST',
                url: "ProcessController?moduleid=021009&process=tempamount&sessionid="+sessionID+"&suppcode="+suppCode,
                success: function (result) {
                    //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                }
            });
          
            
            $.ajax({
                async:true,
                url: "ap_pv_prepare_voucher.jsp?suppcode="+suppCode+"&sessionid="+sessionID,
                success: function (result) {
                    console.log('table-load');
                       $('#prepare-voucher').empty().html(result).hide().fadeIn(300);
                
                }
                
                
            });
            
            //$('#total').trigger('change');
            
            
        }
        
        if(curStepBtn=='step-4'){
           var sessionID = $('#sessionid').val();
            var a = $("#savemaster :input").serialize();
            $.ajax({
                data: a,
                async:false,
                type: 'POST',
                url: "ProcessController?moduleid=021009&process=tempmaster&sessionid="+sessionID,
                success: function (result) {
                    //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                }
            });
            
            
            $.ajax({
                async:true,
                url: "ap_pv_finalize.jsp?sessionid="+sessionID,
                success: function (result) {
                    console.log('table-load');
                       $('#finalize-voucher').empty().html(result).hide().fadeIn(300);
                       window.scrollTo(0,0);
                
                }
                
                
            });
            
            
        }
        
        if(curStepBtn=='step-5'){
            alert('Done');
        }
        
        
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    
    $('body').on( 'click', '.activerowy', function (e) {
        //$('.refineryBtn').trigger('click');
        return false;
    });

    $('body').on( 'click', '.thisresult-payee', function (e) {
        var suppcode = $(this).attr('id');
        $('#suppcode').val(suppcode);
        $('.payeeBtn').trigger('click');
        return false;
    });

